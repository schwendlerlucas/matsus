class MeioTransporteInsumosResultadoDTO:
    
    def __init__(self):
        self.atividade_nome = ""
        self.insumo_nome = ""
        self.distancia = 0.0
        self.meio_transporte_nome = ""
        self.GWP = 0.0
        self.GWPTotal = 0.0
        self.ODP = 0.0
        self.ODPTotal = 0.0
        self.AP = 0.0
        self.APTotal = 0.0
        self.EP = 0.0
        self.EPTotal = 0.0
        self.POCP = 0.0
        self.POCPTotal = 0.0
        self.residPerigoso = 0.0
        self.residPerigosoTotal = 0.0
        self.residRadioativo = 0.0
        self.residRadiativoTotal = 0.0
        self.residNaoPerigoso = 0.0
        self.residNaoPerigosoTotal = 0.0
        self.fonteNaoRenovavel = 0.0
        self.fonteNaoRenovavelTotal = 0.0
        self.fonteRenovavel = 0.0
        self.fonteRenovavelTotal = 0.0
        self.custoTotal = 0.0