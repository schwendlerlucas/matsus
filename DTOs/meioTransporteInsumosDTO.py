class MeioTransporteInsumosDTO:

    def __init__(self):
        self.atividade_id = -1
        self.atividade_nome = ""
        self.insumo_id = -1
        self.insumo_nome = ""
        self.quantidade_unitaria = 0
        self.quantidade_total = 0
        self.peso_unitario = 0
        self.peso_total = 0
        self.unidade = ""
        self.meio_transporte_id = None
        self.meio_transporte_nome = ""
        self.distancia1 = 0
        self.distancia2 = 0
        self.valorFrete = 0.0

    def distanciaTotal(self):
        soma = 0.0
        if (self.distancia1 != None) and (self.distancia1 != ""):
            soma += float(self.distancia1)
        if (self.distancia2 != None) and (self.distancia2 != ""):
            soma += float(self.distancia2)
        
        return soma