class ResultadosSistemaContrutivoDTO:
    
    def __init__(self):
        self.projeto_nome = ""
        self.sistema_nome = ""
        self.quantidade = 0.0

        self.GWPTotal = 0.0
        self.ODPTotal = 0.0
        self.APTotal = 0.0
        self.EPTotal = 0.0
        self.POCPTotal = 0.0
        self.ResidPerigosoTotal = 0.0
        self.ResidRadioativoTotal = 0.0
        self.ResidNaoPerigosoTotal = 0.0
        self.EnergiaNaoRenovavelTotal = 0.0
        self.EnergiaRenovavelTotal = 0.0
        self.AguaRedeAbastesimentoTotal = 0.0
        self.AguaReusoTotal = 0.0

        self.CustoTotal = 0.0

        self.AquisiMateriaPrimaLocalTotal = 0.0
        self.MaoObraLocalTotal = 0.0
        
        self.GrauPopularizacaoConceitosTotal = 0.0
        self.GrauFomentoInstituicaoTotal = 0.0
        self.GrauMatCulturalmenteUsadosTotal = 0.0
        self.GrauSalubridadeTotal = 0.0
        self.GrauSeguridadeTotal = 0.0
        self.GrauContribNaEdificacaoTotal = 0.0
        self.GrauComplexidadeTotal = 0.0
        self.PossibilidadeMutiroesTotal = 0.0

        self.ParcelaPodeSerRecicladaTotal = 0.0
        self.ParcelaPodeSerReaproveitadaTotal = 0.0
        self.GrauEstanqueidadeTotal = 0.0
        self.GrauTransmitanciaTermicaTotal = 0.0
        self.GrauTransmissaoOndasTotal = 0.0
        self.ComportMecanicoTotal = 0.0
        self.DurabilidadeTotal = 0.0
        self.AcompanhamentoProfissionaisTotal = 0.0
        self.GrauPericibilidadeTotal = 0.0
        self.FacilidadeEstocagemTotal = 0.0
        self.FacilidadeTransporteTotal = 0.0
        self.FacilidadeManutencaoTotal = 0.0
        self.FacilidadeReparoTotal = 0.0
        self.PadronizacaoReplicacaoTotal = 0.0
        self.VersatilidadeTotal = 0.0

        self.ResultadoFinal = 0.0



        