from models.projeto import Projeto
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from DTOs.atividadeSeguridadeSalubridadeSistemaDTO import AtividadeSeguridadeSalubridadeSistemaDTO
from DTOs.sistemaMeioTransporteInsumosDTO import SistemaMeioTransporteInsumosDTO

class CadProjetoDTO:

    def __init__(self):
        self.projeto = Projeto()
        self.projetoSistemaConstrutivoList = []
        self.sistemaAtividadeSalubridadeSeguridadeList = []
        self.meioTransporteInsumoEquipamentoList = []
        self.meioTransporteInsumoMaterialList = []

    def getSistemaAtividadeSalubridadeSeguridadeList(self, sistema_id):
        for item in self.sistemaAtividadeSalubridadeSeguridadeList:
            if item.sistema_id == sistema_id:
                return item.atividadeList

        return []

    def setSistemaAtividadeSalubridadeSeguridadeList(self, sistema_id, atividadeList):
        if sistema_id <= 0:
            return
        for item in self.sistemaAtividadeSalubridadeSeguridadeList:
            if item.sistema_id == sistema_id:
                item.atividadeList = atividadeList
                return
        
        newitem = AtividadeSeguridadeSalubridadeSistemaDTO()
        newitem.sistema_id = sistema_id
        newitem.atividadeList = atividadeList
        self.sistemaAtividadeSalubridadeSeguridadeList.append(newitem)

    def getMeioTransporteInsumoEquipamentosList(self, sistema_id):
        for item in self.meioTransporteInsumoEquipamentoList:
            if item.sistema_id == sistema_id:
                return item.insumosList

        return []

    def setMeioTransporteInsumoEquipamentosList(self, sistema_id, insumosList):
        if sistema_id <= 0:
            return
        for item in self.meioTransporteInsumoEquipamentoList:
            if item.sistema_id == sistema_id:
                item.insumosList = insumosList
                return
        
        newitem = SistemaMeioTransporteInsumosDTO()
        newitem.sistema_id = sistema_id
        newitem.insumosList = insumosList
        self.meioTransporteInsumoEquipamentoList.append(newitem)

    def getMeioTransporteInsumoMateriaisList(self, sistema_id):
        for item in self.meioTransporteInsumoMaterialList:
            if item.sistema_id == sistema_id:
                return item.insumosList

        return []

    def setMeioTransporteInsumoMateriaisList(self, sistema_id, insumosList):
        if sistema_id <= 0:
            return
        for item in self.meioTransporteInsumoMaterialList:
            if item.sistema_id == sistema_id:
                item.insumosList = insumosList
                return
        
        newitem = SistemaMeioTransporteInsumosDTO()
        newitem.sistema_id = sistema_id
        newitem.insumosList = insumosList
        self.meioTransporteInsumoMaterialList.append(newitem)
 