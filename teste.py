from tkinter import *

from sqlalchemy.orm import sessionmaker, relationship

from database.mydatabase import *
from database.createTables import CreateTables
from models.criarFatoresEmissao import *

from DTOs.cadProjetoDto import CadProjetoDTO

from models.sistemaConstrutivo import SistemaConstrutivo
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from models.projeto import Projeto

from interfaces.frmCadProjetoComparaSistemas import FrameCadProjetoComparaSistemas

db = MyDatabase(SQLITE, dbname='./database/mydb.sqlite')

createAll = CreateTables()
createAll.create(db.db_engine)

Session = sessionmaker(bind=db.db_engine)
session = Session()


criarFatores = CriarFatoresEmissao(session)
criarFatores.CriarSeNaoExistir()

root = Tk()




def openCadProjetoCamparaSistemas(cadProjetoDTO:CadProjetoDTO=None):
     FrameCadProjetoComparaSistemas(root, session=session, cadProjetoDTO=cadProjetoDTO)
     #frmCadProjetoComparaSistemas.definirVoltar(openCadProjetoSelecionarSistema)


cadProjetoDTO = CadProjetoDTO()

cadProjetoDTO.projeto = Projeto()

cadProjetoDTO.projeto.nome = "teste"
cadProjetoDTO.qtdSistemaConstrutivo = 30

projetoSistema = ProjetoSistemaConstrutivo()

projetoSistema.projeto = cadProjetoDTO.projeto
projetoSistema.sistemaConstrutivo = SistemaConstrutivo()
projetoSistema.sistemaConstrutivo.nome = "sistema teste"
projetoSistema.sistemaConstrutivo.id = 1


cadProjetoDTO.projetoSistemaConstrutivoList.append(projetoSistema)


openCadProjetoCamparaSistemas(cadProjetoDTO)


root.mainloop()
