# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.constantesIndicadoresSistemaConstrutivo import *

from datetime import datetime


from models.atividade import Atividade
from models.sistemaConstrutivo import SistemaConstrutivo



class FrameCadSistemaConstrutivoIndicadoresEconomicos(FrameBase):
    
    def __init__(self, parent=None, session=None, sistemaConstrutivo=None):
        super(FrameCadSistemaConstrutivoIndicadoresEconomicos, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__sistemaConstrutivoDto = sistemaConstrutivo
        self.__openProximaTela = None
        self.__lastItemSelected = None

        
        parent.title("Cadastro de Sistema Construtivo")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1000x630+100+100")
        self.__createUI()

        self.__carregarValorCampos()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        
        #frameImage = Frame(master=self.frameTop)
        #frameImage.pack(side=)
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=20)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Cadastro de Sistema Construtivo", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frmLabels = Frame(frameCabecalho, width=1)
        frmLabels.pack(side=LEFT)
        labelSistemaContrutivo = Label(frmLabels, text="                                                           Sistema construtivo")
        labelSistemaContrutivo.pack(side=TOP, fill=X, pady=5)

        
        frmCampo = Frame(frameCabecalho)
        frmCampo.pack(side=RIGHT)

        frmEntries = Frame(frmCampo, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtSistemaConstVar = StringVar()
        self.edtSistemaConst = Entry(frmEntries, width = "30", textvariable=self.edtSistemaConstVar)
        self.edtSistemaConst.pack(side=TOP, fill=X, pady=5)

        self.frmTreeView = Frame()
        self.frmTreeView.pack(fill=BOTH, side=TOP, expand=1)

        frmTreeViewScrollBar = Frame(self.frmTreeView)
        frmTreeViewScrollBar.pack(side=TOP, fill=BOTH, expand=1, padx=5)

        self.treeView = ttk.Treeview(master=frmTreeViewScrollBar, height=4)
        self.treeView.bind('<Double-Button>', self.__onDoubleClick)

        self.scrollbarV = Scrollbar(master=frmTreeViewScrollBar)
        self.scrollbarV.pack(fill=Y, side=RIGHT, pady=15)
        self.scrollbarV.config(command = self.treeView.yview)

        self.treeView.configure(yscrollcommand=self.scrollbarV.set, height=15)
        self.treeView.pack(pady=15, fill=BOTH, expand=1)

        frameEdicao = Frame(self.frmTreeView)
        frameEdicao.pack(side=BOTTOM, fill=X)

        btnOkValor = Button(frameEdicao, width=3, text="ok", command=self.__btnOkOnClick)
        btnOkValor.pack(side=RIGHT, padx=5)

        self.edtValorVar = StringVar() 
        self.edtValor = Entry(frameEdicao, width=10, justify=RIGHT, textvariable=self.edtValorVar)
        self.edtValor.pack(side=RIGHT, padx=5)

        self.lblDescicao = Label(frameEdicao)
        self.lblDescicao.pack(side=RIGHT, padx=10)
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=15)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15)
        self.btnProximo = Button(frmBotoes, text="Seguinte", width=20, command=self.__telaSeguinteOnClick)       
        self.btnProximo.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)

        self.lbWarnings = Label(frmWarning, text="", height="2")
        self.lbWarnings.pack()

        self.__configurarTreeView()      


    def __configurarTreeView(self):
        self.treeView['columns'] = ('valor')
        self.treeView.heading("#0", text=' Indicadores econômicos', anchor='w')
        self.treeView.column("#0", anchor='w', width=800)
        self.treeView.heading('valor', text=' Valor ', anchor='ne')
        self.treeView.column('valor', anchor='ne', width=100)

        segundoNivel = "     "
        terceiroNivel = "          "

        self.treeView.insert('', END, text="Economico", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "Insentivo a economia local", 
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + ECON_AQUISICAO_MATERIA_LOCAL, 
                            values=(self.__pegarValorPelaDescicao(ECON_AQUISICAO_MATERIA_LOCAL)))
        self.treeView.insert('', END, text=terceiroNivel + ECON_CONTRATACAO_MAO_OBRA_LOCAL, 
                            values=(self.__pegarValorPelaDescicao(ECON_CONTRATACAO_MAO_OBRA_LOCAL)))



    def __telaSeguinteOnClick(self):
        if not self.__validarCampos():
            return

        self.__definirValoresNoDto()

        self.__openProximaTela(self.__sistemaConstrutivoDto)    
         
    def __definirValoresNoDto(self):
        self.__sistemaConstrutivoDto.nome = self.edtSistemaConst.get()

        for child in self.treeView.get_children():
            valor = 0
            if len(self.treeView.item(child)["values"]) == 1:
                valor = retornarZeroSeVazio(self.treeView.item(child)["values"][0])

            self.__definirValorNoDto(self.treeView.item(child)["text"], valor)
    
    def __validarCampos(self):
        self.lbWarnings.configure(text = "")
        if self.__sistemaConstrutivoDto == None:
           self.__sistemaConstrutivoDto = SistemaConstrutivo()     

        nomeSistemaContrutivo = self.edtSistemaConst.get()
        if (nomeSistemaContrutivo == ''):
            self.lbWarnings.configure(text = "A 'sistema contrutivo' é obrigatório", foreground="red")
            return False

        sistemaContrutivo = self.session.query(SistemaConstrutivo).filter_by(nome=nomeSistemaContrutivo).first()
        if (sistemaContrutivo != None and sistemaContrutivo.id != self.__sistemaConstrutivoDto.id):
            self.lbWarnings.configure(text = "Sistema contrutivo já cadastrado", foreground="red")
            return False 

        return True
        
    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick
    

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick


    def __carregarValorCampos(self):
        if self.__sistemaConstrutivoDto == None:
            return

        self.edtSistemaConstVar.set(self.__sistemaConstrutivoDto.nome)

        if self.__sistemaConstrutivoDto.id == None:
            return

    def definirTelaSeguinteCadSistemaConstrutivo(self, onClickProximaTela):
        self.__openProximaTela = onClickProximaTela

    def __definirValorNoDto(self, descricao: str, value: float):
        if self.__lastItemSelected == None:
            return False

        if not self.__lastItemSelected.strip() in ARRAY_INDICADORES_SISTEMA_CONS:
            return False

        _descricao = descricao.strip()

        if (_descricao == ECON_AQUISICAO_MATERIA_LOCAL):
            self.__sistemaConstrutivoDto.econAquisicaoMatLocal = value
        elif (_descricao == ECON_CONTRATACAO_MAO_OBRA_LOCAL):
            self.__sistemaConstrutivoDto.econMaoObraLocal = value
        else:
            print("Erro:  não encontrou")

    def __pegarValorPelaDescicao(self, descricao: str):
        if self.__sistemaConstrutivoDto == None:
            return "0.0"
    
        _descricao = descricao.strip()

        if (_descricao == ECON_AQUISICAO_MATERIA_LOCAL):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.econAquisicaoMatLocal)
        if (_descricao == ECON_CONTRATACAO_MAO_OBRA_LOCAL):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.econMaoObraLocal)


    def __onDoubleClick(self, event):
        self.edtValorVar.set("")
        self.lblDescicao.config(text="")
        itemSelected = self.treeView.item(self.treeView.focus())
        self.__lastItemSelected = itemSelected["text"]

        if not  self.__lastItemSelected.strip() in ARRAY_INDICADORES_SISTEMA_CONS:
            return False

        self.lblDescicao.config(text=itemSelected["text"].strip())
        self.edtValorVar.set("")
        self.edtValorVar.set(itemSelected["values"])

    def __btnOkOnClick(self):
        if not validarDoubleOuVazio(self.edtValor.get()):
            return False

        if (self.__lastItemSelected == None):
            return 
        
        if not  self.__lastItemSelected.strip() in ARRAY_INDICADORES_SISTEMA_CONS:
            return False
   
        for child in self.treeView.get_children():
            if self.treeView.item(child)["text"] == self.__lastItemSelected:
                self.treeView.item(child, text=self.__lastItemSelected, values=(self.edtValor.get()))
        


        

        
        

        
        


        





