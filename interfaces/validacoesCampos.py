from datetime import datetime

def validarData(str_date):
    try:
        datetime.strptime(str_date, "%d/%m/%Y").date()
        return True
    except Exception:
        return False

def validarInteger(str_integer):
    try:
        int(str_integer)
        return True
    except Exception:
        return False

def validarDouble(str_double):
    try:
        float(str_double)
        return True
    except Exception:
        return False

def validarDoubleOuVazio(str_double):
    if str_double == "":
        return True

    return validarDouble(str_double)

def retornarZeroSeVazio(str_double: str):
    if str_double == "":
        return 0
    
    return float(str_double)

def retornarZeroSeNoneOuVazio(double):
    if double == None or double == "":
        return 0.0
    
    return float(double)
