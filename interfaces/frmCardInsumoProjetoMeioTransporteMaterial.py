from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmCardInsumoProjetoMeioTransporteBase import FrameCardInsumoProjetoMeioTransporteBase
from controllers.atividadeMaterialController import AtividadeMaterialtoController

class FrameCardInsumoProjetoMeioTransporteMaterial(FrameCardInsumoProjetoMeioTransporteBase):


    def queryInsumos(self):
        super(FrameCardInsumoProjetoMeioTransporteMaterial, self).queryInsumos()
        self.insumosList = self.cadProjetoDTO.getMeioTransporteInsumoMateriaisList(self.sistema_id)
        if len(self.insumosList) == 0:
            listaInsumosDTO = self.__getListNewProjeto()
            self.cadProjetoDTO.setMeioTransporteInsumoMateriaisList(self.sistema_id, listaInsumosDTO)
            self.insumosList = listaInsumosDTO


    def __getListNewProjeto(self):
        controller = AtividadeMaterialtoController(self.session)
        listaAtividades = controller.getMaterialAtividadeList(self.sistema_id, 
            self.cadProjetoDTO.projeto.qtdSistemaConstrutivo)
        return listaAtividades


    def setListOnCadProjetoDTO(self):
        super(FrameCardInsumoProjetoMeioTransporteMaterial, self).setListOnCadProjetoDTO()
        if self.sistema_id > 0:
            self.cadProjetoDTO.setMeioTransporteInsumoMateriaisList(self.sistema_id, self.insumosList)
    
