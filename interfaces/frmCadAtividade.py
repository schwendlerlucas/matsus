# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime


from models.atividade import Atividade
from models.atividadeMaoDeObra import AtividadeMaoDeObra
from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaterial import AtividadeMaterial
from models.maoObra import MaoObra
from models.equipamento import Equipamento
from models.material import Material

from interfaces.frmItensAtividade import FrameItensAtividade


class FrameCadAtividade(FrameBase):
    
    def __init__(self, parent=None, session=None, atividade=None):
        super(FrameCadAtividade, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__atividadeDto = atividade

        
        parent.title("Cadastro de Atividade")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("800x775+100+50")
        self.__createUI()

        self.__carregarValorCampos()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Cadastro de Atividade", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frmLabels = Frame(frameCabecalho, width=1)
        frmLabels.pack(side=LEFT)
        labelAtividade = Label(frmLabels, text="                                             Descrição da Atividade ")
        labelAtividade.pack(side=TOP, fill=X, pady=5)
        labelEncargosSociais = Label(frmLabels, text="                                 Percentual de Encargos Sociais ")
        labelEncargosSociais.pack(side=TOP, fill=X, pady=5)
        labelGrauSalubridade = Label(frmLabels, text="                                                Grau de Salubridade ")
        labelGrauSalubridade.pack(side=TOP, fill=X, pady=5)
        labelGrauSeguridade = Label(frmLabels, text="                                                 Grau de Seguridade ")
        labelGrauSeguridade.pack(side=TOP, fill=X, pady=5)

        
        frmCampo = Frame(frameCabecalho)
        frmCampo.pack(side=RIGHT)

        frmEntries = Frame(frmCampo, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtAtividadeVar = StringVar()
        self.edtAtividade = Entry(frmEntries, width = "30", textvariable=self.edtAtividadeVar)
        self.edtAtividade.pack(side=TOP, fill=X, pady=5)

        self.edtEncargoSocialVar = StringVar()
        self.edtEncargoSocial = Entry(frmEntries, width = "30", textvariable=self.edtEncargoSocialVar, justify=RIGHT)
        self.edtEncargoSocial.pack(side=TOP, fill=X, pady=5)

        self.edtGrauSalubridadeVar = StringVar()
        self.edtGrauSalubridade = Entry(frmEntries, width = "30", textvariable=self.edtGrauSalubridadeVar, justify=RIGHT)
        self.edtGrauSalubridade.pack(side=TOP, fill=X, pady=5)

        self.edtGrauSeguridadeVar = StringVar()
        self.edtGrauSeguridade = Entry(frmEntries, width = "30", textvariable=self.edtGrauSeguridadeVar, justify=RIGHT)
        self.edtGrauSeguridade.pack(side=TOP, fill=X, pady=5)


        self.frmTreeViews = Frame()
        self.frmTreeViews.pack(fill=X, side=TOP)

        self.frameMaoDeObra = FrameItensAtividade(self.frmTreeViews, self.session, MaoObra, AtividadeMaoDeObra)
        self.frameMaoDeObra.definirDescricao("Mão de Obra")
        self.frameMaoDeObra.pack(fill=X, side=TOP, padx=6, pady=6)
        self.frameMaoDeObra.definirBeforeOpenEdicao(self.__beforeOpenEdicao)

        self.frameMaterial = FrameItensAtividade(self.frmTreeViews, self.session, Material, AtividadeMaterial)
        self.frameMaterial.definirDescricao("Material")
        self.frameMaterial.pack(fill=X, side=TOP, padx=6, pady=6)
        self.frameMaterial.definirBeforeOpenEdicao(self.__beforeOpenEdicao)

        self.frameEquipamento = FrameItensAtividade(self.frmTreeViews, self.session, Equipamento, AtividadeEquipamento)
        self.frameEquipamento.definirDescricao("Equipamento")
        self.frameEquipamento.pack(fill=X, side=TOP, padx=6, pady=6)
        self.frameEquipamento.definirBeforeOpenEdicao(self.__beforeOpenEdicao)

        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15)
        self.btnNovo = Button(frmBotoes, text="Novo", width=10)
        self.btnNovo.pack(side=LEFT, padx=15)
        self.btnCadastrar = Button(frmBotoes, text="Cadastrar", width=20, command=self.__cadastrarOnClick)       
        self.btnCadastrar.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)

        self.lbWarnings = Label(frmWarning, text="", height="2")
        self.lbWarnings.pack()


    def __cadastrarOnClick(self):
        self.__recolherCamposEdicao()
        if not self.__validarCampos():
            return

        self.__atividadeDto.nome = self.edtAtividade.get()
        self.__atividadeDto.encargoSocial = self.edtEncargoSocial.get()
        self.__atividadeDto.grauSalubridade = self.edtGrauSalubridade.get()
        self.__atividadeDto.grauSeguridade = self.edtGrauSeguridade.get()

        inserindo = self.__atividadeDto.id == None
        if inserindo:
            self.session.add(self.__atividadeDto)

        self.session.commit()

        self.frameEquipamento.salvar(self.__atividadeDto.id)
        self.frameMaoDeObra.salvar(self.__atividadeDto.id)
        self.frameMaterial.salvar(self.__atividadeDto.id)
        
        self.session.commit()

        if inserindo:
            self.lbWarnings.configure(text = "Atividade cadastrada com sucesso", foreground="black")        
        else:
            self.lbWarnings.configure(text = "Atividade atualizada com sucesso", foreground="black")        
    
    def __validarCampos(self):
        self.lbWarnings.configure(text = "")
        if self.__atividadeDto == None:
           self.__atividadeDto = Atividade()     

        nomeAtividade = self.edtAtividade.get()
        if (nomeAtividade == ''):
            self.lbWarnings.configure(text = "A 'atividade' é obrigatória", foreground="red")
            return False

        if not validarDouble(self.edtEncargoSocial.get()):
            self.lbWarnings.configure(text = "o valor de 'Encargo Social' deve ser numerico.", foreground="red")
            return False
        
        if not validarDouble(self.edtGrauSalubridade.get()):
            self.lbWarnings.configure(text = "o valor de 'Grau de Salubridade' deve ser numerico.", foreground="red")
            return False

        if not validarDouble(self.edtGrauSeguridade.get()):
            self.lbWarnings.configure(text = "o valor de 'Grau de Seguridade' deve ser numerico.", foreground="red")
            return False

        atividade = self.session.query(Atividade).filter_by(nome=nomeAtividade).first()
        if (atividade != None and atividade.id != self.__atividadeDto.id):
            self.lbWarnings.configure(text = "Atividade já cadastrada", foreground="red")
            return False 

        return True
        
    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick
    

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick

    def __beforeOpenEdicao(self):
        self.__recolherCamposEdicao()

    def __recolherCamposEdicao(self):
        self.frameEquipamento.recolherCamposEdicao()
        self.frameMaoDeObra.recolherCamposEdicao()
        self.frameMaterial.recolherCamposEdicao()

    def __carregarValorCampos(self):
        if self.__atividadeDto == None:
            return

        self.edtAtividadeVar.set(self.__atividadeDto.nome)
        self.edtEncargoSocialVar.set(self.__atividadeDto.encargoSocial)
        self.edtGrauSalubridadeVar.set(self.__atividadeDto.grauSalubridade)
        self.edtGrauSeguridadeVar.set(self.__atividadeDto.grauSeguridade)

        if self.__atividadeDto.id == None:
            return

        self.frameEquipamento.consultar(self.__atividadeDto.id)
        self.frameMaoDeObra.consultar(self.__atividadeDto.id)
        self.frameMaterial.consultar(self.__atividadeDto.id) 


    def definirNovaAtividadeClick(self, onClick):
        self.btnNovo["command"] = onClick        





