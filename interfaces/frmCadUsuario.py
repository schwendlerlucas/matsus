# Cadastro 2
#from usuarios import usuario
from tkinter import *
from tkinter import ttk
from interfaces.frmBase import FrameBase
from models.user import User

class FrameCadUsuario(FrameBase):
    
    def __init__(self, parent=None, session=None):
        super(FrameCadUsuario, self).__init__(parent=parent, session=session)

        self.clearParent()
        #Geometria da Janela
        parent.title("Cadastro de novo usuário")
        parent.geometry ("900x600+100+100")

        #Elementos da Janela -> containers na Janela
        ###############################################
        #Container 1 - Logo
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        self.label = Label(image = self.photo)
        ###############################################


        ###############################################
        #Container 2 - Textos
        self.lb1 = Label(text=" Novo Usuário ", font=14, width=10, height="3")
        self.lb2 = Label(text=" Forma de tratamento ", height="2")
        self.lb3 = Label(text="Nome ", height="2")
        self.lb4 = Label(text="Sobrenome ", height="2")
        self.lb5 = Label(text="Email (usuário) ", height="2")
        self.lb6 = Label(text="País ", height="2")
        self.lb7 = Label(text="Estado ", height="2")
        self.lb8 = Label(text="Cidade ", height="2")
        self.lb9 = Label(text="Contato Telefônico ", height="2")
        self.lb10 = Label(text="Organização que possui vinculo ", height="2")
        self.lb11 = Label(text="Senha ", height="2")
        self.lb12 = Label(text="Confirmação de senha ", height="2")
        self.lbWarnings = Label(text="", height="2")
        ###############################################


        ###############################################
        #Container 3 - Entray
        self.nome = Entry(width = "58")
        self.sobrenome = Entry(width = "58")
        self.email = Entry(width = "58")
        self.estado = Entry(width = "58")
        self.cidade = Entry(width = "58")
        self.telefone = Entry(width = "58")
        self.organizacao = Entry(width = "58")
        self.senha = Entry(width = "58", show = "*")
        self.confirmasenha = Entry(width = "58", show = "*")
        ###############################################


        ###############################################
        #Container 3 - botoes
        self.Bt1 = Button(text="Cadastrar", command = self.confirma_senha)
        ###############################################
        
        self.btnVoltar = Button(text="Voltar")

        ###############################################
        #Container 4 - Caixa de seleção
        self.cbTermosDeUsoVar = IntVar()
        self.cbTermosDeUso = Checkbutton(text="Aceito os Termos de uso", height=3, variable=self.cbTermosDeUsoVar)
        ###############################################


        ###############################################
        #Container 5 - Caixa suspensa
        itens1 = ["Sr.", "Sra", "Dr.", "Me.", "Arq.º(ª)", "Eng.º(ª)", "Prof.(ª)"]
        itens2 = ["Selecionar", "Brasil", "", "Afeganistão",	"África do Sul",	"Albânia",	"Alemanha",	"Andorra",	"Angola", "Antiga e Barbuda",	"Arábia Saudita",	"Argélia",	"Argentina",	"Arménia",	"Austrália",	"Áustria",	"Azerbaijão",	"",	"Bahamas",	"Bangladexe",	"Barbados",	"Barém",	"Bélgica",	"Belize",	"Benim",	"Bielorrússia",	"Bolívia",	"Bósnia e Herzegovina",	"Botsuana",	"Brunei",	"Bulgária",	"Burquina Faso",	"Burúndi",	"Butão",	"",	"Cabo Verde",	"Camarões",	"Camboja",	"Canadá",	"Catar",	"Cazaquistão",	"Chade",	"Chile",	"China",	"Chipre",	"Colômbia",	"Comores",	"Congo-Brazzaville",	"Coreia do Norte",	"Coreia do Sul",	"Cosovo",	"Costa do Marfim",	"Costa Rica",	"Croácia",	"Cuaite",	"Cuba",	"",	"Dinamarca",	"Dominica",	"",	"Egito",	"Emirados Árabes Unidos",	"Equador",	"Eritreia",	"Eslováquia",	"Eslovénia",	"Espanha",	"Estado da Palestina",	"Estados Unidos",	"Estónia",	"Etiópia",	"",	"Fiji",	"Filipinas",	"Finlândia",	"França",	"",	"Gabão",	"Gâmbia",	"Gana",	"Geórgia",	"Granada",	"Grécia",	"Guatemala",	"Guiana",	"Guiné",	"Guiné Equatorial",	"Guiné-Bissau",	"",	"Haiti",	"Honduras",	"Hungria",	"",	"Iémen",	"Ilhas Marechal",	"Índia",	"Indonésia",	"Irão",	"Iraque",	"Irlanda",	"Islândia",	"Israel",	"Itália",	"",	"Jamaica",	"Japão",	"Jibuti",	"Jordânia",	"",	"Laus",	"Lesoto",	"Letónia",	"Líbano",	"Libéria",	"Líbia",	"Listenstaine",	"Lituânia",	"Luxemburgo",	"",	"Macedónia",	"Madagáscar",	"Malásia",	"Maláui",	"Maldivas",	"Mali",	"Malta",	"Marrocos",	"Maurícia",	"Mauritânia",	"México",	"Mianmar",	"Micronésia",	"Moçambique",	"Moldávia",	"Mónaco",	"Mongólia",	"Montenegro",	"",	"Namíbia",	"Nauru",	"Nepal",	"Nicarágua",	"Níger",	"Nigéria",	"Noruega",	"Nova Zelândia",	"",	"Omã",	"",	"Países Baixos",	"Palau",	"Panamá",	"Papua Nova Guiné",	"Paquistão",	"Paraguai",	"Peru",	"Polónia",	"Portugal",	"",	"Quénia",	"Quirguistão",	"Quiribáti",	"",	"Reino Unido",	"República Centro-Africana",	"República Checa",	"República Democrática do Congo",	"República Dominicana",	"Roménia",	"Ruanda",	"Rússia",	"",	"Salomão",	"Salvador",	"Samoa",	"Santa Lúcia",	"São Cristóvão e Neves",	"São Marinho",	"São Tomé e Príncipe",	"São Vicente e Granadinas",	"Seicheles",	"Senegal",	"Serra Leoa",	"Sérvia",	"Singapura",	"Síria",	"Somália",	"Sri Lanca",	"Suazilândia",	"Sudão",	"Sudão do Sul",	"Suécia",	"Suíça",	"Suriname",	"Tailândia",	"Taiuã",	"Tajiquistão",	"Tanzânia",	"Timor-Leste",	"Togo",	"Tonga",	"Trindade e Tobago",	"Tunísia",	"Turcomenistão",	"Turquia",	"Tuvalu",	"Ucrânia",	"Uganda",	"Uruguai",	"Usbequistão",	"Vanuatu",	"Vaticano",	"Venezuela",	"Vietname",	"Zâmbia",	"Zimbábue"]


        self.tratamento = ttk.Combobox()
        self.tratamento["values"] = itens1
        self.tratamento.current(0)

        self.pais = ttk.Combobox()
        self.pais["values"] = itens2
        self.pais.current(0)
        ###############################################


        ###############################################
        #Container 6 - Posição dos Elementos

        self.label.grid(row=0, column=0, rowspan=3, sticky=S)

        self.lb1.grid(row=0, column=1, padx=30)
        self.lb2.grid(row=1, column=1, sticky=E)
        self.lb3.grid(row=2, column=1, sticky=E)
        self.lb4.grid(row=3, column=1, sticky=E)
        self.lb5.grid(row=4, column=1, sticky=E)
        self.lb6.grid(row=5, column=1, sticky=E)
        self.lb7.grid(row=6, column=1, sticky=E)
        self.lb8.grid(row=7, column=1, sticky=E)
        self.lb9.grid(row=8, column=1, sticky=E)
        self.lb10.grid(row=9, column=1, sticky=E)
        self.lb11.grid(row=10, column=1, sticky=E)
        self.lb12.grid(row=11, column=1, sticky=E)
        self.lbWarnings.grid(row=12, column=2, sticky=W)

        self.nome.grid(row=2, column=2, sticky=E)
        self.sobrenome.grid(row=3, column=2, sticky=E)
        self.email.grid(row=4, column=2, sticky=E)
        self.estado.grid(row=6, column=2, sticky=E)
        self.cidade.grid(row=7, column=2, sticky=E)
        self.telefone.grid(row=8, column=2, sticky=E)
        self.organizacao.grid(row=9, column=2, sticky=E)
        self.senha.grid(row=10, column=2, sticky=E)
        self.confirmasenha.grid(row=11, column=2, sticky=E)

        self.cbTermosDeUso.grid(row=13, column=2, sticky=W)

        self.Bt1.grid(row=13, column=1, padx=35)
        self.btnVoltar.grid(row=14, column=2, padx=35)


        self.tratamento.grid(row=1, column=2, sticky=W)
        self.pais.grid(row=5, column=2, sticky=W)




    ##########################################################
    # Inserindo os valores para as variaveis do banco de Dados
    ##########################################################
    def validarCampos(self):
        if self.nome.get() == "":
            self.lbWarnings.configure(text = "o campo nome não deve ficar em branco", foreground="red")
            return False 

        email = self.email.get()
        if email == "":
            self.lbWarnings.configure(text = "o campo email não deve ficar em branco", foreground="red")
            return False

        senha = self.senha.get()
        if (senha == ''):
            self.lbWarnings.configure(text = "o campo senha não deve ficar em branco", foreground="red")
            return False     


        if not self.cbTermosDeUsoVar.get():
            self.lbWarnings.configure(text = "É necessario aceitar os termos de uso", foreground="red")
            return False 

        user = self.session.query(User).filter_by(emailUsuario=email, senha=senha).first()
        if (user != None):
            self.lbWarnings.configure(text = "email de usuário já cadastrado", foreground="red")
            return False


        return True    


    def cadastrar(self):
        
        if not (self.validarCampos()):
            return False   

        user = User()
        user.tratamento = self.tratamento.get()
        user.firstName = self.nome.get()
        user.lastName = self.sobrenome.get()
        user.emailUsuario = self.email.get()
        user.pais = self.pais.get()
        user.estado = self.estado.get()
        user.cidade = self.cidade.get()
        user.contatoTelefone = self.telefone.get()
        user.organizacaoVinculado = self.organizacao.get()
        user.senha = self.senha.get()   

        self.session.add(user)
        self.session.commit()

        self.lbWarnings.configure(text = "Senha cadastrada com sucesso!", foreground="black")


    def confirma_senha (self):
        senha = self.senha.get()
        contrasenha = self.confirmasenha.get()

        if senha == contrasenha:
            self.cadastrar()
            return True
        else:
            self.lbWarnings.configure(text = "Confirmação de senha incorreta!", foreground="red")
            return False

    def definirVoltarTelaLogin(self, onClickVoltar):
        self.btnVoltar["command"] = onClickVoltar

