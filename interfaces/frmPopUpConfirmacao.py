import tkinter as tk


class PopUpConfirmacao(tk.Toplevel):
    def __init__(self, master=None, callBack=None, mensagem=None):
        super().__init__(master)

        self.__callBack = callBack

        self.geometry("320x60+400+400")
        self.title("Confirmação")

        if mensagem == None:
            mensagem = "Você tem certeza que deseja deletar este item?"
        
        tk.Label(self, text=mensagem).pack()

        tk.Button(self, text='Sim', command=self.__simClick).pack(side=tk.RIGHT, fill=tk.BOTH, padx=70, pady=5)
        tk.Button(self, text='Não', command=self.__naoClick).pack(side=tk.RIGHT, fill=tk.BOTH, padx=5, pady=5)
    
    def __simClick(self):
        #try:
        self.__callBack(True)
        #except:
        #   self.__callBack = None     

        self.destroy()

    def __naoClick(self):
        try:
            self.__callBack(False)
        except:
           self.__callBack = None 
        self.destroy()
