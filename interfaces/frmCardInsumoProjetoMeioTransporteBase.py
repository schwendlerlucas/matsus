from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.constantesViews import *
from interfaces.frmBase import FrameBase
from interfaces.validacoesCampos import *

from models.projetoSistemaConstrutivoAtividadeTransporte import ProjetoSistemaConstrutivoAtividadeTransporte
from controllers.atividadeEquipamentoController import AtividadeEquipamentoController

from DTOs.cadProjetoDto import CadProjetoDTO

from controllers.equipamentoController import EquipamentoController

class FrameCardInsumoProjetoMeioTransporteBase(FrameBase):

    def __init__(self, parent, session, cadProjetoDTO: CadProjetoDTO=None):
        FrameBase.__init__(self, parent=parent, session=session)
        
        self.sistema_id = -1
        self.insumosList = []
        self.cadProjetoDTO = cadProjetoDTO
        self.__rowInEdit = ""
        self.__meioTransporteController = EquipamentoController(self.session)
        self.__meioTransporteList = self.__meioTransporteController.getMeiosDeTransporte()
        
        
        self.CreateUI()
        self.__beforeOpenEdicao = None


    def CreateUI(self):
        self.frameTitulo = Frame(master=self)
        self.frameTitulo.pack(side=TOP, fill=X)
        self.frameTreeViewEBotoes = Frame(master=self)
        self.frameTreeViewEBotoes.pack(side=TOP, fill=X, after=self.frameTitulo)

        self.frameBotoes = Frame(master=self.frameTreeViewEBotoes)
        self.frameBotoes.pack(side=RIGHT)


        self.frameTreeView = Frame(master=self.frameTreeViewEBotoes)
        self.frameEdicao = Frame(master=self)
        self.treeView = ttk.Treeview(master=self.frameTreeView, height=4)

        self.lbTitulo = Label(master=self.frameTitulo, text="Equipamentos")
        self.lbTitulo.pack(side=LEFT)


        self.scrollbarV = Scrollbar(master=self.frameTreeView)
        self.scrollbarV.pack(fill=Y, side=RIGHT)
        self.scrollbarV.config(command = self.treeView.yview)
        self.scrollbarH = Scrollbar(master=self.frameTreeView)
        self.scrollbarH.config(command = self.treeView.xview, orient=tk.HORIZONTAL)
        self.scrollbarH.pack(fill=X, side=BOTTOM)

        self.treeView.configure(xscrollcommand=self.scrollbarH.set,
                    yscrollcommand=self.scrollbarV.set)
    
        self.treeView.pack(expand=1, fill=BOTH, side=LEFT)
        self.frameTreeView.pack(side=LEFT, fill=BOTH, expand=1)

        self.btnEditar = Button(master=self.frameBotoes, text="Editar", width=5, comman=self.__btnEditarClick)
        self.btnEditar.pack(pady=3, padx=2)

        frameLabelsEdicao = Frame(self.frameEdicao)
        frameLabelsEdicao.pack(side=LEFT, pady=4)

        labelMeioTransporte = Label(frameLabelsEdicao, text="Meio de transporte", anchor='ne')
        labelMeioTransporte.pack(side=TOP, fill=X, pady=5)
        labelDistancia1 = Label(frameLabelsEdicao, text="Distância 1", anchor='ne')
        labelDistancia1.pack(side=TOP, fill=X, pady=5)
        labelDistancia2 = Label(frameLabelsEdicao, text="Distância 2", anchor='ne')
        labelDistancia2.pack(side=TOP, fill=X, pady=5)
        labelValorFrete = Label(frameLabelsEdicao, text="Valor Frete", anchor='ne')
        labelValorFrete.pack(side=TOP, fill=X, pady=5)

        frameCamposEdicao = Frame(self.frameEdicao)
        frameCamposEdicao.pack(side=LEFT, pady=8)

        self.cbbMeioTransporte = ttk.Combobox(frameCamposEdicao, state='readonly') 
        self.cbbMeioTransporte["values"] = self.__meioTransporteController.getEquipamentoNames(self.__meioTransporteList)
        self.cbbMeioTransporte.pack(side=TOP, fill=X, padx=5)
        
        self.edtDistancia1Var = StringVar()
        self.edtDistancia1 = Entry(frameCamposEdicao, width = "30", textvariable=self.edtDistancia1Var, justify=RIGHT)
        self.edtDistancia1.pack(side=TOP, fill=X, pady=6)

        self.edtDistancia2Var = StringVar()
        self.edtDistancia2 = Entry(frameCamposEdicao, width = "30", textvariable=self.edtDistancia2Var, justify=RIGHT)
        self.edtDistancia2.pack(side=TOP, fill=X, pady=5)

        self.edtValorFreteVar = StringVar()
        self.edtValorFrete = Entry(frameCamposEdicao, width = "30", textvariable=self.edtValorFreteVar, justify=RIGHT)
        self.edtValorFrete.pack(side=TOP, fill=X, pady=5)

        self.btnIncluir = Button(master=self.frameEdicao, text="ok", command=self.__btnOkEdicaoClick, width=2)
        self.btnIncluir.pack(side=RIGHT, padx=5, pady=5)

        self.treeView['columns'] = ('descricaoItem', 'qtdUnitaria', 'pesoUnitario', 
            'unidade', 'qtdTotal', 'pesoTotal', 'tipoTransporte', 
            'distancia1', 'distancia2', 'distanciaTotal', 'valorFrete')
        self.treeView.heading("#0", text='Atividade', anchor='w')
        self.treeView.column("#0", anchor='w', width=150)
        self.treeView.heading('descricaoItem', text='Nome', anchor='w')
        self.treeView.column('descricaoItem', anchor='w', width=150)
        self.treeView.heading('qtdUnitaria', text='Qtd. unitária', anchor='ne')
        self.treeView.column('qtdUnitaria', anchor='ne', width=100)
        self.treeView.heading('pesoUnitario', text='Peso unitario', anchor='ne')
        self.treeView.column('pesoUnitario', anchor='ne', width=100)
        self.treeView.heading('unidade', text='Und', anchor='center')
        self.treeView.column('unidade', anchor='center', width=70)
        self.treeView.heading('qtdTotal', text='Quant. total', anchor='ne')
        self.treeView.column('qtdTotal', anchor='ne', width=100)
        self.treeView.heading('pesoTotal', text='Peso total', anchor='ne')
        self.treeView.column('pesoTotal', anchor='ne', width=100)
        self.treeView.heading('tipoTransporte', text='Tipo transporte', anchor='w')
        self.treeView.column('tipoTransporte', anchor='w', width=150)
        self.treeView.heading('distancia1', text='Distância média(1)', anchor='ne')
        self.treeView.column('distancia1', anchor='ne', width=130)
        self.treeView.heading('distancia2', text='Distância média(2)', anchor='ne')
        self.treeView.column('distancia2', anchor='ne', width=130)
        self.treeView.heading('distanciaTotal', text='Distância total', anchor='ne')
        self.treeView.column('distanciaTotal', anchor='ne', width=130)
        self.treeView.heading('valorFrete', text='Valor Frete', anchor='ne')
        self.treeView.column('valorFrete', anchor='ne', width=130)

    def load(self, sistema_id: int):
        self.sistema_id = sistema_id
        self.queryInsumos()
        self.__loadTable()


    def queryInsumos(self):
        pass

    def __loadTable(self):
        for i in self.treeView.get_children():
            self.treeView.delete(i)
        
        for insumo in self.insumosList:
            self.treeView.insert('', END, text=insumo.atividade_nome, 
                                values=(insumo.insumo_nome, insumo.quantidade_unitaria,insumo.peso_unitario,
                                insumo.unidade, insumo.quantidade_total, insumo.peso_total, insumo.meio_transporte_nome,
                                insumo.distancia1, insumo.distancia2, 
                                insumo.distanciaTotal(), insumo.valorFrete))


    def __btnEditarClick(self):
        if self.__beforeOpenEdicao != None:
            self.__beforeOpenEdicao()

        curItem = self.treeView.focus()
        itemSelected = self.treeView.item(curItem)
        if itemSelected["text"] == "":
            return False

        self.__rowInEdit = itemSelected["text"] + "--" + itemSelected["values"][0] 

        self.cbbMeioTransporte.set(itemSelected["values"][6])
        self.edtDistancia1Var.set(itemSelected["values"][7])
        self.edtDistancia2Var.set(itemSelected["values"][8])
        self.edtValorFreteVar.set(itemSelected["values"][10])

        self.frameEdicao.pack(side=BOTTOM, fill=X)

    def __btnOkEdicaoClick(self):
        if not self.__validarCampos():
            return 

        try:
            itemInEdit = self.__pegarItemSelecionadoTreeView()

            itemInEdit.meio_transporte_nome = self.cbbMeioTransporte.get()
            itemInEdit.meio_transporte_id = self.__meioTransporteController.getEquipamentoId(
                itemInEdit.meio_transporte_nome, self.__meioTransporteList)
            
            itemInEdit.distancia1 = self.edtDistancia1.get()
            itemInEdit.distancia2 = self.edtDistancia2.get()  
            itemInEdit.valorFrete = self.edtValorFrete.get()
        finally:
            self.__loadTable() 
            self.frameEdicao.pack_forget()

    def __validarCampos(self):
        if not validarDouble(self.edtDistancia1.get()):
            return False

        if not validarDouble(self.edtDistancia2.get()):
            return False

        if not validarDouble(self.edtValorFrete.get()):
            return False
        
        if (self.cbbMeioTransporte.get() == "") and (self.cbbMeioTransporte.get() == "None"):
            return False

        return True


    def limparCamposEdicao(self):
        pass

    def __pegarItemSelecionadoTreeView(self):
        for item in self.insumosList:
            if (item.atividade_nome + "--" + item.insumo_nome) == self.__rowInEdit:
                return item 


    def definirDescricao(self, descricao):
        self.treeView.heading('descricaoItem', text=descricao, anchor='w')
        self.lbTitulo.config(text=descricao)

    def recolherCamposEdicao(self):
        self.frameEdicao.pack_forget()
    
    def setBeforeOpenEdicao(self, beforeOpenEdicao):
        self.__beforeOpenEdicao = beforeOpenEdicao

    def definirHeightTreeView(self, height):
        self.treeView["height"] = height

    def setListOnCadProjetoDTO(self):
        pass
    
