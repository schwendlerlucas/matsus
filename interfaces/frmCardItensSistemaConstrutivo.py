from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.constantesViews import *
from interfaces.frmCardItensBase import FrameCardItensBase
from interfaces.validacoesCampos import *
from interfaces.interfaceUtils import aplicarMascaraE, aplicarMascaraRS

from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaterial import AtividadeMaterial
from models.atividadeMaoDeObra import AtividadeMaoDeObra
from models.custo import Custo

from interfaces.calculoIndicadores.calculoIndicadoresSoma import CalculoIndicadoresSoma
from interfaces.calculoIndicadores.calculoInidicadoresResult import CalculoIndicadoresResult
from interfaces.calculoIndicadores.calculoIndicadoresPorItem import CalculoIndicadoresPorItem



class FrameCardItensSistemaConstrutivo(FrameCardItensBase):

    def __init__(self, parent, session, model, modelAtividade):
        super(FrameCardItensSistemaConstrutivo, self).__init__(parent, session=session, model=model, modelAtividade=modelAtividade)
        
        self.__listaItensCalculados = []
        self.__listaAtividadesCalculadas = []
        self.__estado = ""

    def CreateUI(self):
        super(FrameCardItensSistemaConstrutivo, self).CreateUI()

        self.__configurarColunasTreeView()
        

        self.scrollbarH = Scrollbar(master=self.frameTreeView)
        self.scrollbarH.config(command = self.treeView.xview, orient=tk.HORIZONTAL)
        self.scrollbarH.pack(fill=X, side=BOTTOM)

        self.treeView.configure(xscrollcommand=self.scrollbarH.set,
                    yscrollcommand=self.scrollbar.set)

        self.treeView.pack_forget()
        self.treeView.pack(expand=1, fill=BOTH, side=LEFT)
        
        framelbAtividade = Frame(self.frameEdicao)
        framelbAtividade.pack(side=TOP, fill=X)

        lbAtividade = Label(master=framelbAtividade, text="Atividade")
        lbAtividade.pack(side=LEFT)

        frameItemClase = Frame(self.frameEdicao)
        frameItemClase.pack(side=TOP, fill=X) 

        self.cbbItemClasse = ttk.Combobox(master=frameItemClase, state='readonly', width=34) 
        self.cbbItemClasse.pack(side=LEFT, padx=5)
        self.cbbItemClasse.bind('<<ComboboxSelected>>', self.onChangeItemClasse)
        
        framelbQuantidade = Frame(self.frameEdicao)
        framelbQuantidade.pack(side=TOP, fill=X)
        
        lbQuantidade = Label(master=framelbQuantidade, text="Quantidade da atividade empregado por em 1m² de sistema construtivo")
        lbQuantidade.pack(side=LEFT)

        frameedtQuantidade = Frame(self.frameEdicao)
        frameedtQuantidade.pack(side=TOP, fill=X)

        self.edtQuantidadeVar = StringVar()
        self.edtQuantidade = Entry(master=frameedtQuantidade, width = "10", textvariable=self.edtQuantidadeVar)
        self.edtQuantidade.pack(side=LEFT, padx=2)
        
        framebtnIncluir = Frame(self.frameEdicao)
        framebtnIncluir.pack(side=TOP, fill=X)

        self.btnIncluir = Button(master=framebtnIncluir, text="ok", command=self.__btnOkEdicaoClick, width=2)
        self.btnIncluir.pack(side=LEFT, padx=2, pady=5)


    def __btnOkEdicaoClick(self):
        super(FrameCardItensSistemaConstrutivo, self).btnOkEdicaoClick()


    def LoadTable(self):
        super(FrameCardItensSistemaConstrutivo, self).LoadTable()
        self.__listaAtividadesCalculadas = []

        for item in self.listaItens:
            self.__listaItensCalculados = []
            equipamentos = self.session.query(AtividadeEquipamento).\
                filter(AtividadeEquipamento.atividade_id == item.pegarObjetoFk().id ).all()  

            for itemAtividade in equipamentos:
                self.__calcularPorItem(itemAtividade.equipamento, itemAtividade.quantidade)
                

            materiais = self.session.query(AtividadeMaterial).\
                filter(AtividadeMaterial.atividade_id == item.pegarObjetoFk().id).all()  

        
            for itemAtividade in materiais:
                self.__calcularPorItem(itemAtividade.material, itemAtividade.quantidade)

        
            calculoSoma = CalculoIndicadoresSoma(self.__listaItensCalculados) 
            soma = calculoSoma.Calcular(item.quantidade)

            soma.custo = self.__calcularCustoPorAtividade(item.pegarObjetoFk(), item.quantidade)
            soma.grauSalubridade = item.pegarObjetoFk().grauSalubridade
            soma.grauSeguridade = item.pegarObjetoFk().grauSeguridade

            self.__listaAtividadesCalculadas.append(soma)

            self.treeView.insert('', END, text=item.pegarObjetoFk().id, 
                                values=(item.pegarObjetoFk().nome,
                                    item.quantidade,
                                    aplicarMascaraE(soma.emiEfeitoEstufaGWP),
                                    aplicarMascaraE(soma.emiDegradamOzonioODP),
                                    aplicarMascaraE(soma.emiChuvaAcidaAP),
                                    aplicarMascaraE(soma.emiToxicosPatogenicosEP),
                                    aplicarMascaraE(soma.emiContribEutroficacaoPOCP),
                                    aplicarMascaraE(soma.qtdResiduosPerigosos),
                                    aplicarMascaraE(soma.qtdResiduosRadioativos),
                                    aplicarMascaraE(soma.qtdResiduosNaoPerigosos),
                                    aplicarMascaraE(soma.qtdEnergiaNaoRenovavel),
                                    aplicarMascaraE(soma.qtdEnergiaRenovavel),
                                    aplicarMascaraE(soma.qtdAguaRedeAbastecimento),
                                    aplicarMascaraE(soma.qtdAguaReutilizada),
                                    aplicarMascaraRS(soma.custo),
                                    aplicarMascaraE(soma.grauSalubridade),
                                    aplicarMascaraE(soma.grauSeguridade)))
        self.__adicionarTotal()


    def __adicionarTotal(self):
        if len(self.__listaAtividadesCalculadas) == 0:
            return

        calculoSoma = CalculoIndicadoresSoma(self.__listaAtividadesCalculadas) 
        soma = calculoSoma.Calcular()

        self.treeView.insert('', END, text="", values=())
        
        self.treeView.insert('', END, text="(+)",
                                values=("Totais (Relativos) Sistema Construtivo/M²",
                                    "",
                                    aplicarMascaraE(soma.emiEfeitoEstufaGWP),
                                    aplicarMascaraE(soma.emiDegradamOzonioODP),
                                    aplicarMascaraE(soma.emiChuvaAcidaAP),
                                    aplicarMascaraE(soma.emiToxicosPatogenicosEP),
                                    aplicarMascaraE(soma.emiContribEutroficacaoPOCP),
                                    aplicarMascaraE(soma.qtdResiduosPerigosos),
                                    aplicarMascaraE(soma.qtdResiduosRadioativos),
                                    aplicarMascaraE(soma.qtdResiduosNaoPerigosos),
                                    aplicarMascaraE(soma.qtdEnergiaNaoRenovavel),
                                    aplicarMascaraE(soma.qtdEnergiaRenovavel),
                                    aplicarMascaraE(soma.qtdAguaRedeAbastecimento),
                                    aplicarMascaraE(soma.qtdAguaReutilizada),
                                    aplicarMascaraRS(soma.custo),
                                    aplicarMascaraE(soma.grauSalubridade),
                                    aplicarMascaraE(soma.grauSeguridade)))

    def __calcularPorItem(self, item, quantidade):
        calculator = CalculoIndicadoresPorItem(item)
        itemCalculado = calculator.Calcular(quantidade)
        self.__listaItensCalculados.append(itemCalculado)

    def updateItem(self, item):
        super(FrameCardItensSistemaConstrutivo, self).updateItem(item)
        item.quantidade = self.edtQuantidadeVar.get()       

    
    def updateComponents(self, item):
        super(FrameCardItensSistemaConstrutivo, self).updateComponents(item)

        self.edtQuantidadeVar.set(item.quantidade)


    def validarCampos(self):
        super(FrameCardItensSistemaConstrutivo, self).limparCamposEdicao()
        if self.cbbItemClasse.get() == "":
            return False
        
        if self.edtQuantidade.get() == "":
            return False
        
        
        return True
        
    def definirIdPaiNoItem(self, item, id):
        super(FrameCardItensSistemaConstrutivo, self).definirIdPaiNoItem(item, id)

        item.sistemaConstrutivo_id = id
        
    def consultar(self, id, estado):
        super(FrameCardItensSistemaConstrutivo, self).consultar(id)

        self.listaItens = self.session.query(self.modelAtividade).\
            filter(self.modelAtividade.sistemaConstrutivo_id == id).all()  
        
        self.__estado = estado

        self.LoadTable() 

    def __configurarColunasTreeView(self):
        self.treeView['columns'] = ('descicao', 'quantidade', 'totalGWP', 'totalODP',\
            'totalAP', 'totalEP', 'totalPOCP', 'totalResidPerigoso',\
            #'totalIntensResidPerigoso',
            'totalResidRadioativo',\
            #'totalIntensResidRadioativo', 
            'totalResidNaoPerigoso',\
            'energiaNaoRenovavel', 'energiaRenovavel', 'aguaRede', 'aguaReuso', 'custo',
            'grauSalubridade', 'grauSeguridade')
        self.treeView.heading("#0", text='Código ', anchor='w')
        self.treeView.column("#0", anchor='n', width=100)
        self.treeView.heading('descicao', text='Atividade', anchor='w')
        self.treeView.column('descicao', anchor='w', width=260)

        self.treeView.heading('quantidade', text='Quantidade por 1m²', anchor='w')
        self.treeView.column('quantidade', anchor='ne', width=160)
        
        self.treeView.heading('totalGWP', text='GWP')
        self.treeView.column('totalGWP', anchor='ne', width=80)

        self.treeView.heading('totalODP', text='ODP')
        self.treeView.column('totalODP', anchor='ne', width=80)

        self.treeView.heading('totalAP', text='AP')
        self.treeView.column('totalAP', anchor='ne', width=80)

        self.treeView.heading('totalEP', text='EP')
        self.treeView.column('totalEP', anchor='ne', width=80)

        self.treeView.heading('totalPOCP', text='POCP')
        self.treeView.column('totalPOCP', anchor='ne', width=80)

        self.treeView.heading('totalResidPerigoso', text='Residuos Perigosos')
        self.treeView.column('totalResidPerigoso', anchor='ne', width=150)

        #self.treeView.heading('totalIntensResidPerigoso', text='Intensidade')
        #self.treeView.column('totalIntensResidPerigoso', anchor='ne', width=120)

        self.treeView.heading('totalResidRadioativo', text='Residuos Raioativos')
        self.treeView.column('totalResidRadioativo', anchor='ne', width=150)

        #self.treeView.heading('totalIntensResidRadioativo', text='Intensidade')
        #self.treeView.column('totalIntensResidRadioativo', anchor='ne', width=120)

        self.treeView.heading('totalResidNaoPerigoso', text='Residuos não perigosos')
        self.treeView.column('totalResidNaoPerigoso', anchor='ne', width=170)

        self.treeView.heading('energiaNaoRenovavel', text='Energia Não Renovável')
        self.treeView.column('energiaNaoRenovavel', anchor='ne', width=170)

        self.treeView.heading('energiaRenovavel', text='Energia Renonável')
        self.treeView.column('energiaRenovavel', anchor='ne', width=140)

        self.treeView.heading('aguaRede', text='Água da Rede de Abastecimento')
        self.treeView.column('aguaRede', anchor='ne', width=250)

        self.treeView.heading('aguaReuso', text='Água de Reuso')
        self.treeView.column('aguaReuso', anchor='ne', width=120)

        self.treeView.heading('custo', text='Custo por m²')
        self.treeView.column('custo', anchor='ne', width=150)

        self.treeView.heading('grauSalubridade', text='Grau de salubridade')
        self.treeView.column('grauSalubridade', anchor='ne', width=150)

        self.treeView.heading('grauSeguridade', text='Grau de seguridade')
        self.treeView.column('grauSeguridade', anchor='ne', width=150)

    def __calcularCustoPorAtividade(self, atividade, quantidadeAtividade):
        soma = 0.0

        equipamentos = self.session.query(AtividadeEquipamento).\
            filter(AtividadeEquipamento.atividade_id == atividade.id).all()  
        for item in equipamentos:
            custo = self.session.query(Custo).filter(Custo.equipamento_id==item.equipamento_id, Custo.estado==self.__estado).first()
            if custo != None:
                soma += custo.custoMedio * item.quantidade

        materiais = self.session.query(AtividadeMaterial).\
            filter(AtividadeMaterial.atividade_id == atividade.id).all()  
        for item in materiais:
            custo = self.session.query(Custo).filter(Custo.material_id==item.material_id, Custo.estado==self.__estado).first()
            if custo != None:
                soma += custo.custoMedio * item.quantidade

        maosDeObra = self.session.query(AtividadeMaoDeObra).\
            filter(AtividadeMaoDeObra.atividade_id == atividade.id).all()  
        for item in maosDeObra:
            custo = self.session.query(Custo).filter(Custo.maoDeObra_id==item.maoDeObra_id, Custo.estado==self.__estado).first()
            if custo != None:
                soma += (custo.custoMedio * item.quantidade) * (atividade.encargoSocial/100) 


        return soma * float(quantidadeAtividade)


    
    