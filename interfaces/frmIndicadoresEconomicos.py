# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime

from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaterial import AtividadeMaterial
from models.atividadeMaoDeObra import AtividadeMaoDeObra
from models.custo import Custo
from interfaces.frmCardIndicadoresEconomicos import FrameCardIndicadoresEconomicos


from interfaces.frmCardItensSistemaConstrutivo import FrameCardItensSistemaConstrutivo


class FrameIndicadoresEconomicos(FrameBase):
    
    def __init__(self, parent=None, session=None, atividade=None):
        super(FrameIndicadoresEconomicos, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__atividadeDto = atividade

        self.__listaItensCalculados = []
        
        parent.title("Calculos relativos para a dimensão econômica")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("810x750+100+50")
        self.__createUI()

        self.__carregarAtividade()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        labelTitulo = Label(self.frameTop, text="Indicadores Economicos", font=14, height="3")
        labelTitulo.pack(side=LEFT)

        frmCampo = Frame(self.frameTop)
        frmCampo.pack(side=BOTTOM)

        self.labelAtividade = Label(frmCampo, text="Pesquise por atividade", font=10, height="2")
        self.labelAtividade.pack(side=TOP, pady=25)

        frmLabels = Frame(frmCampo, width=1)
        frmLabels.pack(side=LEFT)
        labelLocal = Label(frmLabels, text="Estado para a composição de preços")
        labelLocal.pack(side=BOTTOM)

        frmEntries = Frame(frmCampo, width=30)
        frmEntries.pack(side=RIGHT, pady=5)

        self.cbbLocal = ttk.Combobox(frmEntries, state='readonly')
        self.cbbLocal["values"] = LISTA_ESTADOS
        self.cbbLocal.bind('<<ComboboxSelected>>', self.__onChangeClasse)
        self.cbbLocal.current(0)
        self.cbbLocal.pack(side=BOTTOM, padx=5)

        btnBugImagem = Button(command=self.__onclickButtonBug)

        self.frmTreeViews = Frame()
        self.frmTreeViews.pack(side=BOTTOM, fill=X, expand=1)

        
        self.treeViewMaterial = FrameCardIndicadoresEconomicos(self.frmTreeViews, self.session, None, AtividadeMaterial, Custo.material_id)
        self.treeViewMaterial.pack(side=TOP, fill=X)
        self.treeViewMaterial.definirHeightTreeView(2)
        self.treeViewMaterial.definirDescricao('Materiais')
        self.treeViewMaterial.DefinirDescricaoTotal('TOTAIS por m² fase (creadle to gate)')  

        self.treeViewEquipamento = FrameCardIndicadoresEconomicos(self.frmTreeViews, self.session, None, AtividadeEquipamento, Custo.equipamento_id)
        self.treeViewEquipamento.pack(side=TOP, fill=X)
        self.treeViewEquipamento.definirHeightTreeView(2)
        self.treeViewEquipamento.definirDescricao('Equipamentos')
        self.treeViewEquipamento.DefinirDescricaoTotal('TOTAL')

        self.treeViewMaoDeObra = FrameCardIndicadoresEconomicos(self.frmTreeViews, self.session, None, AtividadeMaoDeObra, Custo.maoDeObra_id)
        self.treeViewMaoDeObra.pack(side=TOP, fill=X)
        self.treeViewMaoDeObra.definirHeightTreeView(2)
        self.treeViewMaoDeObra.definirDescricao('Mão de Obra')
        self.treeViewMaoDeObra.DefinirDescricaoTotal('Total')

        self.treeViewTotais = FrameCardIndicadoresEconomicos(self.frmTreeViews, self.session)
        self.treeViewTotais.pack(side=TOP, fill=X)
        self.treeViewTotais.definirHeightTreeView(3)
        self.treeViewTotais.definirDescricao('Totais')
        self.treeViewTotais.DefinirDescricaoTotal('TOTAIS por m² da Atividade') 
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10)
        self.btnVoltar.pack(side=LEFT, padx=15, pady=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15, pady=15)

        
    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick
    

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick

    def __onclickButtonBug(self):
        pass

    def __carregarAtividade(self):
        if self.__atividadeDto == None:
            return
     
        self.labelAtividade.config(text=self.__atividadeDto.nome)

        self.treeViewMaterial.DefinirAtividade(self.__atividadeDto, self.cbbLocal.get())
        self.treeViewEquipamento.DefinirAtividade(self.__atividadeDto, self.cbbLocal.get())
        self.treeViewMaoDeObra.DefinirAtividade(self.__atividadeDto, self.cbbLocal.get())

        percentualEncargoSocial = float(self.__atividadeDto.encargoSocial)

        total = self.treeViewMaoDeObra.custoTotal * (percentualEncargoSocial/100)

        self.treeViewTotais.LimparValores()
        self.treeViewTotais.InserirTotalEncargoSocial(str(percentualEncargoSocial)+"%", round(total,2))

        total += self.treeViewEquipamento.custoTotal

        self.treeViewTotais.ExibirTotais(round(total,2), "TOTAIS por m² fase (Aplicação)")

        total += self.treeViewMaterial.custoTotal

        self.treeViewTotais.ExibirTotais(round(total,2))
        
    def __onChangeClasse(self, event):
        self.__carregarAtividade()





        


        





