from tkinter import *
from interfaces.frmBase import FrameBase
import tkinter.font as font

class FrameMenu(FrameBase):
    
    def __init__(self, parent=None, session=None):
        super(FrameMenu, self).__init__(parent=parent, session=session)

        self.clearParent()

        parent.title("Menu")
        parent.geometry ("910x510+100+100")

        frameLeft = Frame()
        frameLeft.grid(row=0, column=0)

        labelDB = Label(frameLeft, text="Banco de dados  (BD)")
        labelDB.grid(row=0, column=1, sticky=W)

        self.btnCadMaterial = Button(frameLeft, text="Cadastro de Materiais", padx=15, width=25, command=self.__defaultClick)
        self.btnCadMaterial.grid(row=1, column=1, pady=5, padx=5)

        self.btnCadEquipamentos = Button(frameLeft, text="Cadastro de Equipamentos", padx=15, width=25)
        self.btnCadEquipamentos.grid(row=2, column=1, pady=5, padx=5)

        self.btnCadMaoDeObra = Button(frameLeft, text="Cadastro de Mão de Obra", padx=15, width=25)
        self.btnCadMaoDeObra.grid(row=3, column=1, pady=5, padx=5)
        
        labelManipulacao = Label(frameLeft, text="Manipulação de dados e Composições\n(MDCP)")
        labelManipulacao.grid(row=4, column=1, sticky=W, pady=6)

        self.btnCadCusto = Button(frameLeft, text="Cadastro de Custos Unitários", padx=15, width=25)
        self.btnCadCusto.grid(row=5, column=1, pady=5, padx=5)

        self.btnCadAtividade = Button(frameLeft, text="Cadastro de Atividades", 
            padx=15, width=25)
        self.btnCadAtividade.grid(row=6, column=1, pady=5, padx=5)

        labelCalculosIndicativos = Label(frameLeft, text="Calculos de Indicadores Relativos para\nAtividades (IRA)")
        labelCalculosIndicativos.grid(row=7, column=1, sticky=W, pady=6)

        self.btnIndicadoresAmbientais = Button(frameLeft, text="Indicadores Ambientais", 
            padx=15, width=25)
        self.btnIndicadoresAmbientais.grid(row=8, column=1, pady=5, padx=5)

        self.btnIndicadoresEconomicos= Button(frameLeft, text="Indicadores Econômicos", 
            padx=15, width=25)
        self.btnIndicadoresEconomicos.grid(row=9, column=1, pady=5, padx=5)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.label = Label(image = self.photo)
        self.label.grid(row=0, column=1, columnspan=4, sticky=E)

        frameRight = Frame()
        frameRight.grid(row=0, column=5, columnspan=6)

        labelSistema = Label(frameRight, text="Sistemas Construtivos")
        labelSistema.grid(row=0, column=1, sticky=W)

        self.btnSistemaConstrutivo = Button(frameRight, text="Composição de Sistema Construtivo", 
            padx=15, width=25)
        self.btnSistemaConstrutivo.grid(row=1, column=1, pady=5, padx=5)

        labelProjeto = Label(frameRight, text="Análise de Projeto (AP)")
        labelProjeto.grid(row=2, column=1, sticky=W, pady=6)

        self.btnCadProjeto = Button(frameRight, text="Cadastro de Projeto", 
        padx=15, width=25)
        self.btnCadProjeto.grid(row=3, column=1, pady=5, padx=5)

        labelIndicadoresSistema = Label(frameRight, text="Visualizar Indicadores Totais para os\nSistemas Construtivos Cadastrados (ITSC)")
        labelIndicadoresSistema.grid(row=4, column=1, sticky=W, pady=6)

        self.btnIndicadoresSistema= Button(frameRight, text="Indicadores de Projeto", 
            padx=15, width=25)
        self.btnIndicadoresSistema.grid(row=5, column=1, pady=5, padx=5)

        self.btnIndicadoresTransporte= Button(frameRight, text="Indicadores de Transportes", 
            padx=15, width=25)
        self.btnIndicadoresTransporte.grid(row=6, column=1, pady=5, padx=5)

        labelIndicadoresSistema = Label(frameRight, text="Pesos")
        labelIndicadoresSistema.grid(row=7, column=1, sticky=W, pady=6)   
           
        self.btnFatoresDecisao= Button(frameRight, text="Fatores de Decisão", 
            padx=15, width=25)
        self.btnFatoresDecisao.grid(row=8, column=1, pady=5, padx=5)   

        self.btnIndicadoresSistemaComPesos= Button(frameRight, text="Categorias Ponderadas", 
            padx=15, width=25)
        self.btnIndicadoresSistemaComPesos.grid(row=9, column=1, pady=5, padx=5)   

        self.btnDesempenhoSustentavel= Button(frameRight, text="Desempenho Sustentável", 
            padx=15, width=25)
        self.btnDesempenhoSustentavel.grid(row=10, column=1, pady=5, padx=5)   

        #canvas = Canvas(width=900)
        #canvas.create_line(0, 25, 900, 25, fill="green")
        #canvas.grid(row=2, column=0, columnspan=6)

        metodoStr = 'Método de apoio à tomada de decisão para a seleção de materiais mais sustentáveis aplicados à habitações rurais'
        metodoStr += ' - módulo: técnicas cosntrutivasa a base\n de solo / Desenvolvimento educacional no âmbito da pesquisa de doutorado entitulada:'
        metodoStr += ' "O potencial do uso de materiais alternativos a base de solo para    \n edificações rurais",'
        metodoStr += ' de autoria de Christian Souza Barboza sob a orientação e coorientação dos professores Douglas Barreto e Ricardo Mateus                    '

        frameBottom = Frame()
        frameBottom.grid(row=3, column=0, columnspan=6)

        myFont = font.Font(size=7)
        labelMetodo = Label(frameBottom, text=metodoStr)
        labelMetodo["font"] = myFont
        labelMetodo.grid(row=0, column=1, columnspan=5, sticky=E)

        self.photoApoio = PhotoImage(file = "./interfaces/imagens/apoio.png")
        
        frameImagemApoio = Frame(frameBottom)
        frameImagemApoio.grid(row=0, column=6, columnspan=2)

        self.labelApoio = Label(frameImagemApoio, image=self.photoApoio)
        self.labelApoio.grid(row=0, column=0, sticky=E)


    def __defaultClick(self):
        pass

    def definirAbrirCadMaoObra(self, onClick):
        self.btnCadMaoDeObra["command"] = onClick

    def definirAbrirCadMaterial(self, onClick):
        self.btnCadMaterial["command"] = onClick
        
    def definirAbrirCadAtividade(self, onClick):
        self.btnCadAtividade["command"] = onClick

    def definirAbrirCadEquipamentos(self, onClick):
        self.btnCadEquipamentos["command"] = onClick

    def definitAbrirCadCusto(self, onClick):
        self.btnCadCusto["command"] = onClick

    def definitAbrirCadSistemaConstrutivo(self, onClick):
        self.btnSistemaConstrutivo["command"] = onClick

    def definitAbrirIndicadoresAmbientais(self, onClick):
        self.btnIndicadoresAmbientais["command"] = onClick

    def definitAbrirIndicadoresEconomicos(self, onClick):
        self.btnIndicadoresEconomicos["command"] = onClick
    
    def definirAbrirCadProjeto(self, onClick):
        self.btnCadProjeto["command"] = onClick

    def definirAbrirIndicadoresSistema(self, onClick):
        self.btnIndicadoresSistema["command"] = onClick        

    def definirAbrirCadFatoresDecisao(self, onClick):
        self.btnFatoresDecisao["command"] = onClick
        
    def definirAbrirIndicadoresSistemaComPesos(self, onClick):
        self.btnIndicadoresSistemaComPesos["command"] = onClick

    def definirAbrirDefinirDesempenhoSustentavel(self, onClick):
        self.btnDesempenhoSustentavel["command"] = onClick

    def definirAbrirIndicadoresDeTransporte(self, onClick):
        self.btnIndicadoresTransporte["command"] = onClick