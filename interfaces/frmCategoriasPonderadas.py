# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.interfaceUtils import aplicarMascaraE, aplicarMascaraPerc
from datetime import datetime

from models.projeto import Projeto

from DTOs.cadProjetoDto import CadProjetoDTO

from controllers.projetoSistemasNormatizadoController import ProjetoSistemasNormatizadoController


class FrameCategoriasPonderadas(FrameBase):
    
    def __init__(self, parent=None, session=None, cadProjetoDTO:CadProjetoDTO=None):
        super(FrameCategoriasPonderadas, self).__init__(parent=parent, session=session)

        self.clearParent()
       
        self.__projetoDto = None
        self.__cadProjetoDTO = cadProjetoDTO
        if self.__cadProjetoDTO != None:
            self.__projetoDto = cadProjetoDTO.projeto

        self.__proximaTela = None
        self.__voltarTelaAnterior = None
        self.__openMenu = None
        self.__listaResultados = []
        self.__listaResultadosNormatizados = []
        self.__OpenGraficos = None
        
        parent.title("Categorias ponderadas")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1200x800+100+20")
        self.__createUI()

        self.__consultarSistemas()
        self.__configurarTreeView()
        self.__carregarValorCampos()
        self.__loadResult()

        

    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Categorias ponderadas", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frameCampos = Frame()
        frameCampos.pack(side=TOP)

        frmLabels = Frame(frameCampos, width=1)
        frmLabels.pack(side=LEFT)
        labelProjeto = Label(frmLabels, text="Nome do Projeto ", anchor='ne')
        labelProjeto.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Quantidade total ", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Estado de referência para custo", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)


        frmEntries = Frame(frameCampos, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtProjetoVar = StringVar()
        self.edtProjeto = Entry(frmEntries, width = "30", textvariable=self.edtProjetoVar, state='readonly')
        self.edtProjeto.pack(side=TOP, fill=X, pady=5)

        self.edtQuantidadeVar = StringVar()
        self.edtQuantidade = Entry(frmEntries, width = "30", textvariable=self.edtQuantidadeVar, state='readonly')
        self.edtQuantidade.pack(side=TOP, fill=X, pady=5)
        if self.__projetoDto != None:
            self.edtQuantidadeVar.set(self.__projetoDto.qtdSistemaConstrutivo)

        self.edtLocalVar = StringVar()
        self.edtLocal = Entry(frmEntries, width = "30", textvariable=self.edtLocalVar, state='readonly')
        self.edtLocal.pack(side=TOP,fill=X, pady=5)
        if self.__projetoDto != None:
            self.edtLocalVar.set(self.__projetoDto.estado)



        self.frmTreeView = Frame()
        self.frmTreeView.pack(fill=BOTH, side=TOP, expand=1)

        frmTreeViewScrollBar = Frame(self.frmTreeView)
        frmTreeViewScrollBar.pack(side=TOP, fill=BOTH, expand=1, padx=5)

        self.treeView = ttk.Treeview(master=frmTreeViewScrollBar, height=4)

        self.scrollbarV = Scrollbar(master=frmTreeViewScrollBar)
        self.scrollbarV.pack(fill=Y, side=RIGHT, pady=15)
        self.scrollbarV.config(command = self.treeView.yview)

        self.scrollbarH = Scrollbar(master=frmTreeViewScrollBar)
        self.scrollbarH.config(command = self.treeView.xview, orient=tk.HORIZONTAL)
        self.scrollbarH.pack(fill=X, side=BOTTOM)

        self.treeView.configure(xscrollcommand=self.scrollbarH.set,
                    yscrollcommand=self.scrollbarV.set)

        #self.treeView.configure(yscrollcommand=self.scrollbarV.set, height=15)
        self.treeView.pack(pady=15, fill=BOTH, expand=1)
        
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10, command=self.__btnVoltarOnClick)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15)
        #self.btnGraficos = Button(frmBotoes, text="Graficos", width=10, command=self.__bntGraficosOnClick)
        #self.btnGraficos.pack(side=LEFT, padx=15)
        self.btnOpenMenu = Button(frmBotoes, text="Menu", width=20, command=self.__menuOnClick)
        self.btnOpenMenu.pack(side=RIGHT, padx=15)



    def __menuOnClick(self):
        self.__openMenu()
            

    def __btnVoltarOnClick(self):        
        self.__voltarTelaAnterior()

    def definirVoltar(self, voltarTelaAnterior):
        self.__voltarTelaAnterior = voltarTelaAnterior

    def __carregarValorCampos(self):
        if self.__cadProjetoDTO == None:
            return 
        self.edtProjetoVar.set(self.__projetoDto.nome)

    def definirTelaMenu(self, openMenu):
        self.__openMenu = openMenu
        
    def __loadResult(self):
        for i in self.treeView.get_children():
            self.treeView.delete(i)

        if self.__cadProjetoDTO == None:
            return

        segundoNivel = "     "

        self.treeView.insert('', END, text="Ambiental", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "AI", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].AI)))                             
        self.treeView.insert('', END, text=segundoNivel + "AII", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].AII)))  
        self.treeView.insert('', END, text=segundoNivel + "AIII", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].AIII)))  
        self.treeView.insert('', END, text=segundoNivel + "AIV", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].AIV)))  

        self.treeView.insert('', END, text="Social", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "SI", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].SI)))                             
        self.treeView.insert('', END, text=segundoNivel + "SII", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].SII)))  
        self.treeView.insert('', END, text=segundoNivel + "SIII", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].SIII)))  
        self.treeView.insert('', END, text=segundoNivel + "SIV", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].SIV)))                                  

        self.treeView.insert('', END, text="Economica", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "EI", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].EI)))                             
        self.treeView.insert('', END, text=segundoNivel + "EII", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].EII)))

        self.treeView.insert('', END, text="Técnica", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "TI", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].TI)))                             
        self.treeView.insert('', END, text=segundoNivel + "TII", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].TII)))  
        self.treeView.insert('', END, text=segundoNivel + "TIII", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].TIII)))  
        self.treeView.insert('', END, text=segundoNivel + "TIV", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].TIV)))                               
        self.treeView.insert('', END, text=segundoNivel + "TV", 
                            values=(self.__retornarArrayValores(lambda i : self.__listaResultadosNormatizados[i].TV)))                             
                                                                                                                

    def __configurarTreeView(self):
        listaColunas =[]
        for i in range(0, len(self.__listaResultados)):
            listaColunas.append('sistema' + str(i))

        self.treeView['columns'] = listaColunas
        self.treeView.heading("#0", text=' Resultados', anchor='w')
        self.treeView.column("#0", anchor='w', width=200)

        for i in range(0, len(self.__listaResultados)):
            self.treeView.heading('sistema' + str(i), text=self.__listaResultados[i].sistema_nome, anchor='ne')
            self.treeView.column('sistema' + str(i), anchor='ne', width=200)
        

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick

    def __retornarArrayValores(self, getValor):
        result = []
        for i in range(0, len(self.__listaResultadosNormatizados)):
            result.append(aplicarMascaraPerc(getValor(i)))

        return result

    def __consultarSistemas(self):
        if self.__projetoDto == None:
            return

        controller = ProjetoSistemasNormatizadoController(self.session, self.__projetoDto.id, self.__projetoDto.estado)
        controller.Calc()

        self.__listaResultados = controller.GetListaResultados()
        self.__listaResultadosNormatizados = controller.GetListaResultadosNormatizados()

    def __bntGraficosOnClick(self):
        if self.__cadProjetoDTO == None:
            return

        if self.__OpenGraficos == None:
            return

        self.__OpenGraficos(self.__cadProjetoDTO)

    def definirOpenGraficos(self, openGraficos):
        self.__OpenGraficos = openGraficos