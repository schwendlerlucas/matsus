# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.interfaceUtils import aplicarMascaraRS
from datetime import datetime

from models.custo import Custo


from interfaces.frmCardItensSistemaConstrutivo import FrameCardItensSistemaConstrutivo


class FrameCardIndicadoresEconomicos(FrameBase):
    
    def __init__(self, parent=None, session=None, atividade=None, modelAtividadeItem=None, filtro=None):
        super(FrameCardIndicadoresEconomicos, self).__init__(parent=parent, session=session)

        self.__atividadeDto = atividade
        self.__modelAtividadeItem = modelAtividadeItem

        self.custoTotal = 0.0 
        self.__descricaoTotal = ""
        self.__filtro = filtro

        self.__createUI()

    def __createUI(self):
        frameTitulo = Frame()
        frameTitulo.pack(side=TOP, fill=X, padx=5, pady=5)

        self.lbTitulo = Label(master=frameTitulo, text="Titulo")
        self.lbTitulo.pack(side=LEFT)

        self.frmTreeView = Frame()
        self.frmTreeView.pack(side=TOP, fill=X, expand=1, padx=5, pady=5)
        
        self.treeView = ttk.Treeview(master=self.frmTreeView, height=4)

        self.scrollbarV = Scrollbar(master=self.frmTreeView)
        self.scrollbarV.pack(fill=Y, side=RIGHT)
        self.scrollbarV.config(command = self.treeView.yview)
        self.scrollbarH = Scrollbar(master=self.frmTreeView)
        self.scrollbarH.config(command = self.treeView.xview, orient=tk.HORIZONTAL)
        self.scrollbarH.pack(fill=X, side=BOTTOM)

        self.treeView.configure(xscrollcommand=self.scrollbarH.set,
                    yscrollcommand=self.scrollbarV.set)
        
        self.treeView.pack(expand=1, fill=X, side=TOP)

        self.__configurarColunasTreeView()


    def __configurarColunasTreeView(self):
        self.treeView['columns'] = ('descicao', 'unidade', 'quantidade', 'custoUnitario', 'custoRelativo')
        self.treeView.heading("#0", text='Código ', anchor='w')
        self.treeView.column("#0", anchor='n', width=80)
        self.treeView.heading('descicao', text='Nome', anchor='w')
        self.treeView.column('descicao', anchor='w', width=240)
        self.treeView.heading('unidade', text='Unidade')
        self.treeView.column('unidade', anchor='center', width=100)
        self.treeView.heading('quantidade', text='Quantidade')
        self.treeView.column('quantidade', anchor='ne', width=100)
        
        self.treeView.heading('custoUnitario', text='Custo Unitário')
        self.treeView.column('custoUnitario', anchor='ne', width=150)
        self.treeView.heading('custoRelativo', text='Custo Relativo')
        self.treeView.column('custoRelativo', anchor='ne', width=120)


    def LimparValores(self):
        for i in self.treeView.get_children():
            self.treeView.delete(i)


    def __carregarAtividade(self, estado):
        if self.__atividadeDto == None:
            return

        self.LimparValores()

        self.custoTotal = 0.0

        itens = self.session.query(self.__modelAtividadeItem).\
            filter(self.__modelAtividadeItem.atividade_id == self.__atividadeDto.id).all()  

        for item in itens:
            custo = self.session.query(Custo).filter(self.__filtro==item.pegarObjetoFk().id, Custo.estado==estado).first()
            if custo != None:
                self.custoTotal += custo.custoMedio * item.quantidade
            self.__inserirItemNaGrid(item, item.pegarObjetoFk(), custo)

        self.__inserirSomatorioFinal(self.custoTotal, self.__descricaoTotal)

    def __inserirItemNaGrid(self, itemAtividade, itemEspecifico, custo):
        custoUnitario = 0.0
        if custo != None:
             custoUnitario = custo.custoMedio

        self.treeView.insert('', END, text=itemEspecifico.id, 
                                values=(itemEspecifico.nome, itemAtividade.unidade, itemAtividade.quantidade,
                                       aplicarMascaraRS(custoUnitario), aplicarMascaraRS(custoUnitario * itemAtividade.quantidade)))


    def __inserirSomatorioFinal(self, custoTotal, descricao, totalEncargoSocial=""):
        self.treeView.insert('', END, text="(+)", 
                                values=(descricao, "", totalEncargoSocial,
                                    "", aplicarMascaraRS(custoTotal)))
        
    def DefinirAtividade(self, atividade, estado):
        self.__atividadeDto = atividade
        self.__carregarAtividade(estado)

    def DefinirDescricaoTotal(self, descricao):
        self.__descricaoTotal = descricao

    def ExibirTotais(self, custoTotal, descricao=None):
        if descricao == None:
            descricao = self.__descricaoTotal
        self.__inserirSomatorioFinal(custoTotal, descricao)

    def InserirTotalEncargoSocial(self, percentual, total):
        self.__inserirSomatorioFinal(total, "Total com Encargos Sociais", percentual)
    
    def definirHeightTreeView(self, height):
        self.treeView["height"] = height

    def definirDescricao(self, descricao):
        self.lbTitulo.config(text=descricao)





        


        





