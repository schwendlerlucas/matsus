# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
from tkinter import ttk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime

from models.projeto import Projeto
from DTOs.cadProjetoDto import CadProjetoDTO


class FrameCadProjeto(FrameBase):
    
    def __init__(self, parent=None, session=None, cadProjetoDTO:CadProjetoDTO=None):
        super(FrameCadProjeto, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__projetoDto = None
        self.__cadprojetoDTO = cadProjetoDTO
        if self.__cadprojetoDTO != None:
            self.__projetoDto = cadProjetoDTO.projeto
        self.__proximaTela = None

        
        parent.title("Cadastro de projeto")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("800x600+100+100")
        self.__createUI()

        self.__carregarValorCampos()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Cadastro de projeto", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frameCampos = Frame()
        frameCampos.pack(side=TOP)

        frmLabels = Frame(frameCampos, width=1)
        frmLabels.pack(side=LEFT)
        labelProjeto = Label(frmLabels, text="Nome do Projeto ", anchor='ne')
        labelProjeto.pack(side=TOP, fill=X, pady=5)
        labelLocal = Label(frmLabels, text="Local ", anchor='ne')
        labelLocal.pack(side=TOP, fill=X, pady=5)
        labelLocal = Label(frmLabels, text="Situação financeira ", anchor='ne')
        labelLocal.pack(side=TOP, fill=X, pady=5)
        labelClassifRegiao = Label(frmLabels, text="Classificação de região ", anchor='ne')
        labelClassifRegiao.pack(side=TOP, fill=X, pady=5)
        labelDistCentros = Label(frmLabels, text="Distancia de centros urbanos ", anchor='ne')
        labelDistCentros.pack(side=TOP, fill=X, pady=5)
        labeNvlConheTecAlternativas = Label(frmLabels, text="Nivel de conhecimento sobre técnicas alternativas de construção ", anchor='ne')
        labeNvlConheTecAlternativas.pack(side=TOP, fill=X, pady=5)
        labelNvlConhecSolo = Label(frmLabels, text="Nivel de conhecimento sobre técnicas a base de solo ", anchor='ne')
        labelNvlConhecSolo.pack(side=TOP, fill=X, pady=5)
        labelGrauAceitacaoTec = Label(frmLabels, text="Grau de aceitação de técnicas a base de solo ", anchor='ne')
        labelGrauAceitacaoTec.pack(side=TOP, fill=X, pady=5)
        labelPredisposicaoSolo = Label(frmLabels, text="Predisposição do solo da região para o uso em sistemas construtivos ", anchor='ne')
        labelPredisposicaoSolo.pack(side=TOP, fill=X, pady=5)
        labelHaCulturaConstrucao = Label(frmLabels, text=" ", anchor='ne')
        labelHaCulturaConstrucao.pack(side=TOP, fill=X, pady=5)
        labelQuantidadeSistemaConstrutivo = Label(frmLabels, text="Quantidade de Sistema construtivo ", anchor='ne')
        labelQuantidadeSistemaConstrutivo.pack(side=TOP, fill=X, pady=5)


        frmEntries = Frame(frameCampos, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtProjetoVar = StringVar()
        self.edtProjeto = Entry(frmEntries, width = "30", textvariable=self.edtProjetoVar)
        self.edtProjeto.pack(side=TOP, fill=X, pady=6)

        self.cbbLocal = ttk.Combobox(frmEntries, state='readonly')
        self.cbbLocal["values"] = LISTA_ESTADOS
        self.cbbLocal.current(0)
        self.cbbLocal.pack(side=TOP,fill=X, pady=6)

        self.cbbSituacaoFinanceira = ttk.Combobox(frmEntries, state='readonly')
        self.cbbSituacaoFinanceira["values"] = LISTA_SITUACAO_FINANCEIRA
        self.cbbSituacaoFinanceira.current(0)
        self.cbbSituacaoFinanceira.pack(side=TOP,fill=X, pady=6)

        self.cbbClassifRegiao = ttk.Combobox(frmEntries, state='readonly')
        self.cbbClassifRegiao["values"] = LISTA_CLASSIF_REGIAO
        self.cbbClassifRegiao.current(0)
        self.cbbClassifRegiao.pack(side=TOP,fill=X, pady=6)

        self.edtDistanciaVar = StringVar()
        self.edtDistancia = Entry(frmEntries, width = "30", textvariable=self.edtDistanciaVar, justify=RIGHT)
        self.edtDistancia.pack(side=TOP, fill=X, pady=6)

        self.edtNvlConhecTecAlternativasVar = StringVar()
        self.edtNvlConhecTecAlternativas = Entry(frmEntries, width = "30", textvariable=self.edtNvlConhecTecAlternativasVar)
        self.edtNvlConhecTecAlternativas.pack(side=TOP, fill=X, pady=6)

        self.edtNvlConhecSoloVar = StringVar()
        self.edtNvlConhecSolo = Entry(frmEntries, width = "30", textvariable=self.edtNvlConhecSoloVar)
        self.edtNvlConhecSolo.pack(side=TOP, fill=X, pady=5)

        self.edtGrauAceitacaoTecSoloVar = StringVar()
        self.edtGrauAceitacaoTecSolo = Entry(frmEntries, width = "30", textvariable=self.edtGrauAceitacaoTecSoloVar)
        self.edtGrauAceitacaoTecSolo.pack(side=TOP, fill=X, pady=6)

        self.edtPredisposicaoSoloVar = StringVar()
        self.edtPredisposicaoSolo = Entry(frmEntries, width = "30", textvariable=self.edtPredisposicaoSoloVar)
        self.edtPredisposicaoSolo.pack(side=TOP, fill=X, pady=7)

        frameCbHaCultura = Frame(frmEntries)
        frameCbHaCultura.pack(side=TOP, fill=X)

        self.cbHaCulturaAutoConstVar = BooleanVar()
        self.cbHaCulturaAutoConst = Checkbutton(frameCbHaCultura, width = "30", 
            text="Há a cultura de auto contrução?               ", variable=self.cbHaCulturaAutoConstVar, justify=LEFT)
        self.cbHaCulturaAutoConst.pack(side=LEFT, pady=6)

        self.edtQtdSistemaContrutivoVar = StringVar()
        self.edtQtdSistemaContrutivo = Entry(frmEntries, width = "30", textvariable=self.edtQtdSistemaContrutivoVar, justify=RIGHT)
        self.edtQtdSistemaContrutivo.pack(side=TOP, fill=X, pady=5)


        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15)
        self.btnPriximaTela = Button(frmBotoes, text="Seguinte", width=20, command=self.__proximaTelaOnClick)       
        self.btnPriximaTela.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)

        self.lbWarnings = Label(frmWarning, text="", height="2")
        self.lbWarnings.pack()


    def __proximaTelaOnClick(self):
        if not self.__validarCampos():
            return

        self.__projetoDto.nome = self.edtProjeto.get()
        self.__projetoDto.estado = self.cbbLocal.get()
        self.__projetoDto.classifRegiao = self.cbbClassifRegiao.get()
        self.__projetoDto.situacaoFinanceira = self.cbbSituacaoFinanceira.get()
        self.__projetoDto.distanciaCentroUrbano = self.edtDistanciaVar.get()
        self.__projetoDto.nvlConhecimentoTecAlternativas = self.edtNvlConhecTecAlternativasVar.get()
        self.__projetoDto.nvlConhecimentoTecBaseSolo = self.edtNvlConhecSoloVar.get()
        self.__projetoDto.grauAceitacaoTecBaseSolo = self.edtGrauAceitacaoTecSoloVar.get()
        self.__projetoDto.predisposicaoSolo = self.edtPredisposicaoSoloVar.get()
        self.__projetoDto.culturaAutoConstrucao = self.cbHaCulturaAutoConstVar.get()
        self.__projetoDto.qtdSistemaConstrutivo = self.edtQtdSistemaContrutivoVar.get()
        
        self.__proximaTela(self.__cadprojetoDTO)

    
    def __validarCampos(self):
        self.lbWarnings.configure(text = "")
        if self.__projetoDto == None:
            self.__projetoDto = Projeto()
            if self.__cadprojetoDTO == None:    
                self.__cadprojetoDTO = CadProjetoDTO()
                self.__cadprojetoDTO.projeto = self.__projetoDto


        nomeProjeto = self.edtProjeto.get()
        if (nomeProjeto == ''):
            self.lbWarnings.configure(text = "O 'nome do projeto' é obrigatório", foreground="red")
            return False
        
        if not validarDouble(self.edtDistancia.get()):
            self.lbWarnings.configure(text = "o valor de 'Distancia de centros urbanos' deve ser numerico.", foreground="red")
            return False

        projeto = self.session.query(Projeto).filter_by(nome=nomeProjeto).first()
        if (projeto != None and projeto.id != self.__projetoDto.id):
            self.lbWarnings.configure(text = "Projeto já cadastrado", foreground="red")
            return False 

        return True
        
    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick
    

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick


    def __carregarValorCampos(self):
        if self.__projetoDto == None:
            return
    
        self.edtProjetoVar.set(self.__projetoDto.nome)
        self.cbbLocal.set(self.__projetoDto.estado)
        self.cbbClassifRegiao.set(self.__projetoDto.classifRegiao)
        self.cbbSituacaoFinanceira.set(self.__projetoDto.situacaoFinanceira)
        self.edtDistanciaVar.set(self.__projetoDto.distanciaCentroUrbano)
        self.edtNvlConhecTecAlternativasVar.set(self.__projetoDto.nvlConhecimentoTecAlternativas)
        self.edtNvlConhecSoloVar.set(self.__projetoDto.nvlConhecimentoTecBaseSolo)
        self.edtGrauAceitacaoTecSoloVar.set(self.__projetoDto.grauAceitacaoTecBaseSolo)
        self.edtPredisposicaoSoloVar.set(self.__projetoDto.predisposicaoSolo)
        self.cbHaCulturaAutoConstVar.set(self.__projetoDto.culturaAutoConstrucao)
        self.edtQtdSistemaContrutivoVar.set(self.__projetoDto.qtdSistemaConstrutivo)
        
    def definirProximaTela(self, proximaTela):
        self.__proximaTela = proximaTela
        