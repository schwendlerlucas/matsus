LISTA_ESTADOS = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS",  "SC", "SE", "SP", "TO"]

LISTA_CLASSE_CUSTOS = ["Equipamento", "Mão de obra", "Material"]

LISTA_PAISES = ["Brasil", "Argentina", "Dinamarca"]

INDEX_CLASSE_EQUIPAMENTO = 0
INDEX_CLASSE_MAO_DE_OBRA = 1
INDEX_CLASSE_MATERIAL    = 2

LISTA_UNIDADE = ["M", "H"]

LISTA_CLASSIF_REGIAO = ["Assentamento rural", "Reserva Indígena", "Propriedade rural de pequeno porte", 
                        "Propriedade rural de médio porte", "Propriedade rural de grande porte"]

LISTA_SITUACAO_FINANCEIRA = ["Sem Renda", "01 salário mínimo ou menos", "Entre 01 e 02 salários mínimos",
                             "Entre 02 e 03 salários mínimos", "Entre 03 e 05 salários mínimos",
                             "Entre 05 e 10 salários mínimos", "Mais que 20 sálários mínimos", 
                             "Não declarar"]        


LISTA_SALUBRIDADE = ["1", "2", "3", "7"]

LISTA_SEGURIDADE = ["1", "2", "3", "4", "5"]