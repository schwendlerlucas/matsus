# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
from tkinter import ttk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime

from models.projeto import Projeto
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from models.sistemaConstrutivo import SistemaConstrutivo

from interfaces.frmCardSistemaConstrutivo import FrameCardSistemaConstrutivo

from DTOs.cadProjetoDto import CadProjetoDTO


class FrameCadProjetoSelecionarSistemas(FrameBase):
    
    def __init__(self, parent=None, session=None, cadProjetoDTO:CadProjetoDTO=None):
        super(FrameCadProjetoSelecionarSistemas, self).__init__(parent=parent, session=session)

        self.clearParent()
        
        self.__projetoDto = None
        self.__cadProjetoDTO = cadProjetoDTO
        if self.__cadProjetoDTO != None:
            self.__projetoDto = cadProjetoDTO.projeto

        self.__proximaTela = None
        self.__voltarTelaAnterior = None

        
        parent.title("Cadastro de projeto")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("800x600+100+100")
        self.__createUI()

        self.__carregarValorCampos()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Cadastro de projeto", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frameCampos = Frame()
        frameCampos.pack(side=TOP)

        frmLabels = Frame(frameCampos, width=1)
        frmLabels.pack(side=LEFT)
        labelProjeto = Label(frmLabels, text="Nome do Projeto ", anchor='ne')
        labelProjeto.pack(side=TOP, fill=X, pady=4)


        frmEntries = Frame(frameCampos, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtProjetoVar = StringVar()
        self.edtProjeto = Entry(frmEntries, width = "30", textvariable=self.edtProjetoVar, state='readonly')
        self.edtProjeto.pack(side=TOP, fill=X, pady=5)

        frameTreeView = Frame()
        frameTreeView.pack(side=TOP, fill=X, padx=6, pady=6)

        self.frameSistemaContrutivo = FrameCardSistemaConstrutivo(frameTreeView, 
            self.session, SistemaConstrutivo, ProjetoSistemaConstrutivo)
        self.frameSistemaContrutivo.definirDescricao("Sistemas construtivos a serem comparados")
        self.frameSistemaContrutivo.definirHeightTreeView(10)
        self.frameSistemaContrutivo.pack(fill=BOTH, side=TOP)
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10, command=self.__btnVoltarOnClick)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnPriximaTela = Button(frmBotoes, text="Seguinte", width=20, command=self.__proximaTelaOnClick)       
        self.btnPriximaTela.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)

        self.lbWarnings = Label(frmWarning, text="", height="2")
        self.lbWarnings.pack()


    def __proximaTelaOnClick(self):
        if not self.__validarCampos():
            return

        self.__cadProjetoDTO.projetoSistemaConstrutivoList = self.frameSistemaContrutivo.getSistemaContrutivoList()
        
        self.__proximaTela(self.__cadProjetoDTO)
    
    def __validarCampos(self):
        self.lbWarnings.configure(text = "")
        if self.__projetoDto == None:
           self.__projetoDto = Projeto()     

        return True
        

    def __btnVoltarOnClick(self):
        self.__voltarTelaAnterior(self.__cadProjetoDTO)

    def definirVoltar(self, voltarTelaAnterior):
        self.__voltarTelaAnterior = voltarTelaAnterior


    def __carregarValorCampos(self):
        if self.__projetoDto == None:
            return

        self.edtProjetoVar.set(self.__projetoDto.nome)

        if len(self.__cadProjetoDTO.projetoSistemaConstrutivoList) > 0:
            self.frameSistemaContrutivo.loadSistemas(self.__cadProjetoDTO.projetoSistemaConstrutivoList)
        else:
            self.frameSistemaContrutivo.consultar(self.__projetoDto.id)
    
    def definirProximaTela(self, proximaTela):
        self.__proximaTela = proximaTela
        