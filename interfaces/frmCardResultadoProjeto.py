# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime
from interfaces.interfaceUtils import aplicarMascaraE, aplicarMascaraRS

from interfaces.calculoIndicadores.calculoIndicadoresPorItem import CalculoIndicadoresPorItem
from interfaces.calculoIndicadores.calculoIndicadoresSoma import CalculoIndicadoresSoma
from interfaces.calculoIndicadores.calculoInidicadoresResult import CalculoIndicadoresResult


from interfaces.frmCardItensSistemaConstrutivo import FrameCardItensSistemaConstrutivo

from controllers.resultadoTransporteProjetoController import ResultadoTransporteProjetoController


class FrameCardResultadoProjeto(FrameBase):
    
    def __init__(self, parent=None, session=None):
        super(FrameCardResultadoProjeto, self).__init__(parent, session=session)


        self.listaItensCalculados = []
        self.__descricaoTotal = ""
        
        self.__createUI()

        #self.__carregarAtividade()


    def __createUI(self):

        frameTitulo = Frame()
        frameTitulo.pack(side=TOP, fill=X, padx=5, pady=5)

        self.lbTitulo = Label(master=frameTitulo, text="Resultados")
        self.lbTitulo.pack(side=LEFT)

        self.frmTreeView = Frame()
        self.frmTreeView.pack(side=TOP, fill=BOTH, expand=1, padx=5, pady=5)

        
        self.treeView = ttk.Treeview(master=self.frmTreeView)

        self.scrollbarV = Scrollbar(master=self.frmTreeView)
        self.scrollbarV.pack(fill=Y, side=RIGHT)
        self.scrollbarV.config(command = self.treeView.yview)
        self.scrollbarH = Scrollbar(master=self.frmTreeView)
        self.scrollbarH.config(command = self.treeView.xview, orient=tk.HORIZONTAL)
        self.scrollbarH.pack(fill=X, side=BOTTOM)

        self.treeView.configure(xscrollcommand=self.scrollbarH.set,
                    yscrollcommand=self.scrollbarV.set)

        self.treeView.pack(expand=1, fill=X, side=TOP)

        self.__configurarColunasTreeView()


    def __configurarColunasTreeView(self):
        self.treeView['columns'] = ('insumo', 'distancia', 'tipoTransporte',\
            'custoTotal',
            'GWP', 'GWPTotal', 'ODP', 'ODPTotal', 
            'AP', 'APTotal',\
            'EP', 'EPTotal', 
            'POCP', 'POCPTotal')
        self.treeView.heading("#0", text='Atividade ', anchor='w')
        self.treeView.column("#0", anchor='w', width=240)

        self.treeView.heading('insumo', text='Insumo', anchor='w')
        self.treeView.column('insumo', anchor='w', width=240)
        self.treeView.heading('distancia', text='Km Distância')
        self.treeView.column('distancia', anchor='ne', width=100)
        self.treeView.heading('tipoTransporte', text='Tipo de transporte')
        self.treeView.column('tipoTransporte', anchor='w', width=150)

        self.treeView.heading('custoTotal', text='Custo')
        self.treeView.column('custoTotal', anchor='ne', width=100)       

        self.treeView.heading('GWP', text='GWP (unit)')
        self.treeView.column('GWP', anchor='ne', width=100)
        self.treeView.heading('GWPTotal', text='GWP (sub. total)')
        self.treeView.column('GWPTotal', anchor='ne', width=100)

        self.treeView.heading('ODP', text='ODP (unit)')
        self.treeView.column('ODP', anchor='ne', width=100)
        self.treeView.heading('ODPTotal', text='ODP (sub. total)')
        self.treeView.column('ODPTotal', anchor='ne', width=100)

        self.treeView.heading('AP', text='AP (unit)')
        self.treeView.column('AP', anchor='ne', width=100)
        self.treeView.heading('APTotal', text='AP (sub. total)')
        self.treeView.column('APTotal', anchor='ne', width=100)

        self.treeView.heading('EP', text='EP (unit)')
        self.treeView.column('EP', anchor='ne', width=100)
        self.treeView.heading('EPTotal', text='EP (sub. total)')
        self.treeView.column('EPTotal', anchor='ne', width=100)

        self.treeView.heading('POCP', text='POCP (unit)')
        self.treeView.column('POCP', anchor='ne', width=100)
        self.treeView.heading('POCPTotal', text='POCP (sub. total)')
        self.treeView.column('POCPTotal', anchor='ne', width=100)


    def carregarAtividade(self, projeto_id:int, sistema_id:int, quantidade:float):
        for i in self.treeView.get_children():
            self.treeView.delete(i)

        resultadoController = ResultadoTransporteProjetoController(self.session)

        insumos = resultadoController.queryTransportationResults(projeto_id, sistema_id, quantidade)

        custoFinal = 0.0
        GWPFinal = 0.0
        ODPFinal = 0.0
        APFinal = 0.0
        EPFinal = 0.0
        POCPFinal = 0.00
        
        for insumo in insumos:
            self.treeView.insert("", END, text=insumo.atividade_nome, 
                                values=(insumo.insumo_nome, insumo.distancia, 
                                        insumo.meio_transporte_nome,
                                        aplicarMascaraRS(insumo.custoTotal), 
                                        aplicarMascaraE(insumo.GWP), 
                                        aplicarMascaraE(insumo.GWPTotal),
                                        aplicarMascaraE(insumo.ODP), 
                                        aplicarMascaraE(insumo.ODPTotal),
                                        aplicarMascaraE(insumo.AP), 
                                        aplicarMascaraE(insumo.APTotal),
                                        aplicarMascaraE(insumo.EP), 
                                        aplicarMascaraE(insumo.EPTotal),
                                        aplicarMascaraE(insumo.POCP), 
                                        aplicarMascaraE(insumo.POCPTotal)))

            custoFinal += insumo.custoTotal
            GWPFinal += insumo.GWPTotal
            ODPFinal += insumo.ODPTotal
            APFinal += insumo.APTotal
            EPFinal += insumo.EPTotal
            POCPFinal += insumo.POCPTotal


        self.treeView.insert("", END, text="", values=("", ""))

        self.treeView.insert("", END, text="Totais para a fase de transporte", 
                                values=("", "", "", 
                                        aplicarMascaraRS(custoFinal),
                                        "GWP (Final)", aplicarMascaraE(GWPFinal),
                                        "ODP (Final)", aplicarMascaraE(ODPFinal),
                                        "AP (Final)", aplicarMascaraE(APFinal),
                                        "EP (Final)", aplicarMascaraE(EPFinal),
                                        "POCP (Final)", aplicarMascaraE(POCPFinal)))
    
    
    def definirHeightTreeView(self, height):
        self.treeView["height"] = height

    def definirDescricao(self, descricao):
        self.lbTitulo.config(text=descricao)
    
    






        


        





