# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.constantesFatoresDecisao import *

from models.fatoresDecisao import FatoresDecisao

from datetime import datetime


from models.atividade import Atividade
from models.sistemaConstrutivo import SistemaConstrutivo

class FrameFatoresDecisao(FrameBase):
    
    def __init__(self, parent=None, session=None):
        super(FrameFatoresDecisao, self).__init__(parent=parent, session=session)
        self.__fatoresDecisaoDTO = None
        self.__consultarFatoresDecisao()
        self.clearParent()
        self.__lastItemSelected = None


        
        parent.title("Cadastro de fatores de decisão")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1000x630+100+100")
        self.__createUI()

    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        
        #frameImage = Frame(master=self.frameTop)
        #frameImage.pack(side=)
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=20)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Cadastro de fatores de decisão", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)


        self.frmTreeView = Frame()
        self.frmTreeView.pack(fill=BOTH, side=TOP, expand=1)

        frmTreeViewScrollBar = Frame(self.frmTreeView)
        frmTreeViewScrollBar.pack(side=TOP, fill=BOTH, expand=1, padx=5)

        self.treeView = ttk.Treeview(master=frmTreeViewScrollBar, height=4)
        self.treeView.bind('<Double-Button>', self.__onDoubleClick)

        self.scrollbarV = Scrollbar(master=frmTreeViewScrollBar)
        self.scrollbarV.pack(fill=Y, side=RIGHT, pady=15)
        self.scrollbarV.config(command = self.treeView.yview)

        self.treeView.configure(yscrollcommand=self.scrollbarV.set, height=15)
        self.treeView.pack(pady=15, fill=BOTH, expand=1)

        frameEdicao = Frame(self.frmTreeView)
        frameEdicao.pack(side=BOTTOM, fill=X)

        btnOkValor = Button(frameEdicao, width=3, text="ok", command=self.__btnOkOnClick)
        btnOkValor.pack(side=RIGHT, padx=5)

        self.edtValorVar = StringVar() 
        self.edtValor = Entry(frameEdicao, width=10, justify=RIGHT, textvariable=self.edtValorVar)
        self.edtValor.pack(side=RIGHT, padx=5)

        self.lblDescicao = Label(frameEdicao)
        self.lblDescicao.pack(side=RIGHT, padx=10)
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=15)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnCadastrar = Button(frmBotoes, text="Cadastrar", width=20, command=self.__cadastrarOnClick)       
        self.btnCadastrar.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)

        self.lbWarnings = Label(frmWarning, text="", height="2")
        self.lbWarnings.pack()

        self.__configurarTreeView()      


    def __configurarTreeView(self):
        self.treeView['columns'] = ('valor')
        self.treeView.heading("#0", text=' Indicadoes econômicos', anchor='w')
        self.treeView.column("#0", anchor='w', width=800)
        self.treeView.heading('valor', text=' % Fator ', anchor='ne')
        self.treeView.column('valor', anchor='ne', width=100)

        segundoNivel = "     "
        terceiroNivel = "          "

        self.treeView.insert('', END, text="AMBIENTAL", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + CONST_AI, 
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.AI)))

        self.treeView.insert('', END, text=segundoNivel + CONST_AII, 
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.AII)))                 

        self.treeView.insert('', END, text=segundoNivel + CONST_AIII, 
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.AIII)))                               

        self.treeView.insert('', END, text=segundoNivel + CONST_AIV, 
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.AIV)))   


        self.treeView.insert('', END, text="Economico", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + CONST_EI, 
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.EI)))

        self.treeView.insert('', END, text=segundoNivel + CONST_EII,
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.EII)))



        self.treeView.insert('', END, text="Social - avaliação para a aplicação do sistema construtivo", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + CONST_SI, 
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.SI)))

        self.treeView.insert('', END, text=segundoNivel + CONST_SII, 
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.SII)))

        self.treeView.insert('', END, text=segundoNivel + CONST_SIII,
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.SIII)))


        self.treeView.insert('', END, text=segundoNivel + CONST_SIV,
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.SIV)))


        self.treeView.insert('', END, text="Ténica", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + CONST_TI,
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TI)))

        self.treeView.insert('', END, text=segundoNivel + CONST_TII,
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TII)))

        self.treeView.insert('', END, text=segundoNivel + CONST_TIII,
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TIII)))

        self.treeView.insert('', END, text=segundoNivel + CONST_TIV,
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TIV)))

        self.treeView.insert('', END, text=segundoNivel + CONST_TV,
                            values=(retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TV)))                          
                            


    def __cadastrarOnClick(self):
        if not self.__validarCampos():
            return

        self.__definirValoresNoDto()

        add = self.__fatoresDecisaoDTO.id == None
        if add:
            self.session.add(self.__fatoresDecisaoDTO)
        
        self.session.commit()

        if add:
            self.lbWarnings.configure(text="Fatores de decisão cadastrados com sucesso", foreground="black")
        else:
            self.lbWarnings.configure(text="Fatores de decisão atualizados com sucesso", foreground="black")

         
    def __definirValoresNoDto(self):
        for child in self.treeView.get_children():
            valor = 0
            if len(self.treeView.item(child)["values"]) == 1:
                valor = retornarZeroSeVazio(self.treeView.item(child)["values"][0])

            self.__definirValorNoDto(self.treeView.item(child)["text"], valor)
    
    def __validarCampos(self):
        return True
        
    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick


    def definirTelaSeguinteCadSistemaConstrutivo(self, onClickProximaTela):
        self.__openProximaTela = onClickProximaTela

    def __definirValorNoDto(self, descricao: str, value: float):
        if self.__lastItemSelected == None:
            return False

        if not self.__lastItemSelected.strip() in ARRAY_FATORES_DECISAO:
            return False

        descricao = descricao.strip()

        if (descricao == CONST_EI):
            self.__fatoresDecisaoDTO.EI = value
        elif (descricao == CONST_EII):
            self.__fatoresDecisaoDTO.EII = value

        elif (descricao == CONST_AI):
            self.__fatoresDecisaoDTO.AI = value
        elif (descricao == CONST_AII):
            self.__fatoresDecisaoDTO.AII = value
        elif (descricao == CONST_AIII):
            self.__fatoresDecisaoDTO.AIII = value
        elif (descricao == CONST_AIV):
            self.__fatoresDecisaoDTO.AIV = value

        elif (descricao == CONST_SI):
            self.__fatoresDecisaoDTO.SI = value
        elif (descricao == CONST_SII):
            self.__fatoresDecisaoDTO.SII = value
        elif (descricao == CONST_SIII):
            self.__fatoresDecisaoDTO.SIII = value
        elif (descricao == CONST_SIV):
            self.__fatoresDecisaoDTO.SIV = value

        elif (descricao == CONST_TI):
            self.__fatoresDecisaoDTO.TI = value
        elif (descricao == CONST_TII):
            self.__fatoresDecisaoDTO.TII = value
        elif (descricao == CONST_TIII):
            self.__fatoresDecisaoDTO.TIII = value
        elif (descricao == CONST_TIV):
            self.__fatoresDecisaoDTO.TIV = value
        elif (descricao == CONST_TV):
            self.__fatoresDecisaoDTO.TV = value

        else:
            print("Erro:  não encontrou")

    def __pegarValorPelaDescicao(self, descricao: str):
        if self.__fatoresDecisaoDTO == None:
            return "0.0"
    
        descricao = descricao.strip()

        if (descricao == CONST_AI): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.AI)
        if (descricao == CONST_AII): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.AII)        
        if (descricao == CONST_AIII): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.AIII)                            
        if (descricao == CONST_AIV): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.AIV)                        

        if (descricao == CONST_EI): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.EI)
        if (descricao == CONST_EII): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.EII)  

        if (descricao == CONST_SI): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.SI)
        if (descricao == CONST_SII): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.SII)        
        if (descricao == CONST_SIII): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.SIII)                            
        if (descricao == CONST_SIV): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.SIV) 

        if (descricao == CONST_TI): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TI)
        if (descricao == CONST_TII): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TII)        
        if (descricao == CONST_TIII): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TIII)                            
        if (descricao == CONST_TIV): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TIV) 
        if (descricao == CONST_TV): 
            return retornarZeroSeNoneOuVazio(self.__fatoresDecisaoDTO.TV) 

    def __onDoubleClick(self, event):
        self.edtValorVar.set("")
        self.lblDescicao.config(text="")
        itemSelected = self.treeView.item(self.treeView.focus())
        self.__lastItemSelected = itemSelected["text"]

        if not  self.__lastItemSelected.strip() in ARRAY_FATORES_DECISAO:
            return False

        self.lblDescicao.config(text=itemSelected["text"].strip())
        self.edtValorVar.set("")
        self.edtValorVar.set(itemSelected["values"])

    def __consultarFatoresDecisao(self):
        fatoresDecisao = self.session.query(FatoresDecisao).filter(FatoresDecisao.id==1).first()
        self.__fatoresDecisaoDTO = fatoresDecisao

        if self.__fatoresDecisaoDTO == None:
            self.__fatoresDecisaoDTO = FatoresDecisao()


    def __btnOkOnClick(self):
        if not validarDoubleOuVazio(self.edtValor.get()):
            return 

        if (self.__lastItemSelected == None):
            return 
        
        if not self.__lastItemSelected.strip() in ARRAY_FATORES_DECISAO:
            return False

        valor = float(self.edtValor.get())

        if (valor < 0 or valor > 100):
            self.lbWarnings.configure(text="O fator de decisão deve estar entre o intervalo de 0 a 100!", foreground="red")
            return
   
        for child in self.treeView.get_children():
            if self.treeView.item(child)["text"] == self.__lastItemSelected:
                self.treeView.item(child, text=self.__lastItemSelected, values=(valor))



        

        
        

        
        


        





