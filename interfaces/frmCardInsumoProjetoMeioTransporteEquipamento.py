from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.frmCardInsumoProjetoMeioTransporteBase import FrameCardInsumoProjetoMeioTransporteBase

from models.projetoSistemaConstrutivoAtividadeTransporte import ProjetoSistemaConstrutivoAtividadeTransporte
from controllers.atividadeEquipamentoController import AtividadeEquipamentoController

from DTOs.cadProjetoDto import CadProjetoDTO

from controllers.equipamentoController import EquipamentoController

class FrameCardInsumoProjetoMeioTransporteEquipamento(FrameCardInsumoProjetoMeioTransporteBase):


    def queryInsumos(self):
        super(FrameCardInsumoProjetoMeioTransporteEquipamento, self).queryInsumos()
        self.insumosList = self.cadProjetoDTO.getMeioTransporteInsumoEquipamentosList(self.sistema_id)
        if len(self.insumosList) == 0:
            listaInsumosDTO = self.__getListNewProjeto()
            self.cadProjetoDTO.setMeioTransporteInsumoEquipamentosList(self.sistema_id, listaInsumosDTO)
            self.insumosList = listaInsumosDTO


    def __getListNewProjeto(self):
        controller = AtividadeEquipamentoController(self.session)
        listaAtividades = controller.getEquipamentoAtividadeList(self.sistema_id, 
            self.cadProjetoDTO.projeto.qtdSistemaConstrutivo)
        return listaAtividades


    def setListOnCadProjetoDTO(self):
        super(FrameCardInsumoProjetoMeioTransporteEquipamento, self).setListOnCadProjetoDTO()
        if self.sistema_id > 0:
            self.cadProjetoDTO.setMeioTransporteInsumoEquipamentosList(self.sistema_id, self.insumosList)
    
