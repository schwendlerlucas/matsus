# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime

from interfaces.calculoIndicadores.calculoIndicadoresPorItem import CalculoIndicadoresPorItem
from interfaces.calculoIndicadores.calculoIndicadoresSoma import CalculoIndicadoresSoma
from interfaces.calculoIndicadores.calculoInidicadoresResult import CalculoIndicadoresResult


from interfaces.frmCardItensSistemaConstrutivo import FrameCardItensSistemaConstrutivo

from controllers.resultadoTransporteProjetoController import ResultadoTransporteProjetoController


class FrameCardResultadoProjeto(FrameBase):
    
    def __init__(self, parent=None, session=None):
        super(FrameCardResultadoProjeto, self).__init__(parent, session=session)


        self.listaItensCalculados = []
        self.__descricaoTotal = ""
        
        self.__createUI()

        #self.__carregarAtividade()


    def __createUI(self):

        frameTitulo = Frame()
        frameTitulo.pack(side=TOP, fill=X, padx=5, pady=5)

        self.lbTitulo = Label(master=frameTitulo, text="Resultados")
        self.lbTitulo.pack(side=LEFT)

        self.frmTreeView = Frame()
        self.frmTreeView.pack(side=TOP, fill=BOTH, expand=1, padx=5, pady=5)

        
        self.treeView = ttk.Treeview(master=self.frmTreeView)

        self.scrollbarV = Scrollbar(master=self.frmTreeView)
        self.scrollbarV.pack(fill=Y, side=RIGHT)
        self.scrollbarV.config(command = self.treeView.yview)
        self.scrollbarH = Scrollbar(master=self.frmTreeView)
        self.scrollbarH.config(command = self.treeView.xview, orient=tk.HORIZONTAL)
        self.scrollbarH.pack(fill=X, side=BOTTOM)

        self.treeView.configure(xscrollcommand=self.scrollbarH.set,
                    yscrollcommand=self.scrollbarV.set)

        self.treeView.pack(expand=1, fill=X, side=TOP)

        self.__configurarColunasTreeView()


    def __configurarColunasTreeView(self):
        self.treeView['columns'] = ('insumo', 'distancia', 'tipoTransporte',\
            'residPerigoso', 'residPerigosoTotal', 'residRadioativo', 'residRadiativoTotal', 
            'residNaoPerigoso', 'residNaoPerigosoTotal',\
            'fonteNaoRenovavel', 'fonteNaoRenovavelTotal', 
            'fonteRenovavel', 'fonteRenovavelTotal')
        self.treeView.heading("#0", text='Atividade ', anchor='w')
        self.treeView.column("#0", anchor='w', width=240)

        self.treeView.heading('insumo', text='Insumo', anchor='w')
        self.treeView.column('insumo', anchor='w', width=240)
        self.treeView.heading('distancia', text='Km Distância')
        self.treeView.column('distancia', anchor='ne', width=100)
        self.treeView.heading('tipoTransporte', text='Tipo de transporte')
        self.treeView.column('tipoTransporte', anchor='ne', width=150)

        self.treeView.heading('residPerigoso', text='Resíduos perigosos (unit)')
        self.treeView.column('residPerigoso', anchor='ne', width=200)
        self.treeView.heading('residPerigosoTotal', text='Resíduos perigosos (sub. total)')
        self.treeView.column('residPerigosoTotal', anchor='ne', width=220)

        self.treeView.heading('residRadioativo', text='Resíduos radioativos (unit)')
        self.treeView.column('residRadioativo', anchor='ne', width=200)
        self.treeView.heading('residRadiativoTotal', text='Resíduos radioativos (sub. total)')
        self.treeView.column('residRadiativoTotal', anchor='ne', width=230)

        self.treeView.heading('residNaoPerigoso', text='Resíduos não perigosos (unit)')
        self.treeView.column('residNaoPerigoso', anchor='ne', width=220)
        self.treeView.heading('residNaoPerigosoTotal', text='Resíduos não perigosos (sub. total)')
        self.treeView.column('residNaoPerigosoTotal', anchor='ne', width=270)

        self.treeView.heading('fonteNaoRenovavel', text='Fontes não renováveis (unit)')
        self.treeView.column('fonteNaoRenovavel', anchor='ne', width=220)
        self.treeView.heading('fonteNaoRenovavelTotal', text='Fontes não renováveis (sub. total)')
        self.treeView.column('fonteNaoRenovavelTotal', anchor='ne', width=240)

        self.treeView.heading('fonteRenovavel', text='Fontes renováveis (unit)')
        self.treeView.column('fonteRenovavel', anchor='ne', width=200)
        self.treeView.heading('fonteRenovavelTotal', text='Fontes renováveis (sub. total)')
        self.treeView.column('fonteRenovavelTotal', anchor='ne', width=220)


    def carregarAtividade(self, projeto_id:int, sistema_id:int, quantidade:float):
        for i in self.treeView.get_children():
            self.treeView.delete(i)

        resultadoController = ResultadoTransporteProjetoController(self.session)

        insumos = resultadoController.queryTransportationResults(projeto_id, sistema_id, quantidade)

        RPFinal = 0.0
        RRFinal = 0.0
        RNPFinal = 0.0
        ENRFinal = 0.0
        ERFinal = 0.00
        
        for insumo in insumos:
            self.treeView.insert("", END, text=insumo.atividade_nome, 
                                values=(insumo.insumo_nome, insumo.distancia, 
                                        insumo.meio_transporte_nome, 
                                        insumo.residPerigoso, insumo.residPerigosoTotal,
                                        insumo.residRadioativo, insumo.residRadiativoTotal,
                                        insumo.residNaoPerigoso, insumo.residNaoPerigosoTotal,
                                        insumo.fonteNaoRenovavel, insumo.fonteNaoRenovavelTotal,
                                        insumo.fonteRenovavel, insumo.fonteRenovavelTotal))
            
            RPFinal += insumo.residPerigosoTotal
            RRFinal += insumo.residRadiativoTotal
            RNPFinal += insumo.residNaoPerigosoTotal
            ENRFinal += insumo.fonteNaoRenovavelTotal
            ERFinal += insumo.fonteRenovavelTotal


        self.treeView.insert("", END, text="", values=("", ""))

        self.treeView.insert("", END, text="Totais para a fase de transporte", 
                                values=("", "", "", 
                                        "R.P (Final)", RPFinal,
                                        "R.R (Final)", RRFinal,
                                        "R.N.P (Final)", RNPFinal,
                                        "E.N.R (Final)", ENRFinal,
                                        "E.R (Final)", ERFinal))
    
    
    def definirHeightTreeView(self, height):
        self.treeView["height"] = height

    def definirDescricao(self, descricao):
        self.lbTitulo.config(text=descricao)
    
    






        


        





