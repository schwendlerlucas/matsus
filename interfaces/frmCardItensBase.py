from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.constantesViews import *
from interfaces.frmBase import FrameBase
from interfaces.validacoesCampos import *
from models.equipamento import Equipamento

class FrameCardItensBase(FrameBase):

    def __init__(self, parent, session, model, modelAtividade):
        FrameBase.__init__(self, parent=parent, session=session)

        self.listaItens = []
        self.__model = model
        self.modelAtividade = modelAtividade
        self.listaItensExlusao = []
        self.CreateUI()
        self.LoadTable()
        self.__beforeOpenEdicao = None


    def CreateUI(self):
        self.frameTitulo = Frame(master=self)
        self.frameTitulo.pack(side=TOP, fill=X)
        self.frameTreeViewEBotoes = Frame(master=self)
        self.frameTreeViewEBotoes.pack(side=TOP, fill=X, after=self.frameTitulo)

        self.frameBotoes = Frame(master=self.frameTreeViewEBotoes)
        self.frameBotoes.pack(side=RIGHT)


        self.frameTreeView = Frame(master=self.frameTreeViewEBotoes)
        self.frameEdicao = Frame(master=self)
        self.treeView = ttk.Treeview(master=self.frameTreeView, height=4)

        self.lbTitulo = Label(master=self.frameTitulo, text="Titulo")
        self.lbTitulo.pack(side=LEFT)


        self.scrollbar = Scrollbar(master=self.frameTreeView)
        self.scrollbar.pack(fill=Y, side = RIGHT)
        self.scrollbar.config(command = self.treeView.yview)
    
        self.treeView.configure(xscrollcommand=self.scrollbar.set)
        self.treeView.pack(expand=1, fill=BOTH, side=LEFT)
        self.frameTreeView.pack(side=LEFT, fill=BOTH, expand=1)
        #self.treeView.pack(fill=BOTH, side=LEFT)
        #self.frameTreeView.pack(side=LEFT, fill=BOTH)

        #self.frameBotoes = Frame(master=self.frameTreeViewEBotoes)
        #self.frameBotoes.pack(side=RIGHT)
        self.btnNovo = Button(self.frameBotoes, text="Novo", command=self.__btnNovoClick, width=5)
        self.btnNovo.pack(pady=3, padx=2)

        self.btnEditar = Button(master=self.frameBotoes, text="Editar", width=5, comman=self.__btnEditarClick)
        self.btnEditar.pack(pady=3, padx=2)
        self.btnExcluir = Button(master=self.frameBotoes, text="Excluir", width=5, command=self.__btnExcluirClick)
        self.btnExcluir.pack(pady=3, padx=2)



    def LoadTable(self):
        for i in self.treeView.get_children():
            self.treeView.delete(i)

    def __btnNovoClick(self):
        if self.__beforeOpenEdicao != None:
            self.__beforeOpenEdicao()

        self.limparCamposEdicao()
        self.cbbItemClasse.set("")
        if self.__model == Equipamento:
            self.__listItensClasse = self.session.query(self.__model).filter(Equipamento.ehMeioTransporte==False).order_by(self.__model.nome).all()
        else:
            self.__listItensClasse = self.session.query(self.__model).filter().order_by(self.__model.nome).all()
        listaCombo = []
        for item in self.__listItensClasse:
            listaCombo.append(item.nome)

        self.cbbItemClasse["values"] = listaCombo

        self.frameEdicao.pack(side=BOTTOM, fill=X)

    def __btnEditarClick(self):
        if self.__beforeOpenEdicao != None:
            self.__beforeOpenEdicao()

        curItem = self.treeView.focus()
        itemSelected = self.treeView.item(curItem)
        if itemSelected["text"] == "":
            return False
        if itemSelected["text"] == "(+)":
            return False
        
        self.cbbItemClasse.set(itemSelected["values"][0])
        self.onChangeItemClasse(None)

        self.frameEdicao.pack(side=BOTTOM, fill=X)

    def btnOkEdicaoClick(self):
        if not self.validarCampos():
            return 

        try:
            for item in self.listaItens:
                if item.pegarObjetoFk().nome == self.cbbItemClasse.get():
                    self.updateItem(item)
                    return

            itemInserido = self.__pegarItemClasseSelecionada()
            newItem = self.modelAtividade()
            newItem.definirFk(itemInserido.id)
            newItem.definirObjetoFk(itemInserido)
            self.updateItem(newItem)

            self.listaItens.append(newItem)   
        finally:
            self.LoadTable() 
            self.frameEdicao.pack_forget()

    def updateItem(self, item):
        pass
    

    def onChangeItemClasse(self, event):
        if self.listaItens == None:
            return

        for item in self.listaItens:
            if item.pegarObjetoFk().nome == self.cbbItemClasse.get():
                self.updateComponents(item)
                return

        self.limparCamposEdicao()

    def updateComponents(self, item):
        pass

    def limparCamposEdicao(self):
        pass

    def __pegarItemClasseSelecionada(self):
        for item in self.__listItensClasse:
            if item.nome == self.cbbItemClasse.get(): 
                return item
        
        return None

    def validarCampos(self):
        return True

    def __btnExcluirClick(self):
        item = self.__pegarItemSelecionadoTreeView()
        if item == None:
            return

        self.listaItens.remove(item)
        self.listaItensExlusao.append(item)

        self.LoadTable()        


    def __pegarItemSelecionadoTreeView(self):
        curItem = self.treeView.focus()
        itemSelected = self.treeView.item(curItem)
        if itemSelected["text"] == "":
            return None

        itemClasseNome = itemSelected["values"][0]

        for item in self.listaItens:
            if item.pegarObjetoFk().nome == itemClasseNome:
                return item    

    def salvar(self, idPai):

        if self.listaItensExlusao != None:
            for item in self.listaItensExlusao:
                if item.id != None:
                    self.session.delete(item)
        if self.listaItens != None: 
            for item in self.listaItens:
                if item.id == None:
                    self.definirIdPaiNoItem(item, idPai)
                    self.session.add(item)
        
        self.session.commit()    

    def definirIdPaiNoItem(self, item, id):
        pass     

    def consultar(self, id):
        pass 

    def definirDescricao(self, descricao):
        self.treeView.heading('descicao', text=descricao, anchor='w')
        self.lbTitulo.config(text=descricao)

    def recolherCamposEdicao(self):
        self.frameEdicao.pack_forget()
    
    def definirBeforeOpenEdicao(self, beforeOpenEdicao):
        self.__beforeOpenEdicao = beforeOpenEdicao

    def definirHeightTreeView(self, height):
        self.treeView["height"] = height
    