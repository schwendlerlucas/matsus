

ECON_AQUISICAO_MATERIA_LOCAL = 'Aquisição de matéria-prima próxima ao local de sua aplicação'
ECON_CONTRATACAO_MAO_OBRA_LOCAL = 'Contratação de mão de obra local'

SOC_GRAU_POPULARIZACAO_CONCEITOS = 'Grau de popularização dos conceitos de sustentabilidade ao longo da cadeia produtiva do sistema construtivo'
SOC_GRAU_FOMENTO_INSTITUICOES = 'Grau de Fomento/apoio a instituições que promovem a sustentabilidade da construção civil'
SOC_GRAU_MATERIAIS_CULTURALMENTE = 'Grau de utilização de materiais culturalmente usados pela comunidade local'
SOC_GRAU_CONTRIBUICAO_EDIFICACAO = 'Grau de contribuição do sistema construtivo na edificação'
SOC_GRAU_COMPLEXIDADE = 'Grau de complexidade produtiva do sistema construtivo'
SOC_GRAU_POSIBILIDADE_MUTIROES = 'Possibilidade de produção do sistema construtivo por meio de mutirões'

TEC_PARCELA_PODE_SER_RECICLADA = 'Parcela que pode ser reciclada do sistema construtivo após o seu desuso'
TEC_PARCELA_PODE_SER_REAPROVEITADA = 'Parcela que pode ser reaproveitada do sistema construtivo após o seu desuso'
TEC_GRAU_ESTANQUEIDADE = 'Grau de estanqueidade do sistema construtivo'
TEC_GRAU_TRANSMITANCIA_TERMICA = 'Grau de transmitância térmica do sistema construtivo'
TEC_GRAU_TRANSMITANCIA_SONORA = 'Grau de transmissão de ondas sonoras do sistema construtivo'
TEC_COMPORTAMENTO_MECANICO = 'Comportamento mecânico do sistema construtivo aos esforços de: compressão, tração e abrasão'
TEC_DURABILIDADE = 'Durabilidade prevista para o sistema construtivo'
TEC_ACOMPANHAMENTO_TECNICO = 'Acompanhamento de profissionais habilitados durante as fases de produção e aplicação do sistema construtivo'
TEC_GRAU_PERECIBILIDADE = 'Grau de perecibilidade dos elementos que compõem o sistema construtivo'
TEC_FACILIDADE_ESTOCAGEM = 'Facilidade de estocagem dos elementos que compõem o sistema construtivo'
TEC_FACILIDADE_TRANSPORTE = 'Facilidade de transporte dos elementos que compõem o sistema construtivo'
TEC_FACILIDADE_MANUTENCAO_PERIODICAS = 'Facilidade da realização de manutenções periódicas no sistema construtivo'
TEC_FACILIDADE_REPAROS = 'Facilidade de realizar reparos no sistema construtivo'
TEC_PADRONIZACAO_REPLICACAO = 'Padronização na replicação do sistema construtivo'
TEC_VERSATILIDADE = 'Versatilidade da aplicação e uso do sistema construtivo'


ARRAY_INDICADORES_SISTEMA_CONS = [
    ECON_AQUISICAO_MATERIA_LOCAL,
    ECON_CONTRATACAO_MAO_OBRA_LOCAL,
    SOC_GRAU_POPULARIZACAO_CONCEITOS,
    SOC_GRAU_FOMENTO_INSTITUICOES,
    SOC_GRAU_MATERIAIS_CULTURALMENTE,
    SOC_GRAU_CONTRIBUICAO_EDIFICACAO,
    SOC_GRAU_COMPLEXIDADE,
    SOC_GRAU_POSIBILIDADE_MUTIROES,

    TEC_PARCELA_PODE_SER_RECICLADA,
    TEC_PARCELA_PODE_SER_REAPROVEITADA,
    TEC_GRAU_ESTANQUEIDADE,
    TEC_GRAU_TRANSMITANCIA_TERMICA,
    TEC_GRAU_TRANSMITANCIA_SONORA,
    TEC_COMPORTAMENTO_MECANICO,
    TEC_DURABILIDADE,
    TEC_ACOMPANHAMENTO_TECNICO,
    TEC_GRAU_PERECIBILIDADE,
    TEC_FACILIDADE_ESTOCAGEM,
    TEC_FACILIDADE_TRANSPORTE,
    TEC_FACILIDADE_MANUTENCAO_PERIODICAS,
    TEC_FACILIDADE_REPAROS,
    TEC_PADRONIZACAO_REPLICACAO,
    TEC_VERSATILIDADE] 