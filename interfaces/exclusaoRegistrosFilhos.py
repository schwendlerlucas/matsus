from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaterial import AtividadeMaterial
from models.atividadeMaoDeObra import AtividadeMaoDeObra
from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo

from controllers.projetoController import ProjetoController

def excluirFilhosAtividade(session, id):
    print("excluindo filhos atividade")
    itens = session.query(AtividadeEquipamento).filter(AtividadeEquipamento.atividade_id==id).all()
    for item in itens:
        session.delete(item)

    itens = session.query(AtividadeMaterial).filter(AtividadeMaterial.atividade_id==id).all()
    for item in itens:
        session.delete(item)

    itens = session.query(AtividadeMaoDeObra).filter(AtividadeMaoDeObra.atividade_id==id).all()
    for item in itens:
        session.delete(item)

def excluirFilhosSistemaConstrutivo(session, id):
    itens = session.query(SistemaConstrutivoAtividade).filter(SistemaConstrutivoAtividade.sistemaConstrutivo_id==id).all()
    for item in itens:
        session.delete(item)

def excluirFilhosProjeto(session, id):
    projetoController = ProjetoController(session)
    projetoController.deleteChildRecords(id)
