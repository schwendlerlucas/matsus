# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
from tkinter import ttk
from interfaces.frmBase import FrameBase
from interfaces.validacoesCampos import validarDoubleOuVazio, retornarZeroSeVazio
from interfaces.interfaceUtils import defineValorSeDiferenteDeNone
from models.equipamento import Equipamento

class FrameCadEquipamentoAmbiental(FrameBase):
    
    def __init__(self, parent=None, session=None, equipamento=None):
        super(FrameCadEquipamentoAmbiental, self).__init__(parent=parent, session=session)

        self.clearParent()

        self.__openProximaTela = None

        self.__voltarCadastroEquipamento = None
        
        self.__equipamentoDto = equipamento

        self.__openMenu = None

        self.__novoCadastro = None
        #print(self.__equipamentoDto)

        #Geometria da Janela
        parent.title("Cadastro de equipamento")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("950x780+10+10")

        #Elementos da Janela -> containers na Janela
        ###############################################
        #Container 1 - Logo

        frameTop = Frame()
        frameLogo = Frame()
        frameCampos = Frame()

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        self.label = Label(frameLogo, image = self.photo)
        ###############################################
        ###############################################
        #Container 2 - Textos
        espacos = " "
        for i in range(1, 100):
            espacos += " "

        self.titulo = Label(frameTop, text="Cadastro de Equipamento"+ espacos, font=14, height="3")
        self.esp = Label(frameTop, text="")
        #self.int = Label(text="Grau de intensidade")
        self.Samb = Label(frameCampos, text="Ambiental", font='bold')

        self.Samb1 = Label(frameCampos, text="Emissões", height="2")
        self.Samb11 = Label(frameCampos, text="Emissão de gases que agravam efeito estufa", height="2")
        self.Samb12 = Label(frameCampos, text="Emissão de gases que degradam a camada de ozônio", height="2")
        self.Samb13 = Label(frameCampos, text="Emissão de gases relacionados à formação de chuva ácida", height="2")
        self.Samb14 = Label(frameCampos, text="Emissão de gases tóxicos/ patogênicos", height="2")
        self.Samb15 = Label(frameCampos, text="Emissões de gases que contribuem para o processo de eutrofização", height="2")

        self.Samb2 = Label(frameCampos, text="Resíduos sólidos", height="2")
        self.Samb21 = Label(frameCampos, text="Quantidade de Resíduos Perigosos", height="2")
        self.Samb22 = Label(frameCampos, text="Quantidade gerada de resíduos não perigosos", height="2")
        self.Samb23 = Label(frameCampos, text="Quantidade de Resíduos radioativos", height="2")

        self.Samb3 = Label(frameCampos, text="Consumo de energia", height="2")
        self.Samb31 = Label(frameCampos, text="Quantidade total de energia primária não renovável empregada", height="2")
        self.Samb32 = Label(frameCampos, text="Quantidade total de energia primária renovável empregada", height="2")

        self.Samb4 = Label(frameCampos, text="Consumo de água", height="2")
        self.Samb41 = Label(frameCampos, text="Quantidade de água proveniente da rede de abastecimento", height="2")
        self.Samb42 = Label(frameCampos, text="Quantidade de água reutilizada, a partir de fonte alternativa", height="2")
        #Unidades
        self.Uamb11 = Label(frameCampos, text="kg CO2- eq", height="2")
        self.Uamb12 = Label(frameCampos, text="kg CFC11-eq", height="2")
        self.Uamb13 = Label(frameCampos, text="kg SO2-eq", height="2")
        self.Uamb14 = Label(frameCampos, text="kg PO4 3- eq", height="2")
        self.Uamb15 = Label(frameCampos, text="kg C2H4- eq", height="2")

        self.Ukg = Label(frameCampos, text="kg", height="2")
        self.Ukg1 = Label(frameCampos, text="kg", height="2")
        self.Ukg2 = Label(frameCampos, text="kg", height="2")

        self.UMJ = Label(frameCampos, text="MJ", height="2")
        self.UMJ1 = Label(frameCampos, text="MJ", height="2")

        self.UM3 = Label(frameCampos, text="m³", height="2")
        self.UM31 = Label(frameCampos, text="m³", height="2")

        self.Upts = Label(frameCampos, text="pontos", height="2")

        self.Ureais = Label(frameCampos, text="Reais /", height="2")

        self.UMpa = Label(frameCampos, text="MPa", height="2")

        self.UKgm = Label(frameCampos, text="Kg/m³", height="2")

        ###############################################
        ###############################################
        #Container 3 - Entray
        self.edtEmiEfeitoEstufaVar = StringVar()
        self.edtEmiEfeitoEstufa = Entry(frameCampos, width = "30", textvariable=self.edtEmiEfeitoEstufaVar, justify=RIGHT)
        self.edtEmiDegradamOzonioVar = StringVar()
        self.edtEmiDegradamOzonio = Entry(frameCampos, width = "30", textvariable=self.edtEmiDegradamOzonioVar, justify=RIGHT)
        self.edtEmiChuvaAcidaVar = StringVar()
        self.edtEmiChuvaAcida = Entry(frameCampos, width = "30", textvariable=self.edtEmiChuvaAcidaVar, justify=RIGHT)
        self.edtEmiToxicosPatogenicosVar = StringVar()
        self.edtEmiToxicosPatogenicos = Entry(frameCampos, width = "30", textvariable=self.edtEmiToxicosPatogenicosVar, justify=RIGHT)
        self.edtEmiContribEutroficacaoVar = StringVar()
        self.edtEmiContribEutroficacao = Entry(frameCampos, width = "30", textvariable=self.edtEmiContribEutroficacaoVar, justify=RIGHT)
        
        self.edtQtdResiduosPerigososVar = StringVar()
        self.edtQtdResiduosPerigosos = Entry(frameCampos, width = "30", textvariable=self.edtQtdResiduosPerigososVar, justify=RIGHT)
        self.edtQtdResiduosRadioativosVar = StringVar()
        self.edtQtdResiduosRadioativos = Entry(frameCampos, width = "30", textvariable=self.edtQtdResiduosRadioativosVar, justify=RIGHT)
        self.edtQtdResiduosNaoPerigososVar = StringVar()
        self.edtQtdResiduosNaoPerigosos = Entry(frameCampos, width = "30", textvariable=self.edtQtdResiduosNaoPerigososVar, justify=RIGHT)

        self.edtQtdEnergiaNaoRenovavelVar = StringVar()
        self.edtQtdEnergiaNaoRenovavel = Entry(frameCampos, width = "30", textvariable=self.edtQtdEnergiaNaoRenovavelVar, justify=RIGHT)
        self.edtQtdEnergiaRenovavelVar = StringVar()
        self.edtQtdEnergiaRenovavel = Entry(frameCampos, width = "30", textvariable=self.edtQtdEnergiaRenovavelVar, justify=RIGHT)

        self.edtQtdAguaRedeAbastecimentoVar = StringVar()
        self.edtQtdAguaRedeAbastecimento = Entry(frameCampos, width = "30", textvariable=self.edtQtdAguaRedeAbastecimentoVar, justify=RIGHT)
        self.edtQtdAguaReutilizadaVar = StringVar()
        self.edtQtdAguaReutilizada = Entry(frameCampos, width = "30", textvariable=self.edtQtdAguaReutilizadaVar, justify=RIGHT)
        ###############################################
        ###############################################
        #Container 3 - botoes
        self.btnVoltar = Button(frameTop, text="Voltar", width=20, command=self.btnVoltarClick)
        self.btnSeguinte = Button(frameTop, text="Cadastrar", width=20, command=self.__btnCadastrarClick)
        self.btnNovo = Button(frameTop, text="Novo", width=20, command=self.__btnNovoOnClick)
        ###############################################
        ###############################################

        frameTop.grid(row=1, column=2)
        frameCampos.grid(row=2, column=2)
        
        
        frameLogo.grid(row=1, column=1)
        # Posição dos Elementos
        #Logo
        self.label.grid(row=0, column=0, rowspan=4, sticky=S)
        #Strings coluna 1
        self.titulo.grid(row=2, column=1, sticky=W)
        self.esp.grid(row=1, column=1)

        #self.Samb.grid(row=2, column=1, columnspan=3)
        self.Samb1.grid(row=3, column=1, sticky=W)
        self.Samb11.grid(row=4, column=1, sticky=E)
        self.Samb12.grid(row=5, column=1, sticky=E)
        self.Samb13.grid(row=6, column=1, sticky=E)
        self.Samb14.grid(row=7, column=1, sticky=E)
        self.Samb15.grid(row=8, column=1, sticky=E)

        self.esp.grid(row=9, column=1)

        self.Samb2.grid(row=10, column=1, sticky=W)
        self.Samb21.grid(row=11, column=1, sticky=E)
        self.Samb23.grid(row=12, column=1, sticky=E)
        self.Samb22.grid(row=13, column=1, sticky=E)

        self.esp.grid(row=14, column=1)

        self.Samb3.grid(row=15, column=1, sticky=W)
        self.Samb31.grid(row=16, column=1, sticky=E)
        self.Samb32.grid(row=17, column=1, sticky=E)

        self.esp.grid(row=18, column=1)

        self.Samb4.grid(row=19, column=1, sticky=W)
        self.Samb41.grid(row=20, column=1, sticky=E)
        self.Samb42.grid(row=21, column=1, sticky=E)
        #Entray coluna 2

        self.edtEmiEfeitoEstufa.grid(row=4, column=2)
        self.edtEmiDegradamOzonio.grid(row=5, column=2)
        self.edtEmiChuvaAcida.grid(row=6, column=2)
        self.edtEmiToxicosPatogenicos.grid(row=7, column=2)
        self.edtEmiContribEutroficacao.grid(row=8, column=2)




        self.edtQtdResiduosPerigosos.grid(row=11, column=2)
        self.edtQtdResiduosRadioativos.grid(row=12, column=2)
        self.edtQtdResiduosNaoPerigosos.grid(row=13, column=2)

        self.edtQtdEnergiaNaoRenovavel.grid(row=16, column=2)
        self.edtQtdEnergiaRenovavel.grid(row=17, column=2)

        self.edtQtdAguaRedeAbastecimento.grid(row=20, column=2)
        self.edtQtdAguaReutilizada.grid(row=21, column=2)
        
        #unidades coluna 3
        self.Uamb11.grid(row=4, column=3, sticky=W)
        self.Uamb12.grid(row=5, column=3, sticky=W)
        self.Uamb13.grid(row=6, column=3, sticky=W)
        self.Uamb14.grid(row=7, column=3, sticky=W)
        self.Uamb15.grid(row=8, column=3, sticky=W)

        self.Ukg.grid(row=11, column=3, sticky=W)
        self.Ukg1.grid(row=12, column=3, sticky=W)
        self.Ukg2.grid(row=13, column=3, sticky=W)

        self.UMJ.grid(row=16, column=3, sticky=W)
        self.UMJ1.grid(row=17, column=3, sticky=W)

        self.UM3.grid(row=20, column=3, sticky=W)
        self.UM31.grid(row=21, column=3, sticky=W)
        self.esp.grid(row=22, column=2)


        #Botões
        self.btnVoltar.grid(row=1, column=4, pady=2)
        self.btnSeguinte.grid(row=2, column=4, pady=2)
        self.btnNovo.grid(row=3, column=4, pady=2)

        self.lbWarnings = Label(text="", height="2", foreground="red")
        self.lbWarnings.grid(row=23, column=2)

        if self.__equipamentoDto != None:
            self.__carregarValoresCampos()



    def __btnCadastrarClick(self):
        self.lbWarnings.configure(text = "")

        if not self.__validarCampos():
            return False

        if self.__equipamentoDto == None:
            self.__equipamentoDto = Equipamento()

        self.__carregarValoresDto()

        if self.__equipamentoDto.id == None:
            self.session.add(self.__equipamentoDto)

        self.session.commit()

        self.lbWarnings.configure(text="Equipamento cadastrado", foreground="black")
        self.btnVoltar.configure(text="Menu", command=self.__openMenu)



    
    def definirVoltarMenu(self, onClickVoltarMenu):
        self.__openMenu = onClickVoltarMenu

    def __validarCampos(self):
        valorCorreto = validarDoubleOuVazio(self.edtEmiEfeitoEstufa.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtEmiDegradamOzonio.get()) 
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtEmiChuvaAcida.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtEmiToxicosPatogenicos.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtEmiContribEutroficacao.get())

        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtQtdResiduosPerigosos.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtQtdResiduosRadioativos.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtQtdResiduosNaoPerigosos.get())

        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtQtdEnergiaNaoRenovavel.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtQtdEnergiaRenovavel.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtQtdAguaRedeAbastecimento.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtQtdAguaReutilizada.get())


        if not valorCorreto:
            self.lbWarnings.configure(text = "Campos acima só aceitam números", foreground="red")
            return False    
        
        return True

    def __carregarValoresDto(self):
        self.__equipamentoDto.emiEfeitoEstufa = retornarZeroSeVazio(self.edtEmiEfeitoEstufa.get())
        self.__equipamentoDto.emiDegradamOzonio = retornarZeroSeVazio(self.edtEmiDegradamOzonio.get())
        self.__equipamentoDto.emiChuvaAcida = retornarZeroSeVazio(self.edtEmiChuvaAcida.get())
        self.__equipamentoDto.emiToxicosPatogenicos = retornarZeroSeVazio(self.edtEmiToxicosPatogenicos.get())
        self.__equipamentoDto.emiContribEutroficacao = retornarZeroSeVazio(self.edtEmiContribEutroficacao.get())
        #ambiental - resíduos
        self.__equipamentoDto.qtdResiduosPerigosos = retornarZeroSeVazio(self.edtQtdResiduosPerigosos.get())
        #self.__equipamentoDto.intensidadeResiduosPerigosos = self.cbIntensidadeResiduosPerigosos.get()
        self.__equipamentoDto.qtdResiduosRadioativos = retornarZeroSeVazio(self.edtQtdResiduosRadioativos.get())
        #self.__equipamentoDto.intensidadeResiduosRadioativos = self.cbIntensidadeResiduosRadioativos.get()
        self.__equipamentoDto.qtdResiduosNaoPerigosos = retornarZeroSeVazio(self.edtQtdResiduosNaoPerigosos.get())
        #ambiental - consumo energia
        self.__equipamentoDto.qtdEnergiaNaoRenovavel = retornarZeroSeVazio(self.edtQtdEnergiaNaoRenovavel.get())
        self.__equipamentoDto.qtdEnergiaRenovavel = retornarZeroSeVazio(self.edtQtdEnergiaRenovavel.get())
        #ambiental - consumo agua
        self.__equipamentoDto.qtdAguaRedeAbastecimento = retornarZeroSeVazio(self.edtQtdAguaRedeAbastecimento.get())
        self.__equipamentoDto.qtdAguaReutilizada = retornarZeroSeVazio(self.edtQtdAguaReutilizada.get())

    def __carregarValoresCampos(self):

        defineValorSeDiferenteDeNone(self.__equipamentoDto.emiEfeitoEstufa, self.edtEmiEfeitoEstufaVar)
        defineValorSeDiferenteDeNone(self.__equipamentoDto.emiDegradamOzonio, self.edtEmiDegradamOzonioVar)
        defineValorSeDiferenteDeNone(self.__equipamentoDto.emiChuvaAcida, self.edtEmiChuvaAcidaVar)
        defineValorSeDiferenteDeNone(self.__equipamentoDto.emiToxicosPatogenicos, self.edtEmiToxicosPatogenicosVar)
        defineValorSeDiferenteDeNone(self.__equipamentoDto.emiContribEutroficacao, self.edtEmiContribEutroficacaoVar)
        #ambiental - resíduos
        defineValorSeDiferenteDeNone(self.__equipamentoDto.qtdResiduosPerigosos, self.edtQtdResiduosPerigososVar)
        #defineValorSeDiferenteDeNone(self.__equipamentoDto.intensidadeResiduosPerigosos, self.cbIntensidadeResiduosPerigosos)
        defineValorSeDiferenteDeNone(self.__equipamentoDto.qtdResiduosRadioativos, self.edtQtdResiduosRadioativosVar)
        #defineValorSeDiferenteDeNone(self.__equipamentoDto.intensidadeResiduosRadioativos, self.cbIntensidadeResiduosRadioativos)
        defineValorSeDiferenteDeNone(self.__equipamentoDto.qtdResiduosNaoPerigosos, self.edtQtdResiduosNaoPerigososVar)
        #ambiental - consumo energia
        defineValorSeDiferenteDeNone(self.__equipamentoDto.qtdEnergiaNaoRenovavel, self.edtQtdEnergiaNaoRenovavelVar)
        defineValorSeDiferenteDeNone(self.__equipamentoDto.qtdEnergiaRenovavel, self.edtQtdEnergiaRenovavelVar)
        #ambiental - consumo agua
        defineValorSeDiferenteDeNone(self.__equipamentoDto.qtdAguaRedeAbastecimento, self.edtQtdAguaRedeAbastecimentoVar)
        defineValorSeDiferenteDeNone(self.__equipamentoDto.qtdAguaReutilizada, self.edtQtdAguaReutilizadaVar)


    def definirVoltar(self, onClickVoltar):
        self.__voltarCadastroEquipamento = onClickVoltar

    def btnVoltarClick(self):
        self.__carregarValoresDto()
        self.__voltarCadastroEquipamento(self.__equipamentoDto)
    
    def definirNovoCadastro(self, onClick):
        self.__novoCadastro = onClick

    def __btnNovoOnClick(self):
        self.__novoCadastro()