# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.interfaceUtils import aplicarMascaraE
from datetime import datetime

from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaterial import AtividadeMaterial
from interfaces.calculoIndicadores.calculoIndicadoresPorItem import CalculoIndicadoresPorItem
from interfaces.calculoIndicadores.calculoIndicadoresSoma import CalculoIndicadoresSoma
from interfaces.calculoIndicadores.calculoInidicadoresResult import CalculoIndicadoresResult
from interfaces.frmCardIndicadoresAmbientais import FrameCardIndicadoresAmbientais


from interfaces.frmCardItensSistemaConstrutivo import FrameCardItensSistemaConstrutivo


class FrameIndicadoresAmbientais(FrameBase):
    
    def __init__(self, parent=None, session=None, atividade=None):
        super(FrameIndicadoresAmbientais, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__atividadeDto = atividade

        self.__listaItensCalculados = []
        
        parent.title("Calculos relativos para a dimensão Ambiental")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1000x620+100+100")
        self.__createUI()

        self.__carregarAtividade()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        labelTitulo = Label(self.frameTop, text="Indicadores Ambientais", font=14, height="3")
        labelTitulo.pack(side=LEFT)

        frmCampo = Frame(self.frameTop)
        frmCampo.pack(side=BOTTOM)

        frmLabels = Frame(frmCampo, width=1)
        frmLabels.pack(side=LEFT)
        self.labelAtividade = Label(frmLabels, text="Pesquise por atividade", font=10, height="2")
        self.labelAtividade.pack(side=RIGHT)

        frmEntries = Frame(frmCampo, width=30)
        frmEntries.pack(side=RIGHT)

        btnBugImagem = Button(frmEntries, command=self.__onclickButtonBug)

        self.frmTreeViews = Frame()
        self.frmTreeViews.pack(side=BOTTOM, fill=X, expand=1)

        self.treeViewMaterial = FrameCardIndicadoresAmbientais(self.frmTreeViews, self.session, None, AtividadeMaterial)
        self.treeViewMaterial.pack(side=TOP, fill=X)
        self.treeViewMaterial.definirHeightTreeView(2)
        self.treeViewMaterial.definirDescricao('Materiais')
        self.treeViewMaterial.DefinirDescricaoTotal('TOTAIS por m² fase (creadle to gate)')        

        self.treeViewEquipamento = FrameCardIndicadoresAmbientais(self.frmTreeViews, self.session, None, AtividadeEquipamento)
        self.treeViewEquipamento.pack(side=TOP, fill=X)
        self.treeViewEquipamento.definirHeightTreeView(2)
        self.treeViewEquipamento.definirDescricao('Equipamentos')
        self.treeViewEquipamento.DefinirDescricaoTotal('TOTAIS por m² fase (Aplicação)')

        self.treeViewTotais = FrameCardIndicadoresAmbientais(self.frmTreeViews, self.session, None, AtividadeMaterial)
        self.treeViewTotais.pack(side=TOP, fill=X)
        self.treeViewTotais.definirHeightTreeView(1)
        self.treeViewTotais.definirDescricao('Totais')
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10)
        self.btnVoltar.pack(side=LEFT, padx=15, pady=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15, pady=15)

        
    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick
    

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick


    def __onclickButtonBug(self):
        pass

    def __carregarAtividade(self):
        if self.__atividadeDto == None:
            return

        self.labelAtividade.config(text=self.__atividadeDto.nome)

        self.treeViewEquipamento.DefinirAtividade(self.__atividadeDto)
        self.treeViewMaterial.DefinirAtividade(self.__atividadeDto)

        listaItens = self.treeViewEquipamento.listaItensCalculados
        listaItens += self.treeViewMaterial.listaItensCalculados

        self.treeViewTotais.ExibirTotais(listaItens, 'TOTAIS por m² de sistema Construtivo')









        


        





