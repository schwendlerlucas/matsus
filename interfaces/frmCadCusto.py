# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime

from models.custo import Custo
from models.maoObra import MaoObra
from models.equipamento import Equipamento
from models.material import Material




class FrameCadCusto(FrameBase):
    
    def __init__(self, parent=None, session=None, custo=None):
        super(FrameCadCusto, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__custoDto = custo


        #Geometria da Janela
        parent.title("Cadastro mão de obra")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("700x390+100+100")

        #Elementos da Janela -> containers na Janela
        ###############################################
        #Container 1 - Logo
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        self.label = Label(image = self.photo)
        ###############################################

        ###############################################
        #Container 2 - Textos
        self.titulo = Label(text="Cadastro de custo unitário", font=14, height="3")
        self.lbEspacoAntesCampos = Label(text="")
        
        self.lbLocal = Label(text="Local")
        self.lbEspaco1= Label()
        self.lbClasse = Label(text="Classe")
        self.lbEspaco2 = Label()
        self.lbItemClasse = Label(text=LISTA_CLASSE_CUSTOS[0])
        self.lbEspaco3= Label()
        self.lbCustoUnitario = Label(text="Valor unitário")
        self.lbUnidade = Label(text="R$/Unidade")
        self.lbDataCotacao = Label(text="Data cotação")
        self.lbEspaco4 = Label()
        self.lbWarnings = Label(text="", height="2")
        ###############################################



        ###############################################
        #Container 3 - botoes
        self.btnPesquisar = Button(text="Pesquisar", width=10)
        self.btnVoltar = Button(text="Voltar", width=10)
        self.btnCadastrar = Button(text="Cadastrar", width=20, command=self.__cadastrarCusto)
        self.btnNovo = Button(text="Novo", width=10, command=self.__btnNovoOnClick)
        ###############################################

        ###############################################
        #Container 3 - Entray
        self.edtCustoVar = StringVar()
        self.edtCusto = Entry(width = "22", textvariable=self.edtCustoVar)

        self.edtDataCotacaoVar = StringVar()
        self.edtDataCotacao = Entry(width = "22", textvariable=self.edtDataCotacaoVar)
        ###############################################

        ###############################################
        #Container 5 - ComboBox
        self.cbbLocal = ttk.Combobox(state='readonly')
        self.cbbLocal["values"] = LISTA_ESTADOS
        self.cbbLocal.bind('<<ComboboxSelected>>', self.__onChangeLocal)
        self.cbbLocal.current(0)

        self.cbbClasse = ttk.Combobox(state='readonly')
        self.cbbClasse["values"] = LISTA_CLASSE_CUSTOS
        self.cbbClasse.bind('<<ComboboxSelected>>', self.__onChangeClasse)
        self.cbbClasse.current(0)

        self.cbbItemClasse = ttk.Combobox(state='readonly') 
        self.cbbItemClasse.bind('<<ComboboxSelected>>', self.__onChangeItemClasse)

        self.edtUnidadeVar = StringVar()
        self.edtUnidade = Entry(width = "22", textvariable=self.edtUnidadeVar)
        ###############################################
        # Posição dos Elementos

        #Logo
        self.label.grid(row=0, column=0, rowspan=4, sticky=S)


        #Strings coluna 1
        self.titulo.grid(row=0, column=1, sticky=W)
        self.lbEspacoAntesCampos.grid(row=1, column=1)

        self.lbLocal.grid(row=2, column=1, sticky=E)
        self.cbbLocal.grid(row=2, column=2, sticky=W)

        self.lbEspaco1.grid(row=3, column=1, sticky=E)

        self.lbClasse.grid(row=4, column=1, sticky=E)
        self.cbbClasse.grid(row=4, column=2, sticky=W)

        self.lbEspaco2.grid(row=5, column=1, sticky=E)

        self.lbItemClasse.grid(row=6, column=1, sticky=E)
        self.cbbItemClasse.grid(row=6, column=2, sticky=W)

        self.lbEspaco3.grid(row=7, column=1, sticky=E)

        self.lbCustoUnitario.grid(row=8, column=1, sticky=E)
        self.edtCusto.grid(row=8, column=2, sticky=W, pady=3)

        self.lbUnidade.grid(row=9, column=1, sticky=E)
        self.edtUnidade.grid(row=9, column=2, sticky=W, pady=3)

        self.lbDataCotacao.grid(row=10, column=1, sticky=E)
        self.edtDataCotacao.grid(row=10, column=2, sticky=W, pady=3)

        #self.lbEspaco4.grid(row=11, column=1, sticky=E)

        self.lbWarnings.grid(row=12, column=1)

        self.btnPesquisar.grid(row=13, column=1) 
        self.btnVoltar.grid(row=13, column=0)
        self.btnCadastrar.grid(row=13, column=2)
        self.btnNovo.grid(row=13, column=3, padx=60)

        self.__definirItensClasse()
        self.__carregarValoresCampos(True)  

    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick
    
    def __cadastrarCusto(self):
        if not self.__validarValores():
            return
        
        if self.__custoDto == None:
            print("criou dto")
            self.__custoDto = Custo()

        print("id dto ", self.__custoDto.id)

        self.__custoDto.estado = self.cbbLocal.get()
        self.__definirIdClasseSelecionadaNoDTO()
        self.__custoDto.custoMedio = float(self.edtCusto.get())
        self.__custoDto.unidade = self.edtUnidade.get()

        self.__custoDto.dtCotacao = datetime.strptime(self.edtDataCotacao.get(), '%d/%m/%Y').date()

        print(self.__custoDto.estado)
        print(self.__custoDto.equipamento_id)
        print(self.__custoDto.maoDeObra_id)
        print(self.__custoDto.material_id)
        print(self.__custoDto.equipamento)
        print(self.__custoDto.maoDeObra)
        print(self.__custoDto.material)
        print(self.__custoDto.custoMedio)
        print(self.__custoDto.unidade)
        print(self.__custoDto.dtCotacao)


        if self.__custoDto.id == None:
            self.session.add(self.__custoDto)

        self.session.commit()

        self.lbWarnings.configure(text="Custo cadastrado com sucesso", foreground="black")

    def __carregarValoresCampos(self, carregarChaves):
        self.__limparCampos()

        if self.__custoDto == None:
            return

        if carregarChaves:
            self.cbbLocal.set(self.__custoDto.estado)
            self.__selecionarClasseFromDto()
            self.__selecionarItemClasseFromDto()

        self.edtCustoVar.set(self.__custoDto.custoMedio)
        self.edtUnidadeVar.set(self.__custoDto.unidade)
        self.edtDataCotacaoVar.set(self.__custoDto.dtCotacao.strftime("%d/%m/%Y"))


    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick


    def __validarValores(self):
        if not validarData(self.edtDataCotacao.get()):
            self.lbWarnings.configure(text = "Data da cotação inválida", foreground="red")
            return False 

        if not validarDouble(self.edtCusto.get()):
            self.lbWarnings.configure(text = "Custo inválido", foreground="red")
            return False  

        return True 

    def __onChangeClasse(self, event):
        self.lbWarnings.configure(text = "")
        self.__limparCampos()
        self.__definirItensClasse()

    def __onChangeLocal(self, event):
        self.lbWarnings.configure(text = "")
        self.__limparCampos()
        self.__onChangeItemClasse(None)



    def __definirItensClasse(self):
        self.lbItemClasse.config(text=self.cbbClasse.get())            
        self.cbbItemClasse.set("")

        self.__popularCbbItemClasse(self.__retornaModelClasseSelecionada())

    def __popularCbbItemClasse(self, model):    
        self.__listaClasse = self.session.query(model).filter().order_by(model.nome).all()
        listaCombo = []
        for item in self.__listaClasse:
            listaCombo.append(item.nome)

        self.cbbItemClasse["values"] = listaCombo

    def __onChangeItemClasse(self, event):
        self.lbWarnings.configure(text = "")
        custo = self.__consultarCustoDeAcordoComClasseSelecionada()
        self.__custoDto = custo
        #if self.__custoDto == None:
        #    self.__custoDto = Custo()


        self.__carregarValoresCampos(False)

    def __classeSelecionadaEhEquipamento(self, fromDto=False):
        if fromDto and (self.__custoDto != None):
            return self.__custoDto.equipamento != None        
        return (self.cbbClasse.get() == LISTA_CLASSE_CUSTOS[INDEX_CLASSE_EQUIPAMENTO])

    def __classeSelecionadaEhMaoDeObra(self, fromDto=False):
        if fromDto and (self.__custoDto != None):
            return self.__custoDto.maoDeObra != None        
        return (self.cbbClasse.get() == LISTA_CLASSE_CUSTOS[INDEX_CLASSE_MAO_DE_OBRA])

    def __classeSelecionadaEhMaterial(self, fromDto=False):
        if fromDto and (self.__custoDto != None):
            return self.__custoDto.material != None

        return (self.cbbClasse.get() == LISTA_CLASSE_CUSTOS[INDEX_CLASSE_MATERIAL])

    def __retornaModelClasseSelecionada(self):
        if self.__classeSelecionadaEhEquipamento():
            return Equipamento
        
        if self.__classeSelecionadaEhMaoDeObra():
            return MaoObra
        
        if self.__classeSelecionadaEhMaterial():
            return Material

    def __consultarCustoDeAcordoComClasseSelecionada(self):
        idClasse = self.__retornarIdClasseSelecionada()
        estadoSel = self.cbbLocal.get()
        if self.__classeSelecionadaEhEquipamento():
            return self.session.query(Custo).filter_by(estado=estadoSel, equipamento_id=idClasse).first()
        
        if self.__classeSelecionadaEhMaoDeObra():
            return self.session.query(Custo).filter_by(estado=estadoSel, maoDeObra_id=idClasse).first()
        
        if self.__classeSelecionadaEhMaterial():
            return self.session.query(Custo).filter_by(estado=estadoSel, material_id=idClasse).first()

    def __retornarIdClasseSelecionada(self):

        for classe in self.__listaClasse:
            print(classe.nome)
            if classe.nome == self.cbbItemClasse.get():
                return classe.id

        return -1

    def __definirIdClasseSelecionadaNoDTO(self):
        self.__custoDto.equipamento_id = None
        self.__custoDto.maoDeObra_id = None
        self.__custoDto.material_id = None

        idClasseSelecionada = self.__retornarIdClasseSelecionada()

        if self.__classeSelecionadaEhEquipamento():
            self.__custoDto.equipamento_id = idClasseSelecionada
        
        if self.__classeSelecionadaEhMaoDeObra():
            self.__custoDto.maoDeObra_id = idClasseSelecionada
        
        if self.__classeSelecionadaEhMaterial():
            self.__custoDto.material_id = idClasseSelecionada

    def __selecionarClasseFromDto(self):
        if self.__classeSelecionadaEhEquipamento(True):
            self.cbbClasse.set(LISTA_CLASSE_CUSTOS[INDEX_CLASSE_EQUIPAMENTO])

        elif self.__classeSelecionadaEhMaoDeObra(True):
            self.cbbClasse.set(LISTA_CLASSE_CUSTOS[INDEX_CLASSE_MAO_DE_OBRA])
        
        elif self.__classeSelecionadaEhMaterial(True):
            self.cbbClasse.set(LISTA_CLASSE_CUSTOS[INDEX_CLASSE_MATERIAL])

    def __selecionarItemClasseFromDto(self):
        if self.__classeSelecionadaEhEquipamento(True):
            self.cbbItemClasse.set(self.__custoDto.equipamento.nome)

        elif self.__classeSelecionadaEhMaoDeObra(True):
            self.cbbItemClasse.set(self.__custoDto.maoDeObra.nome)
        
        elif self.__classeSelecionadaEhMaterial(True):
            self.cbbItemClasse.set(self.__custoDto.material.nome)
        

    def __limparCampos(self):
        self.edtCustoVar.set("")
        self.edtUnidadeVar.set("")
        self.edtDataCotacaoVar.set("")

    def __btnNovoOnClick(self):
        self.__limparCampos()
        self.cbbItemClasse.set("")
        self.cbbItemClasse.focus()