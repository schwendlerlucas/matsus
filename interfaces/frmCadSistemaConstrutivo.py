# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime


from models.atividade import Atividade
from models.sistemaConstrutivo import SistemaConstrutivo
from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade

from interfaces.frmCardItensSistemaConstrutivo import FrameCardItensSistemaConstrutivo


class FrameCadSistemaConstrutivo(FrameBase):
    
    def __init__(self, parent=None, session=None, sistemaConstrutivo=None):
        super(FrameCadSistemaConstrutivo, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__sistemaConstrutivoDto = sistemaConstrutivo
        self.__voltarCadastroSistemaConstrutivo = None
        self.__voltarMenu = None
        
        parent.title("Cadastro de Sistema Construtivo")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("800x630+100+100")
        self.__createUI()

        self.__carregarValorCampos()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        
        #frameImage = Frame(master=self.frameTop)
        #frameImage.pack(side=)
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=20)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Cadastro de Sistema Construtivo", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frmLabels = Frame(frameCabecalho, width=1)
        frmLabels.pack(side=LEFT)
        labelSistemaContrutivo = Label(frmLabels, text="                                                 Sistema construtivo")
        labelSistemaContrutivo.pack(side=TOP, fill=X, pady=5)
        labelLocal = Label(frmLabels, text="                        Estado para a composição de preços ")
        labelLocal.pack(side=TOP, fill=X, pady=5)

        
        frmCampo = Frame(frameCabecalho)
        frmCampo.pack(side=RIGHT)

        frmEntries = Frame(frmCampo, width=30)
        frmEntries.pack(side=RIGHT)
        self.lbNomeSistema = Label(frmEntries, width = "30")
        self.lbNomeSistema.pack(side=TOP, fill=X, pady=5)

        self.cbbLocal = ttk.Combobox(frmEntries, state='readonly')
        self.cbbLocal["values"] = LISTA_ESTADOS
        self.cbbLocal.bind('<<ComboboxSelected>>', self.__onChangeLocal)
        self.cbbLocal.current(0)
        self.cbbLocal.pack(side=TOP, fill=X, pady=5)

        self.frmTreeViews = Frame()
        self.frmTreeViews.pack(fill=BOTH, side=TOP)

        self.frameAtividade = FrameCardItensSistemaConstrutivo(self.frmTreeViews, self.session, Atividade, SistemaConstrutivoAtividade)
        self.frameAtividade.definirDescricao("Atividade")
        self.frameAtividade.definirBeforeOpenEdicao(self.__beforeOpenEdicao)
        self.frameAtividade.definirHeightTreeView(10)
        self.frameAtividade.pack(fill=BOTH, side=TOP, padx=6, pady=6)

        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=15)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10, command=self.__btnVoltarClick)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnCadastrar = Button(frmBotoes, text="Cadastrar", width=20, command=self.__cadastrarOnClick)       
        self.btnCadastrar.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)

        self.lbWarnings = Label(frmWarning, text="", height="2")
        self.lbWarnings.pack()


    def __cadastrarOnClick(self):
        self.__recolherCamposEdicao()
        if not self.__validarCampos():
            return

        inserindo = self.__sistemaConstrutivoDto.id == None
        if inserindo:
            self.session.add(self.__sistemaConstrutivoDto)

        self.session.commit()
        self.frameAtividade.salvar(self.__sistemaConstrutivoDto.id)        
        self.session.commit()

        if inserindo:
            self.lbWarnings.configure(text = "Sistema contrutivo cadastrado com sucesso", foreground="black")        
        else:
            self.lbWarnings.configure(text = "Sistema contrutivo atualizado com sucesso", foreground="black")    

        self.btnVoltar["command"] = self.__voltarMenu
        self.btnVoltar["text"] = "Menu"


    
    def __validarCampos(self):
        self.lbWarnings.configure(text = "")
        if self.__sistemaConstrutivoDto == None:
           self.__sistemaConstrutivoDto = SistemaConstrutivo()     

        return True
        
    def definirVoltarMenu(self, onClick):
        self.__voltarMenu = onClick
    
    def __beforeOpenEdicao(self):
        self.__recolherCamposEdicao()

    def __recolherCamposEdicao(self):
        self.frameAtividade.recolherCamposEdicao()

    def __carregarValorCampos(self):
        if self.__sistemaConstrutivoDto == None:
            return

        self.lbNomeSistema.config(text=self.__sistemaConstrutivoDto.nome)

        if self.__sistemaConstrutivoDto.id == None:
            return

        self.frameAtividade.consultar(self.__sistemaConstrutivoDto.id, self.cbbLocal.get())

    def __onChangeLocal(self, event):
        self.__carregarValorCampos()

    def definirVoltar(self, onClickVoltar):
        self.__voltarCadastroSistemaConstrutivo = onClickVoltar       

    def __btnVoltarClick(self):
        self.__voltarCadastroSistemaConstrutivo(self.__sistemaConstrutivoDto)



