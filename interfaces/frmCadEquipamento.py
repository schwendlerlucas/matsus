# Cadastro Materiais 4.1
from tkinter import *
from tkinter import ttk
from interfaces.frmBase import FrameBase
from interfaces.validacoesCampos import retornarZeroSeVazio, validarDoubleOuVazio
from models.equipamento import Equipamento

class FrameCadEquipamento(FrameBase):

    def __init__(self, parent=None, session=None, equipamento=None):
        super(FrameCadEquipamento, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__openProximaTela = None
        self.__voltarCadastroMaterial = None
        self.__equipamentoDto = equipamento

        #Geometria da Janela
        parent.title("Cadastro de equipamentos")
        parent.geometry ("720x520+100+100")

        #Elementos da Janela -> containers na Janela
        ###############################################
        #Container 1 - Logo
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        self.label = Label(image = self.photo)
        ###############################################
        ###############################################
        #Container 2 - Textos
        self.lb1 = Label(text="Cadastro de equipamentos", font=14, height="3")
        self.lb2 = Label(text="")
        self.lb3 = Label(text="Nome do equipamento", height="2")
        self.lb4 = Label(text="Fonte de energia", height="2")

        self.lb5 = Label(text="Peso", height="2")
        self.lb6 = Label(text="Porte", height="2")
        self.lbConsumoAgua = Label(text="Consumo de água", height="2")
        self.lb8 = Label(text="Consumo Medio Diesel", height="2")
        self.lb9 = Label(text="Consumo médio de energia", height="2")
        self.lb10 = Label(text="")
        ###############################################
        # Labels unidades
        self.lbUnidadePeso = Label(text="kg", height="2")
        self.lbUnidadeConsumoMedioAgua = Label(text="m³/h", height="2")
        self.lbUnidadeConsumoMedioDiesel = Label(text="L/h", height="2")
        self.lbUnidadeConsumoMedioEnergia = Label(text="KWh", height="2")

        ###############################################
        ###############################################
        #Container 3 - Entray
        self.edtNomeEquipametoVar = StringVar()
        self.edtNomeEquipameto = Entry(width = "58", textvariable=self.edtNomeEquipametoVar)

        self.edtPesoVar = StringVar()
        self.edtPeso = Entry(width = "58", textvariable=self.edtPesoVar)
        self.edtConsumoAguaVar = StringVar()
        self.edtConsumoAgua = Entry(width = "58", textvariable=self.edtConsumoAguaVar)
        self.edtConsumoMedioDieselVar = StringVar()
        self.edtConsumoMedioDiesel = Entry(width = "58", textvariable=self.edtConsumoMedioDieselVar)
        self.edtConsumoMedioEnergiaVar = StringVar()
        self.edtConsumoMedioEnergia = Entry(width = "58", textvariable=self.edtConsumoMedioEnergiaVar)
        ###############################################
        ###############################################
        #Container 3 - botoes
        self.btnPesquisar = Button(text="Pesquisar", width=10)
        self.btnVoltar = Button(text="Voltar", width=10)
        self.btnProximaTela = Button(text="Seguinte", width=20, command=self.__btnProximaTelaClick)
        ###############################################
        ###############################################
        fontesDeEnergia = ["Elétrica", "Diesel", "Gasolina", "Manual", "Outro"]

        self.cbFonteEnergia = ttk.Combobox()
        self.cbFonteEnergia["values"] = fontesDeEnergia
        self.cbFonteEnergia.current(0)

        portes = ["Pequeno", "Médio", "Grande"]

        self.cbPorte = ttk.Combobox()
        self.cbPorte["values"] = portes
        self.cbPorte.current(0)


        self.cbEhMeioTransporteVar = IntVar()
        self.cbEhMeioTransporte = Checkbutton(text="É meio de transporte?", height=3, variable=self.cbEhMeioTransporteVar, command=self.__ehTransportClick)
        ###############################################
        # Posição dos Elementos
        #Logo
        self.label.grid(row=0, column=0, rowspan=3, sticky=S)
        #Strings
        self.lb1.grid(row=0, column=1, sticky=W, columnspan=2)
        self.lb2.grid(row=1, column=1, sticky=E)
        self.lb3.grid(row=2, column=1, sticky=E)
        self.lb4.grid(row=3, column=1, sticky=E)
        self.lb5.grid(row=4, column=1, sticky=E)
        self.lb6.grid(row=5, column=1, sticky=E)
        self.lbConsumoAgua.grid(row=6, column=1, sticky=E)
        self.lb8.grid(row=7, column=1, sticky=E)
        self.lb9.grid(row=8, column=1, sticky=E)
        self.lb10.grid(row=9, column=1, sticky=E)
        #Entray
        self.edtNomeEquipameto.grid(row=2, column=2, sticky=E)
        self.edtPeso.grid(row=4, column=2, sticky=W)
        self.cbPorte.grid(row=5, column=2, sticky=W)
        self.edtConsumoMedioDiesel.grid(row=7, column=2, sticky=E)
        self.edtConsumoMedioEnergia.grid(row=8, column=2, sticky=E)
        #Botões
        self.btnPesquisar.grid(row=12, column=1)
        self.btnVoltar.grid(row=12, column=0)
        self.btnProximaTela.grid(row=12, column=2)

        self.cbFonteEnergia.grid(row=3, column=2, sticky=W)
        self.edtConsumoAgua.grid(row=6, column=1, columnspan=2, sticky=E)

        self.lbWarnings = Label(text="", height="2")
        self.lbWarnings.grid(row=11, column=2)
        # Labels Unidades
        self.lbUnidadePeso.grid(row=4, column=3, sticky=E)
        self.lbUnidadeConsumoMedioAgua.grid(row=6, column=3, sticky=E)
        self.lbUnidadeConsumoMedioDiesel.grid(row=7, column=3, sticky=E)
        self.lbUnidadeConsumoMedioEnergia.grid(row=8, column=3, sticky=E)
        self.cbEhMeioTransporte.grid(row=10, column=2, sticky=W)


        if self.__equipamentoDto != None:
            self.__carregarValoresCampos()


    def __btnProximaTelaClick(self):
        if not self.__validarCampos():
            return False

        self.__equipamentoDto.nome = self.edtNomeEquipameto.get()
        self.__equipamentoDto.fonteEnergia = self.cbFonteEnergia.get()
        self.__equipamentoDto.peso = self.edtPeso.get()
        self.__equipamentoDto.porte = self.cbPorte.get()
        self.__equipamentoDto.consumoAgua = retornarZeroSeVazio(self.edtConsumoAgua.get())
        self.__equipamentoDto.consumoMedioDiesel = retornarZeroSeVazio(self.edtConsumoMedioDiesel.get())
        self.__equipamentoDto.consumoMedioEnergia = retornarZeroSeVazio(self.edtConsumoMedioEnergia.get())
        self.__equipamentoDto.ehMeioTransporte = self.cbEhMeioTransporteVar.get()


        self.__openProximaTela(self.__equipamentoDto)

    def __validarCampos(self):
        self.lbWarnings.configure(text = "")
        if self.__equipamentoDto == None:
           self.__equipamentoDto = Equipamento()

        nomeEquipamento = self.edtNomeEquipameto.get()
        if (nomeEquipamento == ''):
            self.lbWarnings.configure(text = "O 'nome equipamento' é obrigatório", foreground="red")
            return False

        equipamento = self.session.query(Equipamento).filter_by(nome=nomeEquipamento).first()
        if (equipamento != None and equipamento.id != self.__equipamentoDto.id):
            self.lbWarnings.configure(text = "Equipamento já cadastrada", foreground="red")

            return False


        valorCorreto = validarDoubleOuVazio(self.edtPeso.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtConsumoAgua.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtConsumoMedioDiesel.get())
        valorCorreto = valorCorreto and validarDoubleOuVazio(self.edtConsumoMedioEnergia.get())

        if not valorCorreto:
            self.lbWarnings.configure(text = "Campos acima só aceitam números", foreground="red")
            return False

        return True

    def __carregarValoresCampos(self):
        self.edtNomeEquipametoVar.set(self.__equipamentoDto.nome)
        self.cbFonteEnergia.set(self.__equipamentoDto.fonteEnergia)
        self.edtPesoVar.set(self.__equipamentoDto.peso)
        self.cbPorte.set(self.__equipamentoDto.porte)
        self.edtConsumoAguaVar.set(self.__equipamentoDto.consumoAgua)
        self.edtConsumoMedioDieselVar.set(self.__equipamentoDto.consumoMedioDiesel)
        self.edtConsumoMedioEnergiaVar.set(self.__equipamentoDto.consumoMedioEnergia)
        self.cbEhMeioTransporteVar.set(self.__equipamentoDto.ehMeioTransporte)

        self.__ajustarLabelConsumoMedio()


    def definirTelaSeguinteCadEquipamento(self, onClickProximaTela):
        self.__openProximaTela = onClickProximaTela

    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick

    def __ehTransportClick(self):
        self.__ajustarLabelConsumoMedio()


    def __ajustarLabelConsumoMedio(self):
        if self.cbEhMeioTransporteVar.get():
            self.lbUnidadeConsumoMedioDiesel.config(text="KM/L")
        else:
            self.lbUnidadeConsumoMedioDiesel.config(text="L/H")
