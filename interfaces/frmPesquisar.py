from tkinter import *
from interfaces.frmBase import FrameBase
from interfaces.frmPopUpConfirmacao import PopUpConfirmacao
from interfaces.frmPopUpMensagem import PopUpMensagem


class FramePesquisa(FrameBase):
    def __init__(self, parent=None, session=None):
        super(FramePesquisa, self).__init__(parent=parent, session=session)

        self.clearParent()

        self.__pesquisar = None
        self.__onSelecionar = None
        self.__listItens = None
        self.__listarItem = None
        self.__validadorExclusao = None
        self.__exclusaoFilhos = None
        self.__preparadorRetorno = None

        parent.geometry('600x400+100+100')
        parent.title("Pesquisa")


        self.scrollbar = Scrollbar()
        self.scrollbar.pack(side = RIGHT, fill=Y )

        self.listPesquisa = Listbox(selectmode=EXTENDED)

        self.scrollbar.config(command = self.listPesquisa.yview)

        self.edtPesquisa = Entry(width = "30")
        self.edtPesquisa.pack(fill=BOTH, pady=2)

        self.btnCadastrar = Button(text="Pesquisar", width=20, command=self.__pesquisarClick)
        self.btnCadastrar.pack(fill=BOTH)

        self.listPesquisa.pack(fill=BOTH, expand=1)

        self.btnSelecionar = Button(text="Selecionar", width=20, command=self.__selecionarItem)
        self.btnSelecionar.pack(fill=BOTH)

        self.btCancelar = Button(text="Cancelar", width=20)
        self.btCancelar.pack(fill=BOTH)

        self.btnExcluir = Button(text="Excluir", width=20, command=self.__excluirItem)
        self.btnExcluir.pack(fill=BOTH)

        self.edtPesquisa.focus()


    def definirCancelar(self, onClick):
        self.btCancelar["command"] = onClick

    def definirOnSelecionar(self, onSelecionar):
        self.__onSelecionar = onSelecionar

    def definirPesquisar(self, pesquisar):
        self.__pesquisar = pesquisar

    def __pesquisarClick(self):
        self.listPesquisa.delete(0, END)
        
        textoPesquisado = self.edtPesquisa.get()
        if textoPesquisado == "":
            return False


        self.__listItens = self.__pesquisar(textoPesquisado)


        for item in self.__listItens:
            if self.__listarItem != None: 
                self.__listarItem(self.listPesquisa, item) 
            else:
                self.listPesquisa.insert(END, item.nome)


    def definirListarItem(self, listarItem):
        self.__listarItem = listarItem

    
    def __excluirItem(self):
        itemSelecionado = self.listPesquisa.curselection()
        if len(itemSelecionado) != 1:
            return False
        
        PopUpConfirmacao(self.parent, self.__callBackConfirmacaoExcluir)

    def __callBackConfirmacaoExcluir(self, confirmado):
        print(confirmado)
        if not confirmado:
            return

        itemSelecionado = self.listPesquisa.curselection()
        if len(itemSelecionado) != 1:
            return False
        
        index = itemSelecionado[0]

        item = self.__listItens[index]

        if not self.__validarExclusao(item.id):
            PopUpMensagem(self.parent, None, "Não será possivel excluir este item porque ele possui dependentes.")   
            return False

        
        print("excluiu")
        self.session.delete(item)
        self.__excluirFilhos(item.id)
        self.session.commit()

        self.__pesquisarClick()

    def __selecionarItem(self):
        itemSelecionado = self.listPesquisa.curselection()
        if len(itemSelecionado) != 1:
            return False
        
        index = itemSelecionado[0]

        item = self.__listItens[index]

        if self.__preparadorRetorno != None:
            item = self.__preparadorRetorno(item)

        self.__onSelecionar(item)

    def definirValidadorExclusao(self, validador):
        self.__validadorExclusao = validador 

    def __validarExclusao(self, id):
        if self.__validadorExclusao == None:
            return True
        
        return self.__validadorExclusao(self.session, id)

    def definirExlusaoFilhos(self, exclusaoFilhos):
        self.__exclusaoFilhos = exclusaoFilhos

    def __excluirFilhos(self, id):
        if self.__exclusaoFilhos != None:
            self.__exclusaoFilhos(self.session, id)

    def DefinirPreparadorRetorno(self, preparadorRetorno):
        self.__preparadorRetorno = preparadorRetorno




 