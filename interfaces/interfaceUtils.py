

def defineValorSeDiferenteDeNone(valorStr, varText):
    if valorStr != None:
        varText.set(aplicarMascaraE(valorStr))

def definirValorComboSimNao(valorBool, combo):
    if valorBool == None:
       pass
    
    index = 1
    if valorBool:
        index = 0

    combo.current(index)   

def aplicarMascaraE(valor):
    try:
        return " %0.2e" % float(valor)
    except:
        print("aplicarMascaraE valor:" +str(valor))
        return valor

def aplicarMascaraRS(valor):
    try:
        return "R$ " + str(round(float(valor), 2))
    except:
        print("aplicarMascaraE valor:" + str(valor))
        return "R$ " + valor

def aplicarMascaraPerc(valor):
    try:
        return str(round(float(valor) * 100, 2)) + " %"
    except:
        print("aplicarMascaraE valor:" + str(valor))
        return valor + " %"

def aplicarMascaraDec(valor):
    try:
        return str(round(float(valor), 2)) 
    except:
        print("aplicarMascaraE valor:" + str(valor))
        return valor