from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.constantesViews import *
from interfaces.frmCardItensBase import FrameCardItensBase
from interfaces.validacoesCampos import *
from interfaces.frmBase import FrameBase

from DTOs.cadProjetoDto import CadProjetoDTO

from controllers.sistemaConstrutivoAtividadeController import SistemaConstrutivoAtividadeController

class FrameCardAtividadesProjeto(FrameBase):

    def __init__(self, parent, session, cadProjetoDTO: CadProjetoDTO=None):
        super(FrameCardAtividadesProjeto, self).__init__(parent, session=session)
        self.__cadProjetoDTO = cadProjetoDTO
        self.__sistema_id = 0
        self.__beforeOpenEdicao = None
        self.__descricaoAtividadeEmEdicao = ""
        self.__atividadeSistemaList = None

        self.CreateUI()


    def CreateUI(self):
        self.frameTitulo = Frame(master=self)
        self.frameTitulo.pack(side=TOP, fill=X)
        self.frameTreeViewEBotoes = Frame(master=self)
        self.frameTreeViewEBotoes.pack(side=TOP, fill=X, after=self.frameTitulo)

        self.frameBotoes = Frame(master=self.frameTreeViewEBotoes)
        self.frameBotoes.pack(side=RIGHT)


        self.frameTreeView = Frame(master=self.frameTreeViewEBotoes)
        self.frameEdicao = Frame(master=self)
        self.treeView = ttk.Treeview(master=self.frameTreeView, height=3)

        self.lbTitulo = Label(master=self.frameTitulo, text="Atividades")
        self.lbTitulo.pack(side=LEFT)


        self.scrollbar = Scrollbar(master=self.frameTreeView)
        self.scrollbar.pack(fill=Y, side = RIGHT)
        self.scrollbar.config(command = self.treeView.yview)
    
        self.treeView.configure(xscrollcommand=self.scrollbar.set)
        self.treeView.pack(expand=1, fill=BOTH, side=LEFT)
        self.frameTreeView.pack(side=LEFT, fill=BOTH, expand=1)

        self.btnEditar = Button(master=self.frameBotoes, text="Editar", width=5, comman=self.__btnEditarClick)
        self.btnEditar.pack(pady=3, padx=2)

        self.treeView['columns'] = ('quantidade', 'grauSalubridade', 'grauSeguridade')
        self.treeView.heading("#0", text='Atividades', anchor='w')
        self.treeView.column("#0", anchor='w', width=300)
        self.treeView.heading('quantidade', text='Quantidade', anchor='n')
        self.treeView.column('quantidade', anchor='n', width=100)
        self.treeView.heading("grauSalubridade", text='Grau de Salubridade', anchor='w')
        self.treeView.column("grauSalubridade", anchor='w', width=150)
        self.treeView.heading("grauSeguridade", text='Grau de Seguridade', anchor='w')
        self.treeView.column("grauSeguridade", anchor='w', width=150)
        self.treeView.pack(side=TOP)        


        frameLabelsEdicao = Frame(self.frameEdicao)
        frameLabelsEdicao.pack(side=LEFT)

        labelGrauSalubridade = Label(frameLabelsEdicao, text="Grau de Salubridade ")
        labelGrauSalubridade.pack(side=TOP, fill=X, pady=5)
        labelGrauSeguridade = Label(frameLabelsEdicao, text="Grau de Seguridade ")
        labelGrauSeguridade.pack(side=TOP, fill=X, pady=5)

        frameCombosEdicao = Frame(self.frameEdicao)
        frameCombosEdicao.pack(side=LEFT)

        self.cbbGrauSalubridade = ttk.Combobox(frameCombosEdicao, state='readonly')
        self.cbbGrauSalubridade["values"] = LISTA_SALUBRIDADE
        self.cbbGrauSalubridade.current(0)
        self.cbbGrauSalubridade.pack(side=TOP,fill=X, pady=5)

        self.cbbGrauSeguridade = ttk.Combobox(frameCombosEdicao, state='readonly')
        self.cbbGrauSeguridade["values"] = LISTA_SEGURIDADE
        self.cbbGrauSeguridade.current(0)
        self.cbbGrauSeguridade.pack(side=TOP,fill=X, pady=5)

        self.btnIncluir = Button(master=self.frameEdicao, text="ok", command=self.__btnOkClick, width=2)
        self.btnIncluir.pack(side=RIGHT, padx=5, pady=5)

    def loadAtividades(self, sistema_id):
        self.__sistema_id = sistema_id
        self.__atividadeSistemaList = self.__cadProjetoDTO.getSistemaAtividadeSalubridadeSeguridadeList(sistema_id)
        if len(self.__atividadeSistemaList) == 0:
            atividadeSistemaDTO = self.__getAtividadesProjetoNovo()
            self.__cadProjetoDTO.setSistemaAtividadeSalubridadeSeguridadeList(sistema_id, atividadeSistemaDTO.atividadeList)
            self.__atividadeSistemaList = atividadeSistemaDTO.atividadeList
        
        self.__loadTreeView()
                

    def __loadTreeView(self):
        for i in self.treeView.get_children():
            self.treeView.delete(i)

        for item in self.__atividadeSistemaList:
            self.treeView.insert('', END, text=item.atividade_nome, 
                                values=(item.quantidade, int(item.grauSalubridade), 
                                        int(item.grauSeguridade)))

        sistemaConstrutivoAtividadeController = SistemaConstrutivoAtividadeController(self.session)
        salubridadeAvg, seguridadeAvg, quantidadeSum = sistemaConstrutivoAtividadeController.\
            getAvgSalubridadeSeguridadeAndSumQuantidade(self.__atividadeSistemaList)

        self.treeView.insert('', END, text="Total",
                            values=(quantidadeSum, salubridadeAvg, seguridadeAvg))

    def __getAtividadesProjetoNovo(self):
        sistemaConstrutivoAtividadeController = SistemaConstrutivoAtividadeController(self.session)
        listaAtividades = sistemaConstrutivoAtividadeController.getAtividadesByIdSistemaContrutivo(self.__sistema_id)

        return listaAtividades


    def setBeforeOpenEdicao(self, beforeOpenEdicao):
        self.__beforeOpenEdicao = beforeOpenEdicao

    def __btnEditarClick(self):
        if self.__beforeOpenEdicao != None:
            self.__beforeOpenEdicao()

        curItem = self.treeView.focus()
        itemSelected = self.treeView.item(curItem)
        if itemSelected["text"] == "":
            return False
        if itemSelected["text"] == "Total":
            return False

        self.__descricaoAtividadeEmEdicao = itemSelected["text"]
        
        self.cbbGrauSalubridade.set(itemSelected["values"][1])
        self.cbbGrauSeguridade.set(itemSelected["values"][2])

        self.frameEdicao.pack(side=BOTTOM, fill=X)
    
    def __btnOkClick(self):

        try:
            for item in self.__atividadeSistemaList:
                if item.atividade_nome == self.__descricaoAtividadeEmEdicao:
                    self.__updateItem(item)
                    return 
        finally:
            self.__descricaoAtividadeEmEdicao = ""
            self.__loadTreeView() 
            self.frameEdicao.pack_forget()
    
    def __updateItem(self, itemAtividade):
        itemAtividade.grauSalubridade = int(self.cbbGrauSalubridade.get())
        itemAtividade.grauSeguridade = int(self.cbbGrauSeguridade.get())

    def setListOnCadProjetoDTO(self):
        if self.__sistema_id > 0:
            self.__cadProjetoDTO.setSistemaAtividadeSalubridadeSeguridadeList(self.__sistema_id, self.__atividadeSistemaList)
    
    def recolherCamposEdicao(self):
        self.frameEdicao.pack_forget()
       