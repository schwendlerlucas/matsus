# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
from tkinter import ttk
from interfaces.frmBase import FrameBase
from models.maoObra import MaoObra


class FrameCadMaoDeObra(FrameBase):
    
    def __init__(self, parent=None, session=None, maoDeObra=None):
        super(FrameCadMaoDeObra, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__maoDeObraDto = maoDeObra


        #Geometria da Janela
        parent.title("Cadastro mão de obra")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("700x250+100+100")

        #Elementos da Janela -> containers na Janela
        ###############################################
        #Container 1 - Logo
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        self.label = Label(image = self.photo)
        ###############################################

        ###############################################
        #Container 2 - Textos
        self.titulo = Label(text="Cadastro de mão de obra", font=14, height="3")
        self.esp = Label(text="")
        self.esp1 = Label(text="")
        self.funcao = Label(text="Função")
        self.Nivel = Label(text="Nível de formação mínima", height="2")
        ###############################################


        self.lbWarnings = Label(text="", height="2")

        ###############################################
        #Container 3 - botoes
        self.btnPesquisar = Button(text="Pesquisar", width=10)
        self.btnVoltar = Button(text="Voltar", width=10)
        self.btnCadastrar = Button(text="Cadastrar", width=20)
        self.btnCadastrar["command"] = self.cadastrarMaoObra
        ###############################################

        ###############################################
        #Container 3 - Entray
        self.edtFuncaoVar = StringVar()
        self.edtFuncao = Entry(width = "30", textvariable=self.edtFuncaoVar)
        ###############################################

        ###############################################
        #Container 5 - Caixa suspensa
        int1_5 = ["Sem mínima formação exigida", "Nível superior", "Tecnólogo", "Técnico de nivel médio"]

        self.cbbNivel = ttk.Combobox()
        self.cbbNivel["values"] = int1_5
        self.cbbNivel.current(0)

        ###############################################
        # Posição dos Elementos

        #Logo
        self.label.grid(row=0, column=0, rowspan=4, sticky=S)


        #Strings coluna 1
        self.titulo.grid(row=0, column=1, sticky=W)
        self.esp.grid(row=1, column=1)

        self.funcao.grid(row=2, column=1, sticky=E)
        self.Nivel.grid(row=3, column=1, sticky=E)
        self.edtFuncao.grid(row=2, column=2, sticky=E)
        self.cbbNivel.grid(row=3, column=2, sticky=W, )

        self.esp1.grid(row=5, column=1)


        self.lbWarnings.grid(row=6, column=1)

        self.btnPesquisar.grid(row=7, column=1) 
        self.btnVoltar.grid(row=7, column=0)
        self.btnCadastrar.grid(row=7, column=2)

        if self.__maoDeObraDto != None:
            self.__carregarValoresCampos()

    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick
    
    def cadastrarMaoObra(self):
        self.lbWarnings.configure(text = "")
        funcao = self.edtFuncao.get()
        if (funcao == ''):
            self.lbWarnings.configure(text = "Campo função obrigatório", foreground="red")
            return False

        if self.__maoDeObraDto == None:
            self.__maoDeObraDto = MaoObra()

        maoObra = self.session.query(MaoObra).filter_by(nome=funcao).first()
        if (maoObra != None and maoObra.id != self.__maoDeObraDto.id):
            self.lbWarnings.configure(text = "Função já cadastrada", foreground="red")
            return False 


        self.__maoDeObraDto.nome = funcao
        self.__maoDeObraDto.nivelFormacao = self.cbbNivel.get()

        if self.__maoDeObraDto.id == None:
            self.session.add(self.__maoDeObraDto)

        self.session.commit()

        self.lbWarnings.configure(text="Função cadastrada com sucesso", foreground="black")

        self.cbbNivel.current(0)
        self.edtFuncaoVar.set("")
        self.__maoDeObraDto = None

    def __carregarValoresCampos(self):
        self.edtFuncaoVar.set(self.__maoDeObraDto.nome)
        self.cbbNivel.set(self.__maoDeObraDto.nivelFormacao)

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick

    