# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
from tkinter import ttk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.interfaceUtils import aplicarMascaraE
from datetime import datetime

from models.projeto import Projeto
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from models.sistemaConstrutivo import SistemaConstrutivo

from interfaces.frmCardResultadoProjeto import FrameCardResultadoProjeto

from DTOs.cadProjetoDto import CadProjetoDTO


from controllers.sistemaConstrutivoAtividadeController import SistemaConstrutivoAtividadeController
from controllers.projetoController import ProjetoController

from interfaces.constantesIndicadoresSistemaConstrutivo import *

from controllers.sistemaConstrutivoResultadoAmbientalController import SistemaConstrutivoResultadoAmbientalController
from controllers.sistemaConstrutivoResultadoController import SistemaConstrutivoResultadoController

from controllers.projetoSistemasNormatizadoController import *
from controllers.calculadorResultadoFinalComFatorDecisao import CalculadorResultadoFinalComFatorDecisao

import matplotlib.pyplot as pyplot


class FrameParecerMelhorSistema(FrameBase):
    
    def __init__(self, parent=None, session=None, cadProjetoDTO:CadProjetoDTO=None):
        super(FrameParecerMelhorSistema, self).__init__(parent=parent, session=session)

        self.clearParent()
       
        self.__projetoDto = None
        self.__cadProjetoDTO = cadProjetoDTO
        if self.__cadProjetoDTO != None:
            self.__projetoDto = cadProjetoDTO.projeto

        self.__proximaTela = None
        self.__voltarTelaAnterior = None
        self.__openMenu = None
        self.__listaSistemasComResultado = []
        
        self.__title = "Desempenho sustentável"
        parent.title(self.__title)
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1200x800+100+20")
        self.__createUI()

        self.__consultarSistemas()
        self.__calcularResultados()
        self.__ordenarListaDesc()
        self.__configurarTreeView()
        self.__carregarValorCampos()
        self.__loadResult() 

    def __calcularResultados(self):
        controller = ProjetoSistemasNormatizadoController(self.session, self.__projetoDto.id, self.__projetoDto.estado)
        controller.Calc()

        listaResultados = controller.GetListaResultados()
        listaResultadosNormatizados = controller.GetListaResultadosNormatizados()

        controllerFatoresDecisao = CalculadorResultadoFinalComFatorDecisao(self.session)
        controllerFatoresDecisao.Calc(listaResultados, listaResultadosNormatizados)

        self.__listaSistemasComResultado = listaResultados

    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text=self.__title, font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frameCampos = Frame()
        frameCampos.pack(side=TOP)

        frmLabels = Frame(frameCampos, width=1)
        frmLabels.pack(side=LEFT)
        labelProjeto = Label(frmLabels, text="Nome do Projeto ", anchor='ne')
        labelProjeto.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Quantidade total ", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Estado de referência para custo", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)


        frmEntries = Frame(frameCampos, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtProjetoVar = StringVar()
        self.edtProjeto = Entry(frmEntries, width = "30", textvariable=self.edtProjetoVar, state='readonly')
        self.edtProjeto.pack(side=TOP, fill=X, pady=5)

        self.edtQuantidadeVar = StringVar()
        self.edtQuantidade = Entry(frmEntries, width = "30", textvariable=self.edtQuantidadeVar, state='readonly')
        self.edtQuantidade.pack(side=TOP, fill=X, pady=5)
        if self.__projetoDto != None:
            self.edtQuantidadeVar.set(self.__projetoDto.qtdSistemaConstrutivo)

        self.edtLocalVar = StringVar()
        self.edtLocal = Entry(frmEntries, width = "30", textvariable=self.edtLocalVar, state='readonly')
        self.edtLocal.pack(side=TOP,fill=X, pady=5)
        if self.__projetoDto != None:
            self.edtLocalVar.set(self.__projetoDto.estado)

        self.frmTreeView = Frame()
        self.frmTreeView.pack(fill=BOTH, side=TOP, expand=1)

        frmTreeViewScrollBar = Frame(self.frmTreeView)
        frmTreeViewScrollBar.pack(side=TOP, fill=BOTH, expand=1, padx=5)

        self.treeView = ttk.Treeview(master=frmTreeViewScrollBar, height=4)

        self.scrollbarV = Scrollbar(master=frmTreeViewScrollBar)
        self.scrollbarV.pack(fill=Y, side=RIGHT, pady=15)
        self.scrollbarV.config(command = self.treeView.yview)

        self.treeView.configure(yscrollcommand=self.scrollbarV.set, height=15)
        self.treeView.pack(pady=15, fill=BOTH, expand=1)
        
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10, command=self.__btnVoltarOnClick)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15)
        self.btnGrafico = Button(frmBotoes, text="Grafico", width=10, command=self.__btnGraficoOnClick)
        self.btnGrafico.pack(side=LEFT, padx=15)
        self.btnOpenMenu = Button(frmBotoes, text="Menu", width=20, command=self.__menuOnClick)
        self.btnOpenMenu.pack(side=RIGHT, padx=15)

    def __menuOnClick(self):
        self.__openMenu()
            

    def __btnVoltarOnClick(self):        
        self.__voltarTelaAnterior()

    def definirVoltar(self, voltarTelaAnterior):
        self.__voltarTelaAnterior = voltarTelaAnterior

    def __carregarValorCampos(self):
        if self.__cadProjetoDTO == None:
            return 
        self.edtProjetoVar.set(self.__projetoDto.nome)

    def definirTelaMenu(self, openMenu):
        self.__openMenu = openMenu
        
        
    def __loadResult(self):
        for i in self.treeView.get_children():
            self.treeView.delete(i)

        if self.__cadProjetoDTO == None:
            return

        for i  in range(0, len(self.__listaSistemasComResultado)):
            sistema = self.__listaSistemasComResultado[len(self.__listaSistemasComResultado) - 1 - i]
            self.treeView.insert('', END, text=sistema.sistema_nome, 
                            values=((str(round(sistema.ResultadoFinal * 100, 2)) + " % "), " - "))
                                                        

    def __configurarTreeView(self):
        self.treeView['columns'] = ('resultado')
        self.treeView.heading("#0", text=' Sistema Construtivo', anchor='w')
        self.treeView.column("#0", anchor='w', width=600)
        self.treeView.heading('resultado', text='Resultado', anchor='ne')
        self.treeView.column('resultado', anchor='ne', width=100)
        

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick

    def __consultarSistemas(self):
        estado = self.__projetoDto.estado
        controllerAmbiental = SistemaConstrutivoResultadoAmbientalController(self.session)
        controller = SistemaConstrutivoResultadoController(self.session)

        self.__listaSistemas = []
        for sistema in self.__cadProjetoDTO.projetoSistemaConstrutivoList:
            resultados = controllerAmbiental.Calc(self.__cadProjetoDTO.projeto.id, sistema.sistemaConstrutivo_id, estado)
            controller.Calc(self.__cadProjetoDTO.projeto.id, sistema.sistemaConstrutivo_id, estado, resultados)
                
            self.__listaSistemas.append(resultados)


        for sistema in self.__cadProjetoDTO.projetoSistemaConstrutivoList:
            resultados = controllerAmbiental.Calc(self.__cadProjetoDTO.projeto.id, sistema.sistemaConstrutivo_id, estado)
            controller.Calc(self.__cadProjetoDTO.projeto.id, sistema.sistemaConstrutivo_id, estado, resultados)

    def __ordenarListaDesc(self):
        self.__listaSistemasComResultado = sorted(self.__listaSistemasComResultado, key=lambda x: x.ResultadoFinal)


    def __btnGraficoOnClick(self):
        if self.__cadProjetoDTO == None:
            return
        
        x_list = []
        labels_list = []
        for sistema in self.__listaSistemasComResultado:
            x_list.append(sistema.ResultadoFinal)
            labels_list.append(sistema.sistema_nome)

        pyplot.axis("equal")
        pyplot.pie(x_list, labels=labels_list, autopct="%1.1f%%")

        pyplot.title(self.__title)
        pyplot.show()
