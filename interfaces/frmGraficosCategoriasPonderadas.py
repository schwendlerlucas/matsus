# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.interfaceUtils import aplicarMascaraE, aplicarMascaraPerc
from datetime import datetime

from models.projeto import Projeto

from DTOs.cadProjetoDto import CadProjetoDTO

from controllers.projetoSistemasNormatizadoController import ProjetoSistemasNormatizadoController

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

class FrameGraficosCategoriasPonderadas(FrameBase):
    
    def __init__(self, parent=None, session=None, cadProjetoDTO:CadProjetoDTO=None):
        super(FrameGraficosCategoriasPonderadas, self).__init__(parent=parent, session=session)

        self.clearParent()
       
        self.__projetoDto = None
        self.__cadProjetoDTO = cadProjetoDTO
        if self.__cadProjetoDTO != None:
            self.__projetoDto = cadProjetoDTO.projeto

        self.__proximaTela = None
        self.__voltarTelaAnterior = None
        self.__openMenu = None
        self.__listaResultados = []
        self.__listaResultadosNormatizados = []
        
        self.__title = "Graficos categorias ponderadas"
        parent.title(self.__title)
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("800x600+100+20")
        self.__createUI()

        self.__consultarSistemas()
        self.__carregarValorCampos()

        

    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text=self.__title, font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frameCampos = Frame()
        frameCampos.pack(side=TOP)

        frmLabels = Frame(frameCampos, width=1)
        frmLabels.pack(side=LEFT)
        labelProjeto = Label(frmLabels, text="Nome do Projeto ", anchor='ne')
        labelProjeto.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Quantidade total ", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Estado de referência para custo", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)


        frmEntries = Frame(frameCampos, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtProjetoVar = StringVar()
        self.edtProjeto = Entry(frmEntries, width = "30", textvariable=self.edtProjetoVar, state='readonly')
        self.edtProjeto.pack(side=TOP, fill=X, pady=5)

        self.edtQuantidadeVar = StringVar()
        self.edtQuantidade = Entry(frmEntries, width = "30", textvariable=self.edtQuantidadeVar, state='readonly')
        self.edtQuantidade.pack(side=TOP, fill=X, pady=5)
        if self.__projetoDto != None:
            self.edtQuantidadeVar.set(self.__projetoDto.qtdSistemaConstrutivo)

        self.edtLocalVar = StringVar()
        self.edtLocal = Entry(frmEntries, width = "30", textvariable=self.edtLocalVar, state='readonly')
        self.edtLocal.pack(side=TOP,fill=X, pady=5)
        if self.__projetoDto != None:
            self.edtLocalVar.set(self.__projetoDto.estado)



        frmBotoesGraficos = Frame()
        frmBotoesGraficos.pack(fill=BOTH, side=TOP, expand=1)
        
        btnAI = Button(frmBotoesGraficos, text="AI", width=20, command=self.__btnAIClick)
        btnAI.pack(side=TOP, padx=30)

        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10, command=self.__btnVoltarOnClick)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15)
        self.btnOpenMenu = Button(frmBotoes, text="Menu", width=20, command=self.__menuOnClick)
        self.btnOpenMenu.pack(side=RIGHT, padx=15)


    def __btnAIClick(self):
        # data to plot
        n_groups = 4
        means_frank = (90, 55, 40, 65)
        means_guido = (85, 62, 54, 20)
        means_test1 = (90, 55, 40, 65)
        means_test2 = (90, 55, 40, 65)

        # create plot
        fig, ax = plt.subplots()
        index = np.arange(n_groups)
        bar_width = 0.35 
        opacity = 0.8

        rects1 = plt.bar(index, means_frank, bar_width,
        alpha=opacity,
        color='b',
        label='Frank')
        index += 0.35

        rects2 = plt.bar(index + bar_width, means_guido, bar_width,
        alpha=opacity,
        color='g',
        label='Guido')

        rects3 = plt.bar(index + bar_width * 2, means_test1, bar_width,
        alpha=opacity,
        color='red',
        label='teste1')

        rects4 = plt.bar(index + bar_width * 3, means_test2, bar_width,
        alpha=opacity,
        color='black',
        label='teste2')

        plt.xlabel('Person')
        plt.ylabel('Scores')
        plt.title('Scores by person')
        plt.xticks(index + bar_width, ('AI', 'AII', 'AIII', 'AIV'))
        plt.legend()

        plt.tight_layout()
        plt.show()


    def __btnAIClick_(self):
        x_list = []
        labels_list = []
        prior = []
        for i in range(0, len(self.__listaResultadosNormatizados)):
            labels_list.append(self.__listaResultados[i].sistema_nome)
            x_list.append(self.__listaResultadosNormatizados[i].AI)

        y_pos = np.arange(len(x_list))

        plt.barh(y_pos, x_list, align='center', alpha=0.5)
        plt.yticks(y_pos, labels_list)
        plt.xlabel('')
        plt.title('Categoria AI')

        plt.show()


    def __menuOnClick(self):
        self.__openMenu()
            

    def __btnVoltarOnClick(self):        
        self.__voltarTelaAnterior(self.__cadProjetoDTO)

    def definirVoltar(self, voltarTelaAnterior):
        self.__voltarTelaAnterior = voltarTelaAnterior

    def __carregarValorCampos(self):
        if self.__cadProjetoDTO == None:
            return 
        self.edtProjetoVar.set(self.__projetoDto.nome)

    def definirTelaMenu(self, openMenu):
        self.__openMenu = openMenu
                                                                                       
        

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick

    def __retornarArrayValores(self, getValor):
        result = []
        for i in range(0, len(self.__listaResultadosNormatizados)):
            result.append(aplicarMascaraPerc(getValor(i)))

        return result

    def __consultarSistemas(self):
        if self.__projetoDto == None:
            return

        controller = ProjetoSistemasNormatizadoController(self.session, self.__projetoDto.id, self.__projetoDto.estado)
        controller.Calc()

        self.__listaResultados = controller.GetListaResultados()
        self.__listaResultadosNormatizados = controller.GetListaResultadosNormatizados()

