# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
from tkinter import ttk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime

from models.projeto import Projeto
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from models.sistemaConstrutivo import SistemaConstrutivo

from interfaces.frmCardSistemaConstrutivo import FrameCardSistemaConstrutivo
from interfaces.frmCardAtividadesProjeto import FrameCardAtividadesProjeto
from interfaces.frmCardInsumoProjetoMeioTransporteEquipamento import FrameCardInsumoProjetoMeioTransporteEquipamento
from interfaces.frmCardInsumoProjetoMeioTransporteMaterial import FrameCardInsumoProjetoMeioTransporteMaterial

from DTOs.cadProjetoDto import CadProjetoDTO


from controllers.sistemaConstrutivoAtividadeController import SistemaConstrutivoAtividadeController
from controllers.projetoController import ProjetoController




class FrameCadProjetoComparaSistemas(FrameBase):
    
    def __init__(self, parent=None, session=None, cadProjetoDTO:CadProjetoDTO=None):
        super(FrameCadProjetoComparaSistemas, self).__init__(parent=parent, session=session)

        self.clearParent()
       
        self.__projetoDto = None
        self.__cadProjetoDTO = cadProjetoDTO
        if self.__cadProjetoDTO != None:
            self.__projetoDto = cadProjetoDTO.projeto

        self.__proximaTela = None
        self.__voltarTelaAnterior = None

        
        parent.title("Cadastro de projeto")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1200x800+100+20")
        self.__createUI()

        self.__carregarValorCampos()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Cadastro de projeto", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frameCampos = Frame()
        frameCampos.pack(side=TOP)

        frmLabels = Frame(frameCampos, width=1)
        frmLabels.pack(side=LEFT)
        labelProjeto = Label(frmLabels, text="Nome do Projeto ", anchor='ne')
        labelProjeto.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Escolha o sistema ", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=6)
        labelSistema = Label(frmLabels, text="Quantidade total ", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)


        frmEntries = Frame(frameCampos, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtProjetoVar = StringVar()
        self.edtProjeto = Entry(frmEntries, width = "30", textvariable=self.edtProjetoVar, state='readonly')
        self.edtProjeto.pack(side=TOP, fill=X, pady=5)

        self.cbbSistemaContrutivo = ttk.Combobox(frmEntries, state='readonly')
        self.cbbSistemaContrutivo["values"] = self.__getSistemaContrutivoNames()
        self.cbbSistemaContrutivo.current(0)
        self.cbbSistemaContrutivo.bind('<<ComboboxSelected>>', self.__onChangeSistema)
        self.cbbSistemaContrutivo.pack(side=TOP,fill=X, pady=5)

        self.edtQuantidadeVar = StringVar()
        self.edtQuantidade = Entry(frmEntries, width = "30", textvariable=self.edtQuantidadeVar, state='readonly')
        self.edtQuantidade.pack(side=TOP, fill=X, pady=5)
        self.edtQuantidadeVar.set(self.__projetoDto.qtdSistemaConstrutivo)


        self.frameTreeViewAtividades = Frame()
        self.frameTreeViewAtividades.pack(side=TOP, fill=X, pady=10, padx=10)

        self.frameAtividadesProjeto = FrameCardAtividadesProjeto(self.frameTreeViewAtividades, self.session, self.__cadProjetoDTO)
        self.frameAtividadesProjeto.setBeforeOpenEdicao(self.__onBeforeOpenCardEdicao)
        self.frameAtividadesProjeto.pack(side=LEFT)

        frameTreeviewAtividadeEquipamento = Frame()
        frameTreeviewAtividadeEquipamento.pack(side=TOP, fill=X, pady=10, padx=10)

        self.frmCardInsumoEquipamento = FrameCardInsumoProjetoMeioTransporteEquipamento(frameTreeviewAtividadeEquipamento, 
            self.session, self.__cadProjetoDTO)
        self.frmCardInsumoEquipamento.definirDescricao("Equipamento")
        self.frmCardInsumoEquipamento.setBeforeOpenEdicao(self.__onBeforeOpenCardEdicao)
        self.frmCardInsumoEquipamento.definirHeightTreeView(3)
        self.frmCardInsumoEquipamento.pack(side=LEFT)

        frameTreeviewAtividadeMaterial = Frame()
        frameTreeviewAtividadeMaterial.pack(side=TOP, fill=X, pady=10, padx=10)

        self.frmCardInsumoMaterial = FrameCardInsumoProjetoMeioTransporteMaterial(frameTreeviewAtividadeMaterial, 
            self.session, self.__cadProjetoDTO)
        self.frmCardInsumoMaterial.definirDescricao("Material")
        self.frmCardInsumoMaterial.setBeforeOpenEdicao(self.__onBeforeOpenCardEdicao)
        self.frmCardInsumoMaterial.definirHeightTreeView(3)
        self.frmCardInsumoMaterial.pack(side=LEFT)


        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10, command=self.__btnVoltarOnClick)
        self.btnVoltar.pack(side=LEFT, padx=15)

        self.btnMenu = Button(frmBotoes, text="Menu", width=20)
        self.btnMenu.pack(side=LEFT, padx=15)

        self.btnPriximaTela = Button(frmBotoes, text="Cadastrar", width=20, command=self.__cadastrarOnClick)
        self.btnPriximaTela.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)

        self.lbWarnings = Label(frmWarning, text="", height="2")
        self.lbWarnings.pack()


    def __proximaTelaOnClick(self):
        if not self.__validarCampos():
            return

        
        
        self.__proximaTela(self.__projetoDto)
    
    def __validarCampos(self):
        self.lbWarnings.configure(text = "")
        if self.__projetoDto == None:
           self.__projetoDto = Projeto()     

        return True
        

    def __btnVoltarOnClick(self):
        self.frameAtividadesProjeto.setListOnCadProjetoDTO()
        self.frmCardInsumoEquipamento.setListOnCadProjetoDTO()
        self.frmCardInsumoMaterial.setListOnCadProjetoDTO()
        
        self.__voltarTelaAnterior(self.__cadProjetoDTO)

    def definirVoltar(self, voltarTelaAnterior):
        self.__voltarTelaAnterior = voltarTelaAnterior

    def __carregarValorCampos(self):
        self.edtProjetoVar.set(self.__projetoDto.nome)
        self.__loadAtividadesSalubridade()
        self.__loadInsumosEquipamento()
        self.__loadInsumosMaterial()
    
    def definirProximaTela(self, proximaTela):
        self.__proximaTela = proximaTela
        
    def __getSistemaContrutivoNames(self):
        result = []
        for sistema in self.__cadProjetoDTO.projetoSistemaConstrutivoList:
            result.append(sistema.sistemaConstrutivo.nome)
        
        return result

    def __getIdSistemaConstrutivoSelected(self):
        sistemaName = self.cbbSistemaContrutivo.get()
        for item in self.__cadProjetoDTO.projetoSistemaConstrutivoList:
            if item.sistemaConstrutivo.nome == sistemaName:
                return item.sistemaConstrutivo.id

        print("Erro ao encontrar ID do sistema construtivo")
        return -1
        
    def __loadAtividadesSalubridade(self):
        self.frameAtividadesProjeto.setListOnCadProjetoDTO()
        self.frameAtividadesProjeto.loadAtividades(self.__getIdSistemaConstrutivoSelected())

    def __loadInsumosEquipamento(self):
        self.frmCardInsumoEquipamento.setListOnCadProjetoDTO()
        self.frmCardInsumoEquipamento.load(self.__getIdSistemaConstrutivoSelected())

    def __loadInsumosMaterial(self):
        self.frmCardInsumoMaterial.setListOnCadProjetoDTO()
        self.frmCardInsumoMaterial.load(self.__getIdSistemaConstrutivoSelected())

    def __onChangeSistema(self, event):
        self.__loadAtividadesSalubridade()
        self.__loadInsumosEquipamento()
        self.__loadInsumosMaterial()

    def __cadastrarOnClick(self):
        new = (self.__cadProjetoDTO.projeto.id == None)

        self.frameAtividadesProjeto.setListOnCadProjetoDTO()
        self.frmCardInsumoEquipamento.setListOnCadProjetoDTO()
        self.frmCardInsumoMaterial.setListOnCadProjetoDTO()

        projetoController = ProjetoController(self.session)
        projetoController.saveProjeto(self.__cadProjetoDTO)

        self.session.commit()

        if new:
            self.lbWarnings.config(text="Projeto cadastrado com sucesso!", foreground="black")
        else:
            self.lbWarnings.config(text="Projeto atualizado com sucesso!", foreground="black")

    def __onBeforeOpenCardEdicao(self):
        self.__closeEditControls()

    def __closeEditControls(self):
        self.frameAtividadesProjeto.recolherCamposEdicao()
        self.frmCardInsumoEquipamento.recolherCamposEdicao()
        self.frmCardInsumoMaterial.recolherCamposEdicao()

    def setOpenMenu(self, openMenu):
        self.btnMenu["command"] = openMenu











