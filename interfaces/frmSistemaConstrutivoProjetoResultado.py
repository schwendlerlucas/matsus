# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
from tkinter import ttk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.interfaceUtils import aplicarMascaraE, aplicarMascaraDec, aplicarMascaraRS
from datetime import datetime

from models.projeto import Projeto
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from models.sistemaConstrutivo import SistemaConstrutivo

from interfaces.frmCardResultadoProjeto import FrameCardResultadoProjeto

from DTOs.cadProjetoDto import CadProjetoDTO


from controllers.sistemaConstrutivoAtividadeController import SistemaConstrutivoAtividadeController
from controllers.projetoController import ProjetoController

from interfaces.constantesIndicadoresSistemaConstrutivo import *

from controllers.sistemaConstrutivoResultadoAmbientalController import SistemaConstrutivoResultadoAmbientalController
from controllers.sistemaConstrutivoResultadoController import SistemaConstrutivoResultadoController


class FrameSistemaConstrutivoProjetoResultado(FrameBase):

    def __init__(self, parent=None, session=None, cadProjetoDTO:CadProjetoDTO=None):
        super(FrameSistemaConstrutivoProjetoResultado, self).__init__(parent=parent, session=session)

        self.clearParent()

        self.__projetoDto = None
        self.__cadProjetoDTO = cadProjetoDTO
        if self.__cadProjetoDTO != None:
            self.__projetoDto = cadProjetoDTO.projeto

        self.__proximaTela = None
        self.__voltarTelaAnterior = None
        self.__openMenu = None
        self.__listaSistemas = []

        parent.title("Indicadores de Projeto")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1200x800+100+20")
        self.__createUI()

        self.__consultarSistemas()
        self.__configurarTreeView()
        self.__carregarValorCampos()
        self.__loadResult()



    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Indicadores de Projeto", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frameCampos = Frame()
        frameCampos.pack(side=TOP)

        frmLabels = Frame(frameCampos, width=1)
        frmLabels.pack(side=LEFT)
        labelProjeto = Label(frmLabels, text="Nome do Projeto ", anchor='ne')
        labelProjeto.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Quantidade total ", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Estado de referência para custo", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)


        frmEntries = Frame(frameCampos, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtProjetoVar = StringVar()
        self.edtProjeto = Entry(frmEntries, width = "30", textvariable=self.edtProjetoVar, state='readonly')
        self.edtProjeto.pack(side=TOP, fill=X, pady=5)

        self.edtQuantidadeVar = StringVar()
        self.edtQuantidade = Entry(frmEntries, width = "30", textvariable=self.edtQuantidadeVar, state='readonly')
        self.edtQuantidade.pack(side=TOP, fill=X, pady=5)
        if self.__projetoDto != None:
            self.edtQuantidadeVar.set(self.__projetoDto.qtdSistemaConstrutivo)

        self.edtLocalVar = StringVar()
        self.edtLocal = Entry(frmEntries, width = "30", textvariable=self.edtLocalVar, state='readonly')
        self.edtLocal.pack(side=TOP,fill=X, pady=5)
        if self.__projetoDto != None:
            self.edtLocalVar.set(self.__projetoDto.estado)

        self.frmTreeView = Frame()
        self.frmTreeView.pack(fill=BOTH, side=TOP, expand=1)

        frmTreeViewScrollBar = Frame(self.frmTreeView)
        frmTreeViewScrollBar.pack(side=TOP, fill=BOTH, expand=1, padx=5)

        self.treeView = ttk.Treeview(master=frmTreeViewScrollBar, height=4)

        self.scrollbarV = Scrollbar(master=frmTreeViewScrollBar)
        self.scrollbarV.pack(fill=Y, side=RIGHT, pady=15)
        self.scrollbarV.config(command = self.treeView.yview)

        self.treeView.configure(yscrollcommand=self.scrollbarV.set, height=15)
        self.treeView.pack(pady=15, fill=BOTH, expand=1)


        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10, command=self.__btnVoltarOnClick)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnPesquisar = Button(frmBotoes, text="Pesquisar", width=10)
        self.btnPesquisar.pack(side=LEFT, padx=15)
        self.btnOpenMenu = Button(frmBotoes, text="Menu", width=20, command=self.__menuOnClick)
        self.btnOpenMenu.pack(side=RIGHT, padx=15)

    def __menuOnClick(self):
        self.__openMenu()


    def __btnVoltarOnClick(self):
        self.__voltarTelaAnterior()

    def definirVoltar(self, voltarTelaAnterior):
        self.__voltarTelaAnterior = voltarTelaAnterior

    def __carregarValorCampos(self):
        if self.__cadProjetoDTO == None:
            return
        self.edtProjetoVar.set(self.__projetoDto.nome)

    def definirTelaMenu(self, openMenu):
        self.__openMenu = openMenu


    def __loadResult(self):
        for i in self.treeView.get_children():
            self.treeView.delete(i)

        if self.__cadProjetoDTO == None:
            return

        estado = self.__cadProjetoDTO.projeto.estado

        segundoNivel = "     "
        terceiroNivel = "          "

        self.treeView.insert('', END, text="AMBIENTAL",
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "Emissões previstas para a aplicação do Sistema Construtivo",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "GWP  -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].GWPTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "ODP  -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].ODPTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "AP  -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].APTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "EP  -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].EPTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "POCP  -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].POCPTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Residuos sólidos para a aplicação do sistema construtivo",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Quantidade de Residuos Perigosos -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].ResidPerigosoTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Quantidade de Residuos Raioativos -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].ResidRadioativoTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Quantidade de Residuos não perigosos -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].ResidNaoPerigosoTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Consumo de energia na aplicação",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Não Renovável -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].EnergiaNaoRenovavelTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Renonável -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].EnergiaRenovavelTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Consumo de água na aplicação",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Rede de Abastecimento -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].AguaRedeAbastesimentoTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Água de Reuso -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresE(lambda i : self.__listaSistemas[i].AguaReusoTotal)))


        self.treeView.insert('', END, text="Economico",
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "Custos Financeiros",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Custos para a produção e aplicação do sistema construtivo -Total ("+ estado +" + Transportes)",
                            values=(self.__retornarArrayValoresRS(lambda i : self.__listaSistemas[i].CustoTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Insentivo a economia local",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Aquisição de matéria-prima próxima ao local de sua aplicação",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].AquisiMateriaPrimaLocalTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Contratação de mão de obra local",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].MaoObraLocalTotal)))



        self.treeView.insert('', END, text="Social - avaliação para a aplicação do sistema construtivo",
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "Disseminação de conhecimento técnico acerca da sustentabilidade",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de popularização dos conceitos de sustentabilidade ao longo da cadeia produtiva do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauPopularizacaoConceitosTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de Fomento/apoio a instituições que promovem a sustentabilidade da construção civil",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauFomentoInstituicaoTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Respeito e afirmação histórico e cultural local",
                            values=(""))
        self.treeView.insert('', END, text=(terceiroNivel + "Grau de utilização de materiais culturalmente usados pela comunidade local"),
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauMatCulturalmenteUsadosTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Salubridade e seguridade social",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de salubridade nas condições de trabalho ao longo da cadeia produtiva do sistema construtivo (GS)",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauSalubridadeTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de utilização de mão de obra formal, com a garantia seguridades sociais ao longo da cadeia produtiva do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauSeguridadeTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Incorporação da técnica construtiva pela população local",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de contribuição do sistema construtivo na edificação",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauContribNaEdificacaoTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de complexidade produtiva do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauComplexidadeTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Possibilidade de produção do sistema construtivo por meio de mutirões",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].PossibilidadeMutiroesTotal)))


        self.treeView.insert('', END, text="Ténica",
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel + "Aptidão a reciclagem ou ao reuso",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Parcela que pode ser reciclada do sistema construtivo após o seu desuso",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].ParcelaPodeSerRecicladaTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Parcela que pode ser reaproveitada do sistema construtivo após o seu desuso",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].ParcelaPodeSerReaproveitadaTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Desempenho Físico-mecânico",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de estanqueidade do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauEstanqueidadeTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de transmitância térmica do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauTransmitanciaTermicaTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de transmissão de ondas sonoras do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauTransmissaoOndasTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Comportamento mecânico do sistema construtivo aos esforços de: compressão, tração e abrasão",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].ComportMecanicoTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Durabilidade prevista para o sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].DurabilidadeTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Confiabilidade técnica",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Acompanhamento de profissionais habilitados durante as fases de produção e aplicação do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].AcompanhamentoProfissionaisTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Facilidade de estocagem e transporte",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Grau de perecibilidade dos elementos que compõem o sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].GrauPericibilidadeTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Facilidade de estocagem dos elementos que compõem o sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].FacilidadeEstocagemTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Facilidade de transporte dos elementos que compõem o sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].FacilidadeTransporteTotal)))

        self.treeView.insert('', END, text=segundoNivel + "Facilidade de manutenções/ ampliações",
                            values=(""))
        self.treeView.insert('', END, text=terceiroNivel + "Facilidade da realização de manutenções periódicas no sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].FacilidadeManutencaoTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Facilidade de realizar reparos no sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].FacilidadeReparoTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Padronização na replicação do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].PadronizacaoReplicacaoTotal)))
        self.treeView.insert('', END, text=terceiroNivel + "Versatilidade da aplicação e uso do sistema construtivo",
                            values=(self.__retornarArrayValoresDec(lambda i : self.__listaSistemas[i].VersatilidadeTotal)))



    def __configurarTreeView(self):
        listaColunas =[]
        for i in range(0, len(self.__listaSistemas)):
            listaColunas.append('sistema' + str(i))

        self.treeView['columns'] = listaColunas
        self.treeView.heading("#0", text=' Resultados', anchor='w')
        self.treeView.column("#0", anchor='w', width=600)

        for i in range(0, len(self.__listaSistemas)):
            self.treeView.heading('sistema' + str(i), text=self.__listaSistemas[i].sistema_nome, anchor='ne')
            self.treeView.column('sistema' + str(i), anchor='ne', width=100)


    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick

    def __retornarArrayValoresE(self, getValor):
        result = []
        for i in range(0, len(self.__listaSistemas)):
            result.append(aplicarMascaraE(getValor(i)))

        return result

    def __retornarArrayValoresDec(self, getValor):
        result = []
        for i in range(0, len(self.__listaSistemas)):
            result.append(aplicarMascaraDec(getValor(i)))

        return result

    def __retornarArrayValoresRS(self, getValor):
        result = []
        for i in range(0, len(self.__listaSistemas)):
            result.append(aplicarMascaraRS(getValor(i)))

        return result

    def __consultarSistemas(self):
        if self.__projetoDto == None:
            return
        estado = self.__projetoDto.estado
        controllerAmbiental = SistemaConstrutivoResultadoAmbientalController(self.session)
        controller = SistemaConstrutivoResultadoController(self.session)

        self.__listaSistemas = []
        for sistema in self.__cadProjetoDTO.projetoSistemaConstrutivoList:
            resultados = controllerAmbiental.Calc(self.__cadProjetoDTO.projeto.id, sistema.sistemaConstrutivo_id, estado)
            controller.Calc(self.__cadProjetoDTO.projeto.id, sistema.sistemaConstrutivo_id, estado, resultados)

            self.__listaSistemas.append(resultados)
