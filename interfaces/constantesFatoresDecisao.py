
CONST_AI = "Emissões previstas para a aplicação do Sistema Construtivo (Quant/m²)"
CONST_AII = "Residuos sólidos para a aplicação do sistema construtivo (Quant/m²)"
CONST_AIII = "Consumo de energia na aplicação"
CONST_AIV = "Consumo de água na aplicação"

CONST_SI = "Disseminação de conhecimento técnico acerca da sustentabilidade"
CONST_SII = "Respeito e afirmação histórico e cultural local"
CONST_SIII = "Salubridade e seguridade social"
CONST_SIV = "Incorporação da técnica construtiva pela população local"

CONST_EI = "Custos Financeiros"
CONST_EII = "Insentivo a economia local"

CONST_TI = "Aptidão a reciclagem ou ao reuso"
CONST_TII = "Desempenho Físico-mecânico"
CONST_TIII = "Confiabilidade técnica"
CONST_TIV = "Facilidade de estocagem e transporte"
CONST_TV = "Facilidade de manutenções/ ampliações"

ARRAY_FATORES_DECISAO = [
    CONST_AI,
    CONST_AII,
    CONST_AIII,
    CONST_AIV,

    CONST_SI,
    CONST_SII,
    CONST_SIII,
    CONST_SIV,

    CONST_EI,
    CONST_EII,

    CONST_TI,
    CONST_TII,
    CONST_TIII,
    CONST_TIV,
    CONST_TV
   ] 