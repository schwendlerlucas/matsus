# Janela de login e senha
# Chamada da biblioteca tkinter
from tkinter import *
from interfaces.frmBase import FrameBase
from models.user import User



#Definição de classe

class FrameLogin(FrameBase):
    
    def __init__(self, parent=None, session=None):
        super(FrameLogin, self).__init__(parent=parent, session=session)

        self.__abrirMenu = None
        self.clearParent()

        #Geometria da Janela
        
        parent.title("Login")
        parent.geometry ("750x600+100+100")

        #Elementos da Janela -> containers na Janela
        ###############################################
        #Container 1 - Logo
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.label = Label(image = self.photo)
        self.label.grid(row=0, column=0, columnspan=4, sticky=E)
        ###############################################


        ##############################################
        #Container 2 - Usuários
        #Label Usuário
        #self.UsuarioLabel = Frame(master)
        self.UsuarioLabel = Label(text="Usuário ", width=15, height="2")
        self.UsuarioLabel.grid(row=1, column=0, sticky=E)


        #Entray Usuário
        #self.UsuarioEntray = Frame(master)
        self.UsuarioEntray = Entry(width = "58")
        self.UsuarioEntray.grid(row=1, column=2, columnspan=2, sticky=E)
        ###############################################


        ###############################################
        #Senha -> Container 3
        #label Senha
        #self.SenhaLabel = Frame(master)
        self.SenhaLabel = Label(text="Senha ", width=15, height="2")
        self.SenhaLabel.grid(row=2, column=0, sticky=E)

        #Entray Senha
        #self.SenhaEntray = Frame(master)
        self.SenhaEntray = Entry(width = "58")
        self.SenhaEntray.grid(row=2, column=2, columnspan=2)
        self.SenhaEntray["show"] = "*"
        ###############################################


        ###############################################
        #Confirmar -> Container 4
        #Botão
        #self.botao = Frame(master)
        self.botao = Button(text="Confirmar", padx=15, width=15)
        self.botao.grid(row=4, column=2)
        self.botao["command"] = self.__verificaSenha


        #Texto confirmação
        self.mensagem = Label(text="", fg="red")
        self.mensagem.grid(row=3, column=2, pady=5, sticky=W)
        ###############################################


        ###############################################
        #Confirmar -> Container 5
        #Botão
        self.btnCadUsuario = Button(text="Cadastrar Novo Usuário", padx=15, width=15)
        self.btnCadUsuario.grid(row=4, column=3)
        #self.btnCadUsuario["command"] = self.openCadUsuario
        ###############################################

    def definirOpenCadUsuario(self, onClickCadUsuario):
        self.btnCadUsuario["command"] = onClickCadUsuario

    def definirAbrirMenu(self, abrirMenu):
        self.__abrirMenu = abrirMenu 


     #Método verificar senha
    def __verificaSenha(self):
        usuario = self.UsuarioEntray.get()
        senha = self.SenhaEntray.get()
        user = self.session.query(User).filter_by(emailUsuario=usuario, senha=senha).first()
        if user != None:
            self.mensagem["text"] = "Autenticado"
            self.__abrirMenu()
        else:
            self.mensagem["text"] = "Erro na autenticação!"

