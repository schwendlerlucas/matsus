
from tkinter import Frame

class FrameBase(Frame):


    def __init__(self, parent=None, session=None):
        super(FrameBase, self).__init__(master=parent)

        self.parent = parent
        self.session = session


    def clearParent(self):
        for width in self.parent.winfo_children():
            width.destroy()
