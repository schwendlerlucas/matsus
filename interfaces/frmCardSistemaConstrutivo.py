from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.constantesViews import *
from interfaces.frmCardItensBase import FrameCardItensBase
from interfaces.validacoesCampos import *

class FrameCardSistemaConstrutivo(FrameCardItensBase):

    def __init__(self, parent, session, model, modelAtividade):
        super(FrameCardSistemaConstrutivo, self).__init__(parent, session=session, model=model, modelAtividade=modelAtividade)


    def CreateUI(self):
        super(FrameCardSistemaConstrutivo, self).CreateUI()
        self.btnEditar.pack_forget()

        self.treeView['columns'] = ('descicao')
        self.treeView.heading("#0", text='Código', anchor='w')
        self.treeView.column("#0", anchor='n', width=100)
        self.treeView.heading('descicao', text='Sistemas construtivos')
        self.treeView.column('descicao', width=400)


        self.cbbItemClasse = ttk.Combobox(master=self.frameEdicao, state='readonly', width=34) 
        self.cbbItemClasse.pack(side=LEFT, padx=5)
        self.cbbItemClasse.bind('<<ComboboxSelected>>', self.onChangeItemClasse)

        self.btnIncluir = Button(master=self.frameEdicao, text="ok", command=self.__btnOkEdicaoClick, width=2)
        self.btnIncluir.pack(side=LEFT, padx=5, pady=5)

    def __btnOkEdicaoClick(self):
        super(FrameCardSistemaConstrutivo, self).btnOkEdicaoClick()


    def LoadTable(self):
        super(FrameCardSistemaConstrutivo, self).LoadTable()

        for item in self.listaItens:
            nome = item.pegarObjetoFk().nome
            self.treeView.insert('', END, text=item.pegarObjetoFk().id, 
                                values=(str(nome), nome))


    def updateItem(self, item):
        super(FrameCardSistemaConstrutivo, self).updateItem(item)
            

    
    def updateComponents(self, item):
        super(FrameCardSistemaConstrutivo, self).updateComponents(item)

    def limparCamposEdicao(self):
        super(FrameCardSistemaConstrutivo, self).limparCamposEdicao()
    def validarCampos(self):
        super(FrameCardSistemaConstrutivo, self).limparCamposEdicao()
        if self.cbbItemClasse.get() == "":
            return False
        
        return True    

    def definirIdPaiNoItem(self, item, id):
        super(FrameCardSistemaConstrutivo, self).definirIdPaiNoItem(item, id)

        item.projeto_id = id
        
    def consultar(self, id):
        super(FrameCardSistemaConstrutivo, self).consultar(id)

        self.listaItens = self.session.query(self.modelAtividade).\
            filter(self.modelAtividade.sistemaConstrutivo_id == id).all()  
        
        self.LoadTable() 

    def salvar(self, idPai):
        #to don't invoke the parent's salvar method
        return True

    def getSistemaContrutivoList(self):

        if self.listaItens == None:
            return []

        return self.listaItens

    def loadSistemas(self, listSistemas):
        self.listaItens = listSistemas
        self.LoadTable()


    

    
    
    