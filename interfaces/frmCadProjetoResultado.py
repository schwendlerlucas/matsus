# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
from tkinter import ttk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime

from models.projeto import Projeto
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from models.sistemaConstrutivo import SistemaConstrutivo

from interfaces.frmCardResultadoProjeto import FrameCardResultadoProjeto

from DTOs.cadProjetoDto import CadProjetoDTO


from controllers.sistemaConstrutivoAtividadeController import SistemaConstrutivoAtividadeController
from controllers.projetoController import ProjetoController




class FrameCadProjetoResultado(FrameBase):
    
    def __init__(self, parent=None, session=None, cadProjetoDTO:CadProjetoDTO=None):
        super(FrameCadProjetoResultado, self).__init__(parent=parent, session=session)

        self.clearParent()
       
        self.__projetoDto = None
        self.__cadProjetoDTO = cadProjetoDTO
        if self.__cadProjetoDTO != None:
            self.__projetoDto = cadProjetoDTO.projeto

        self.__proximaTela = None
        self.__voltarTelaAnterior = None
        
        parent.title("Análise de transportes")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1200x650+100+100")
        self.__createUI()

        self.__carregarValorCampos()
        self.__loadResult()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=30)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Análise de transportes", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)

        frameCampos = Frame()
        frameCampos.pack(side=TOP)

        frmLabels = Frame(frameCampos, width=1)
        frmLabels.pack(side=LEFT)
        labelProjeto = Label(frmLabels, text="Nome do Projeto ", anchor='ne')
        labelProjeto.pack(side=TOP, fill=X, pady=4)
        labelSistema = Label(frmLabels, text="Escolha o sistema ", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=6)
        labelSistema = Label(frmLabels, text="Quantidade total ", anchor='ne')
        labelSistema.pack(side=TOP, fill=X, pady=4)


        frmEntries = Frame(frameCampos, width=30)
        frmEntries.pack(side=RIGHT)
        self.edtProjetoVar = StringVar()
        self.edtProjeto = Entry(frmEntries, width = "30", textvariable=self.edtProjetoVar, state='readonly')
        self.edtProjeto.pack(side=TOP, fill=X, pady=5)

        self.cbbSistemaContrutivo = ttk.Combobox(frmEntries, state='readonly')
        self.cbbSistemaContrutivo["values"] = self.__getSistemaContrutivoNames()
        self.cbbSistemaContrutivo.current(0)
        self.cbbSistemaContrutivo.bind('<<ComboboxSelected>>', self.__onChangeSistema)
        self.cbbSistemaContrutivo.pack(side=TOP,fill=X, pady=5)

        self.edtQuantidadeVar = StringVar()
        self.edtQuantidade = Entry(frmEntries, width = "30", textvariable=self.edtQuantidadeVar, state='readonly')
        self.edtQuantidade.pack(side=TOP, fill=X, pady=5)
        self.edtQuantidadeVar.set(self.__projetoDto.qtdSistemaConstrutivo)


        frameTreeViewResultado = Frame()
        frameTreeViewResultado.pack(side=TOP, fill=X, pady=10, padx=10)

        self.frameResultado = FrameCardResultadoProjeto(frameTreeViewResultado, self.session)
        self.frameResultado.pack(fill=BOTH)
        
        
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=25)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10, command=self.__btnVoltarOnClick)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnOpenMenu = Button(frmBotoes, text="Menu", width=20, command=self.__menuOnClick)
        self.btnOpenMenu.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)


    def __menuOnClick(self):
        self.__openMenu()
            

    def __btnVoltarOnClick(self):        
        self.__voltarTelaAnterior(self.__cadProjetoDTO)

    def definirVoltar(self, voltarTelaAnterior):
        self.__voltarTelaAnterior = voltarTelaAnterior

    def __carregarValorCampos(self):
        self.edtProjetoVar.set(self.__projetoDto.nome)

    def definirTelaMenu(self, openMenu):
        self.__openMenu = openMenu
        
    def __getSistemaContrutivoNames(self):
        result = []
        for sistema in self.__cadProjetoDTO.projetoSistemaConstrutivoList:
            result.append(sistema.sistemaConstrutivo.nome)
        
        return result

    def __getIdSistemaConstrutivoSelected(self):
        sistemaName = self.cbbSistemaContrutivo.get()
        for item in self.__cadProjetoDTO.projetoSistemaConstrutivoList:
            if item.sistemaConstrutivo.nome == sistemaName:
                return item.sistemaConstrutivo.id

        print("Erro ao encontrar ID do sistema construtivo")
        return -1

    def __onChangeSistema(self, event):
        self.__loadResult()
        
    def __loadResult(self):
        self.frameResultado.carregarAtividade(self.__cadProjetoDTO.projeto.id, 
            self.__getIdSistemaConstrutivoSelected(), float(self.__projetoDto.qtdSistemaConstrutivo))
    







