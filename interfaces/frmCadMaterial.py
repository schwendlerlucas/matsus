# Cadastro Materiais 4.1
from tkinter import *
from tkinter import ttk
from interfaces.frmBase import FrameBase
from interfaces.validacoesCampos import validarData
from models.material import Material 
from interfaces.constantesViews import *

class FrameCadMaterial(FrameBase):
    
    def __init__(self, parent=None, session=None, material=None):
        super(FrameCadMaterial, self).__init__(parent=parent, session=session)

        self.clearParent()        
        self.__openProximaTela = None
        self.__voltarCadastroMaterial = None        
        self.__materialDto = material

        #Geometria da Janela
        parent.title("Cadastro de materiais")
        parent.geometry ("880x590+100+100")

        #Elementos da Janela -> containers na Janela
        ###############################################
        #Container 1 - Logo
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        self.label = Label(image = self.photo)
        ###############################################
        ###############################################
        #Container 2 - Textos
        self.lb1 = Label(text="Cadastro de materiais", font=14, height="3")
        self.lb2 = Label(text="")
        self.lb3 = Label(text="Nome do material", height="2", padx=4)
        self.lb4 = Label(text="Tipo", height="2", padx=4)

        self.lb5 = Label(text="Fornecedor/ Fabricante", height="2", padx=4)
        self.lb6 = Label(text="Informações adicionais", height="2", padx=4)

        self.lbFonteEPD = Label(text="Fonte EPD", height="2", padx=4)
        self.lb8 = Label(text="Código da EPD", height="2", padx=4)
        self.lbPaisEpd = Label(text="País EPD", height="2", padx=4)
        self.lb9 = Label(text="Data da publicação", height="2", padx=4)
        self.lb10 = Label(text="")
        ###############################################
        ###############################################
        #Container 3 - Entray
        self.nomeMatVar = StringVar()
        self.Nomemat = Entry(width = "58", textvariable=self.nomeMatVar)
        self.FornecedorVar = StringVar()
        self.Fornecedor = Entry(width = "58", textvariable=self.FornecedorVar)
        self.InfaddVar = StringVar()
        self.Infadd = Entry(width = "58", textvariable=self.InfaddVar)
        self.edtFonteEPDVar = StringVar()
        self.edtFonteEPD = Entry(width = "58", textvariable=self.edtFonteEPDVar)
        self.CodEPDVar = StringVar()
        self.CodEPD = Entry(width = "58", textvariable=self.CodEPDVar)
        self.edtPaisVar = StringVar()
        self.edtPais = Entry(width = "58", textvariable=self.edtPaisVar)
        self.DataEPDVar = StringVar()
        self.DataEPD = Entry(width = "58", textvariable=self.DataEPDVar)
        ###############################################
        ###############################################
        #Container 3 - botoes
        self.btnPesquisar = Button(text="Pesquisar", width=10)
        self.btnVoltar = Button(text="Voltar", width=10)
        self.btnProximaTela = Button(text="Seguinte", width=20, command=self.__btnProximaTelaClick)
        ###############################################
        ###############################################
        #Container 4 - Caixa de seleção
        self.cbPossuiDeclaracaoVar = IntVar()
        self.cbPossuiDeclaracao = Checkbutton(text="Possui uma declaração ambiental do produto cadastrada (EPD)", height=3, variable=self.cbPossuiDeclaracaoVar)
        ###############################################
        ###############################################
        #Container 5 - Caixa suspensa
        itens1 = ["aglomerante", "agregado", "cerâmico","composito", "metalico", "madeira", "polimero", "aditivo quimico", "fibra vegetal", "outro"]

        self.Tipo = ttk.Combobox()
        self.Tipo["values"] = itens1
        self.Tipo.current(0)
        ###############################################
        # Posição dos Elementos
        #Logo
        self.label.grid(row=0, column=0, rowspan=3, sticky=S)
        #Strings
        self.lb1.grid(row=0, column=1, sticky=W, columnspan=2)
        self.lb2.grid(row=1, column=1, sticky=E)
        self.lb3.grid(row=2, column=1, sticky=E)
        self.lb4.grid(row=3, column=1, sticky=E)
        self.lb5.grid(row=4, column=1, sticky=E)
        self.lb6.grid(row=5, column=1, sticky=E)

        self.lbFonteEPD.grid(row=7, column=1, sticky=E)
        self.lb8.grid(row=8, column=1, sticky=E)
        self.lbPaisEpd.grid(row=9, column=1, sticky=E)
        self.lb9.grid(row=10, column=1, sticky=E)
        self.lb10.grid(row=11, column=1, sticky=E)
        #Entray
        self.Nomemat.grid(row=2, column=2, sticky=W)
        self.Fornecedor.grid(row=4, column=2, sticky=W)
        self.Infadd.grid(row=5, column=2, sticky=W)
        self.edtFonteEPD.grid(row=7, column=2, sticky=W)
        self.CodEPD.grid(row=8, column=2, sticky=W)
        self.edtPais.grid(row=9, column=2, sticky=W)
        self.DataEPD.grid(row=10, column=2, sticky=W)
        #Botões
        self.btnPesquisar.grid(row=13, column=1)
        self.btnVoltar.grid(row=13, column=0)
        self.btnProximaTela.grid(row=13, column=2)

        self.Tipo.grid(row=3, column=2, sticky=W)
        self.cbPossuiDeclaracao.grid(row=6, column=1, columnspan=2, sticky=W)

        self.lbWarnings = Label(text="", height="2")
        self.lbWarnings.grid(row=12, column=2)

        if self.__materialDto != None:
            self.__carregarValoresCampos()


    def __btnProximaTelaClick(self):
        self.lbWarnings.configure(text = "")
        if self.__materialDto == None:
           self.__materialDto = Material()     

        nomeMaterial = self.Nomemat.get()
        if (nomeMaterial == ''):
            self.lbWarnings.configure(text = "O 'nome material' é obrigatório", foreground="red")
            return False

        material = self.session.query(Material).filter_by(nome=nomeMaterial).first()
        if (material != None and material.id != self.__materialDto.id):
            self.lbWarnings.configure(text = "Material já cadastrada", foreground="red")
            return False 

        if (not validarData(self.DataEPD.get()) and self.cbPossuiDeclaracaoVar.get()):
            self.lbWarnings.configure(text = "Data EPD inválida", foreground="red")
            return False             

        self.__materialDto.nome = nomeMaterial
        self.__materialDto.tipo = self.Tipo.get()
        self.__materialDto.fonecedorFabricante = self.Fornecedor.get()
        self.__materialDto.informacoes = self.Infadd.get()
        self.__materialDto.possuiDeclaracao = self.cbPossuiDeclaracaoVar.get()
        self.__materialDto.fonteEPD = self.edtFonteEPD.get()
        self.__materialDto.cdEPD = self.CodEPD.get()
        self.__materialDto.paisEPD = self.edtPais.get()
        if self.cbPossuiDeclaracaoVar.get():
            self.__materialDto.dtPublicacao = self.DataEPD.get()
        else:
            self.__materialDto.dtPublicacao = "" 
        
        self.__openProximaTela(self.__materialDto)

    def __carregarValoresCampos(self):
        self.nomeMatVar.set(self.__materialDto.nome)
        self.FornecedorVar.set(self.__materialDto.fonecedorFabricante)
        self.InfaddVar.set(self.__materialDto.informacoes)
        self.cbPossuiDeclaracaoVar.set(self.__materialDto.possuiDeclaracao)
        self.edtFonteEPDVar.set(self.__materialDto.fonteEPD)
        self.CodEPDVar.set(self.__materialDto.cdEPD)
        self.edtPaisVar.set(self.__materialDto.paisEPD)
        self.DataEPDVar.set(self.__materialDto.dtPublicacao)
        self.Tipo.set(self.__materialDto.tipo)


    def definirTelaSeguinteCadMaterial(self, onClickProximaTela):
        self.__openProximaTela = onClickProximaTela 

    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick

    def definirPesquisar(self, onClick):
        self.btnPesquisar["command"] = onClick



