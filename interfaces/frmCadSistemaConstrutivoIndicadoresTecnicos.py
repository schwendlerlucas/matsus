# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from interfaces.constantesIndicadoresSistemaConstrutivo import *

from datetime import datetime


from models.atividade import Atividade
from models.sistemaConstrutivo import SistemaConstrutivo



class FrameCadSistemaConstrutivoIndicadoresTecnicos(FrameBase):
    
    def __init__(self, parent=None, session=None, sistemaConstrutivo=None):
        super(FrameCadSistemaConstrutivoIndicadoresTecnicos, self).__init__(parent=parent, session=session)

        self.clearParent()
        self.__sistemaConstrutivoDto = sistemaConstrutivo
        self.__openProximaTela = None
        self.__lastItemSelected = None

        
        parent.title("Cadastro de Sistema Construtivo")
        #cadastro.attributes('-zoomed',True)
        parent.geometry ("1000x730+100+100")
        self.__createUI()

        self.__carregarValorCampos()


    def __createUI(self):
        self.frameTop = Frame()
        self.frameTop.pack(side=TOP, fill=X)

        
        #frameImage = Frame(master=self.frameTop)
        #frameImage.pack(side=)
        self.photo = PhotoImage(file = "./interfaces/imagens/Logo.png")
        self.photo = self.photo.subsample(3, 3)
        labelPhoto = Label(self.frameTop, image = self.photo)
        labelPhoto.pack(side=LEFT)

        frameCabecalho = Frame(master=self.frameTop)
        frameCabecalho.pack(side=LEFT)

        frameEspacoIncialCabecalho = Frame(master=frameCabecalho)
        frameEspacoIncialCabecalho.pack(side=TOP, pady=20)

        frameTitulo = Frame(master=frameCabecalho)
        frameTitulo.pack(side=TOP, fill=X)

        labelTitulo = Label(frameTitulo, text="Cadastro de Sistema Construtivo", font=14, height="3", justify=LEFT)
        labelTitulo.pack(side=LEFT, fill=X, padx=30)
        
        frmCampo = Frame(frameCabecalho)
        frmCampo.pack(side=RIGHT)

        frmEntries = Frame(frmCampo, width=30)
        frmEntries.pack(side=RIGHT)
        self.lblSistemaContrutivo = Label(frmEntries, text="Sistema construtivo", font=14, height="2")
        self.lblSistemaContrutivo.pack(side=RIGHT, fill=X, pady=5)

        self.frmTreeView = Frame()
        self.frmTreeView.pack(fill=BOTH, side=TOP, expand=1)

        frmTreeViewScrollBar = Frame(self.frmTreeView)
        frmTreeViewScrollBar.pack(side=TOP, fill=BOTH, expand=1, padx=5)

        self.treeView = ttk.Treeview(master=frmTreeViewScrollBar, height=4)
        self.treeView.bind('<Double-Button>', self.__onDoubleClick)

        self.scrollbarV = Scrollbar(master=frmTreeViewScrollBar)
        self.scrollbarV.pack(fill=Y, side=RIGHT, pady=15)
        self.scrollbarV.config(command = self.treeView.yview)

        self.treeView.configure(yscrollcommand=self.scrollbarV.set, height=15)
        self.treeView.pack(pady=15, fill=BOTH, expand=1)

        frameEdicao = Frame(self.frmTreeView)
        frameEdicao.pack(side=BOTTOM, fill=X)

        btnOkValor = Button(frameEdicao, width=3, text="ok", command=self.__btnOkOnClick)
        btnOkValor.pack(side=RIGHT, padx=5)

        self.edtValorVar = StringVar() 
        self.edtValor = Entry(frameEdicao, width=10, justify=RIGHT, textvariable=self.edtValorVar)
        self.edtValor.pack(side=RIGHT, padx=5)

        self.lblDescicao = Label(frameEdicao)
        self.lblDescicao.pack(side=RIGHT, padx=10)
        
        frmBotoes = Frame()
        frmBotoes.pack(side=BOTTOM, fill=X, pady=15)

        self.btnVoltar = Button(frmBotoes, text="Voltar", width=10)
        self.btnVoltar.pack(side=LEFT, padx=15)
        self.btnProximo = Button(frmBotoes, text="Seguinte", width=20, command=self.__telaSeguinteOnClick)       
        self.btnProximo.pack(side=RIGHT, padx=15)

        frmWarning = Frame()
        frmWarning.pack(side=BOTTOM, fill=X)

        self.lbWarnings = Label(frmWarning, text="", height="2")
        self.lbWarnings.pack()

        self.__configurarTreeView()      


    def __configurarTreeView(self):
        self.treeView['columns'] = ('valor')
        self.treeView.heading("#0", text=' Indicadores Técnicos', anchor='w')
        self.treeView.column("#0", anchor='w', width=800)
        self.treeView.heading('valor', text=' Valor ', anchor='ne')
        self.treeView.column('valor', anchor='ne', width=100)

        segundoNivel = "     "

        self.treeView.insert('', END, text="Aptidão a reciclagem ou ao reuso", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel +TEC_PARCELA_PODE_SER_RECICLADA, 
                            values=(self.__pegarValorPelaDescicao(TEC_PARCELA_PODE_SER_RECICLADA)))
        self.treeView.insert('', END, text=segundoNivel +TEC_PARCELA_PODE_SER_REAPROVEITADA, 
                            values=(self.__pegarValorPelaDescicao(TEC_PARCELA_PODE_SER_REAPROVEITADA)))
        
        self.treeView.insert('', END, text="Desempenho Físico-mecânico", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel +TEC_GRAU_ESTANQUEIDADE, 
                            values=(self.__pegarValorPelaDescicao(TEC_GRAU_ESTANQUEIDADE)))
        self.treeView.insert('', END, text=segundoNivel +TEC_GRAU_TRANSMITANCIA_TERMICA, 
                            values=(self.__pegarValorPelaDescicao(TEC_GRAU_TRANSMITANCIA_TERMICA)))
        self.treeView.insert('', END, text=segundoNivel +TEC_GRAU_TRANSMITANCIA_SONORA, 
                            values=(self.__pegarValorPelaDescicao(TEC_GRAU_TRANSMITANCIA_SONORA)))
        self.treeView.insert('', END, text=segundoNivel +TEC_COMPORTAMENTO_MECANICO,
                            values=(self.__pegarValorPelaDescicao(TEC_COMPORTAMENTO_MECANICO)))
        self.treeView.insert('', END, text=segundoNivel +TEC_DURABILIDADE, 
                            values=(self.__pegarValorPelaDescicao(TEC_DURABILIDADE)))

        self.treeView.insert('', END, text="Confiabilidade técnica", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel +TEC_ACOMPANHAMENTO_TECNICO,
                            values=(self.__pegarValorPelaDescicao(TEC_ACOMPANHAMENTO_TECNICO)))

        self.treeView.insert('', END, text="Facilidade de estocagem e transporte", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel +TEC_GRAU_PERECIBILIDADE,
                            values=(self.__pegarValorPelaDescicao(TEC_GRAU_PERECIBILIDADE)))
        self.treeView.insert('', END, text=segundoNivel +TEC_FACILIDADE_ESTOCAGEM,
                            values=(self.__pegarValorPelaDescicao(TEC_FACILIDADE_ESTOCAGEM)))
        self.treeView.insert('', END, text=segundoNivel +TEC_FACILIDADE_TRANSPORTE, 
                            values=(self.__pegarValorPelaDescicao(TEC_FACILIDADE_TRANSPORTE)))

        self.treeView.insert('', END, text="Facilidade de manutenções/ ampliações", 
                            values=(""))
        self.treeView.insert('', END, text=segundoNivel +TEC_FACILIDADE_MANUTENCAO_PERIODICAS,
                            values=(self.__pegarValorPelaDescicao(TEC_FACILIDADE_MANUTENCAO_PERIODICAS)))
        self.treeView.insert('', END, text=segundoNivel +TEC_FACILIDADE_REPAROS,
                            values=(self.__pegarValorPelaDescicao(TEC_FACILIDADE_REPAROS)))
        self.treeView.insert('', END, text=segundoNivel +TEC_PADRONIZACAO_REPLICACAO,
                            values=(self.__pegarValorPelaDescicao(TEC_PADRONIZACAO_REPLICACAO)))
        self.treeView.insert('', END, text=segundoNivel +TEC_VERSATILIDADE,
                            values=(self.__pegarValorPelaDescicao(TEC_VERSATILIDADE)))



    def __telaSeguinteOnClick(self):
        if not self.__validarCampos():
            return

        self.__definirValoresNoDto()

        self.__openProximaTela(self.__sistemaConstrutivoDto)    
         
    def __definirValoresNoDto(self):
        for child in self.treeView.get_children():
            valor = 0
            if len(self.treeView.item(child)["values"]) == 1:
                valor = retornarZeroSeVazio(self.treeView.item(child)["values"][0])

            self.__definirValorNoDto(self.treeView.item(child)["text"], valor)
    
    def __validarCampos(self):
        return True
        
    def definirVoltarMenu(self, onClick):
        self.btnVoltar["command"] = onClick
    

    def definirTelaSeguinteCadSistemaConstrutivo(self, onClickProximaTela):
        self.__openProximaTela = onClickProximaTela

    def __definirValorNoDto(self, descricao: str, value: float):
        if self.__lastItemSelected == None:
            return False

        if not self.__lastItemSelected.strip() in ARRAY_INDICADORES_SISTEMA_CONS:
            return False

        _descricao = descricao.strip()

        if (_descricao == TEC_PARCELA_PODE_SER_RECICLADA):
            self.__sistemaConstrutivoDto.tecParcelaPodeReclidada = value
        elif (_descricao == TEC_PARCELA_PODE_SER_REAPROVEITADA):
            self.__sistemaConstrutivoDto.tecParcelaPodeReaproveitada = value
        elif (_descricao == TEC_GRAU_ESTANQUEIDADE):
            self.__sistemaConstrutivoDto.tecGrauEstanqueidade = value
        elif (_descricao == TEC_GRAU_TRANSMITANCIA_TERMICA):
            self.__sistemaConstrutivoDto.tecGrauTransmitanciaTermica = value
        elif (_descricao == TEC_GRAU_TRANSMITANCIA_SONORA):
            self.__sistemaConstrutivoDto.tecGrauTransmitanciaSonora = value
        elif (_descricao == TEC_COMPORTAMENTO_MECANICO):
            self.__sistemaConstrutivoDto.tecComportamentoMecanico = value
        elif (_descricao == TEC_DURABILIDADE):
            self.__sistemaConstrutivoDto.tecDurabilidade = value
        elif (_descricao == TEC_ACOMPANHAMENTO_TECNICO):
            self.__sistemaConstrutivoDto.tecAcompanhamentoTecnico = value
        elif (_descricao == TEC_GRAU_PERECIBILIDADE):
            self.__sistemaConstrutivoDto.tecGrauPerecibilidade = value
        elif (_descricao == TEC_FACILIDADE_ESTOCAGEM):
            self.__sistemaConstrutivoDto.tecFacilidadeEstocagem = value
        elif (_descricao == TEC_FACILIDADE_TRANSPORTE):
            self.__sistemaConstrutivoDto.tecFacilidadeTransporte = value
        elif (_descricao == TEC_FACILIDADE_MANUTENCAO_PERIODICAS):
            self.__sistemaConstrutivoDto.tecFacilidadeManutPeriodica = value
        elif (_descricao == TEC_FACILIDADE_REPAROS):
            self.__sistemaConstrutivoDto.tecFacilidadeReparos = value
        elif (_descricao == TEC_PADRONIZACAO_REPLICACAO):
            self.__sistemaConstrutivoDto.tecPadrinizacaoReplicacao = value
        elif (_descricao == TEC_VERSATILIDADE):
            self.__sistemaConstrutivoDto.tecVersatilidade = value
        else:
            print("Erro:  não encontrou")

    def __pegarValorPelaDescicao(self, descricao: str):
        if self.__sistemaConstrutivoDto == None:
            return "0.0"
    
        _descricao = descricao.strip()

        if (_descricao == TEC_PARCELA_PODE_SER_RECICLADA):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecParcelaPodeReclidada)
        if (_descricao == TEC_PARCELA_PODE_SER_REAPROVEITADA):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecParcelaPodeReaproveitada)
        if (_descricao == TEC_GRAU_ESTANQUEIDADE):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecGrauEstanqueidade)
        if (_descricao == TEC_GRAU_TRANSMITANCIA_TERMICA):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecGrauTransmitanciaTermica)
        if (_descricao == TEC_GRAU_TRANSMITANCIA_SONORA):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecGrauTransmitanciaSonora)
        if (_descricao == TEC_COMPORTAMENTO_MECANICO):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecComportamentoMecanico)
        if (_descricao == TEC_DURABILIDADE):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecDurabilidade)
        if (_descricao == TEC_ACOMPANHAMENTO_TECNICO):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecAcompanhamentoTecnico)
        if (_descricao == TEC_GRAU_PERECIBILIDADE):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecGrauPerecibilidade)
        if (_descricao == TEC_FACILIDADE_ESTOCAGEM):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecFacilidadeEstocagem)
        if (_descricao == TEC_FACILIDADE_TRANSPORTE):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecFacilidadeTransporte)
        if (_descricao == TEC_FACILIDADE_MANUTENCAO_PERIODICAS):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecFacilidadeManutPeriodica)
        if (_descricao == TEC_FACILIDADE_REPAROS):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecFacilidadeReparos)
        if (_descricao == TEC_PADRONIZACAO_REPLICACAO):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecPadrinizacaoReplicacao)
        if (_descricao == TEC_VERSATILIDADE):
            return retornarZeroSeNoneOuVazio(self.__sistemaConstrutivoDto.tecVersatilidade)


    def __onDoubleClick(self, event):
        self.edtValorVar.set("")
        self.lblDescicao.config(text="")
        itemSelected = self.treeView.item(self.treeView.focus())
        self.__lastItemSelected = itemSelected["text"]

        if not  self.__lastItemSelected.strip() in ARRAY_INDICADORES_SISTEMA_CONS:
            return False

        self.lblDescicao.config(text=itemSelected["text"].strip())
        self.edtValorVar.set("")
        self.edtValorVar.set(itemSelected["values"])

    def __btnOkOnClick(self):
        if not validarDoubleOuVazio(self.edtValor.get()):
            return False

        if (self.__lastItemSelected == None):
            return 
        
        if not  self.__lastItemSelected.strip() in ARRAY_INDICADORES_SISTEMA_CONS:
            return False
   
        for child in self.treeView.get_children():
            if self.treeView.item(child)["text"] == self.__lastItemSelected:
                self.treeView.item(child, text=self.__lastItemSelected, values=(self.edtValor.get()))
        
    def __carregarValorCampos(self):
        if self.__sistemaConstrutivoDto == None:
            return

        self.lblSistemaContrutivo.config(text=self.__sistemaConstrutivoDto.nome)

        if self.__sistemaConstrutivoDto.id == None:
            return

        

        
        

        
        


        





