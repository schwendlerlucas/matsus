# Cadastro Materiais 4.2 - Dimensões Ambiental
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.frmBase import FrameBase

from interfaces.constantesViews import *
from interfaces.validacoesCampos import *
from datetime import datetime
from interfaces.interfaceUtils import aplicarMascaraE

from interfaces.calculoIndicadores.calculoIndicadoresPorItem import CalculoIndicadoresPorItem
from interfaces.calculoIndicadores.calculoIndicadoresSoma import CalculoIndicadoresSoma
from interfaces.calculoIndicadores.calculoInidicadoresResult import CalculoIndicadoresResult


from interfaces.frmCardItensSistemaConstrutivo import FrameCardItensSistemaConstrutivo


class FrameCardIndicadoresAmbientais(FrameBase):
    
    def __init__(self, parent=None, session=None, atividade=None, modelAtividadeItem=None):
        super(FrameCardIndicadoresAmbientais, self).__init__(parent, session=session)

        self.__atividadeDto = atividade
        self.__modelAtividadeItem = modelAtividadeItem

        self.listaItensCalculados = []
        self.__descricaoTotal = ""
        
        self.__createUI()

        self.__carregarAtividade()


    def __createUI(self):

        frameTitulo = Frame()
        frameTitulo.pack(side=TOP, fill=X, padx=5, pady=5)

        self.lbTitulo = Label(master=frameTitulo, text="Titulo")
        self.lbTitulo.pack(side=LEFT)

        self.frmTreeView = Frame()
        self.frmTreeView.pack(side=TOP, fill=X, expand=1, padx=5, pady=5)

        
        self.treeView = ttk.Treeview(master=self.frmTreeView, height=4)

        self.scrollbarV = Scrollbar(master=self.frmTreeView)
        self.scrollbarV.pack(fill=Y, side=RIGHT)
        self.scrollbarV.config(command = self.treeView.yview)
        self.scrollbarH = Scrollbar(master=self.frmTreeView)
        self.scrollbarH.config(command = self.treeView.xview, orient=tk.HORIZONTAL)
        self.scrollbarH.pack(fill=X, side=BOTTOM)

        self.treeView.configure(xscrollcommand=self.scrollbarH.set,
                    yscrollcommand=self.scrollbarV.set)

        self.treeView.pack(expand=1, fill=X, side=TOP)

        self.__configurarColunasTreeView()


    def __configurarColunasTreeView(self):
        self.treeView['columns'] = ('descicao', 'unidade', 'quantidade', 'GWP', 'totalGWP', 'ODP', 'totalODP',\
            'AP', 'totalAP', 'EP', 'totalEP', 'POCP', 'totalPOCP', 'residPerigoso', 'totalResidPerigoso',\
            'residRadioativo', 'totalResidRadioativo',\
            'residNaoPerigoso', 'totalResidNaoPerigoso',\
            'energiaNaoRenovavel', 'energiaRenovavel', 'aguaRede', 'aguaReuso')
        self.treeView.heading("#0", text='Código ', anchor='w')
        self.treeView.column("#0", anchor='n', width=100)
        self.treeView.heading('descicao', text='Nome', anchor='w')
        self.treeView.column('descicao', anchor='w', width=240)
        self.treeView.heading('unidade', text='Unidade')
        self.treeView.column('unidade', anchor='center', width=100)
        self.treeView.heading('quantidade', text='Quantidade')
        self.treeView.column('quantidade', anchor='ne', width=100)
        
        self.treeView.heading('GWP', text='GWP (unitário)')
        self.treeView.column('GWP', anchor='ne', width=120)
        self.treeView.heading('totalGWP', text='Sub. Total GWP')
        self.treeView.column('totalGWP', anchor='ne', width=120)

        self.treeView.heading('ODP', text='ODP (unitário)')
        self.treeView.column('ODP', anchor='ne', width=120)
        self.treeView.heading('totalODP', text='Sub. Total ODP')
        self.treeView.column('totalODP', anchor='ne', width=120)

        self.treeView.heading('AP', text='AP (unitário)')
        self.treeView.column('AP', anchor='ne', width=120)
        self.treeView.heading('totalAP', text='Sub. Total AP')
        self.treeView.column('totalAP', anchor='ne', width=120)

        self.treeView.heading('EP', text='EP (unitário)')
        self.treeView.column('EP', anchor='ne', width=120)
        self.treeView.heading('totalEP', text='Sub. Total EP')
        self.treeView.column('totalEP', anchor='ne', width=120)

        self.treeView.heading('POCP', text='POCP (unitário)')
        self.treeView.column('POCP', anchor='ne', width=120)
        self.treeView.heading('totalPOCP', text='Sub. Total POCP')
        self.treeView.column('totalPOCP', anchor='ne', width=120)

        self.treeView.heading('residPerigoso', text='Quant. Residuos Perigosos (unitário)')
        self.treeView.column('residPerigoso', anchor='ne', width=260)
        self.treeView.heading('totalResidPerigoso', text='Sub. Total RP')
        self.treeView.column('totalResidPerigoso', anchor='ne', width=120)

        self.treeView.heading('residRadioativo', text='Quant. de Residuos Raioativos (Unitário)')
        self.treeView.column('residRadioativo', anchor='ne', width=280)
        self.treeView.heading('totalResidRadioativo', text='Sub. Total RR')
        self.treeView.column('totalResidRadioativo', anchor='ne', width=120)

        self.treeView.heading('residNaoPerigoso', text='Quant. Residuos não perigosos (unitário)')
        self.treeView.column('residNaoPerigoso', anchor='ne', width=290)
        self.treeView.heading('totalResidNaoPerigoso', text='Sub. Total RNP')
        self.treeView.column('totalResidNaoPerigoso', anchor='ne', width=120)

        self.treeView.heading('energiaNaoRenovavel', text='Energia Não Renovável')
        self.treeView.column('energiaNaoRenovavel', anchor='ne', width=160)

        self.treeView.heading('energiaRenovavel', text='Energia Renonável')
        self.treeView.column('energiaRenovavel', anchor='ne', width=140)

        self.treeView.heading('aguaRede', text='Água da Rede de Abastecimento')
        self.treeView.column('aguaRede', anchor='ne', width=250)

        self.treeView.heading('aguaReuso', text='Água de Reuso')
        self.treeView.column('aguaReuso', anchor='ne', width=120)



    def __carregarAtividade(self):
        if self.__atividadeDto == None:
            return

        itens = self.session.query(self.__modelAtividadeItem).\
            filter(self.__modelAtividadeItem.atividade_id == self.__atividadeDto.id).all()  

        for item in itens:
            itemCalculado = self.__calcularPorItem(item.pegarObjetoFk(), item.quantidade)
            self.__inserirItemNaGrid(item, item.pegarObjetoFk(), itemCalculado)
        
        self.ExibirTotais(self.listaItensCalculados, self.__descricaoTotal)


    def __calcularPorItem(self, item, quantidade):
        calculator = CalculoIndicadoresPorItem(item)
        itemCalculado = calculator.Calcular(quantidade)
        self.listaItensCalculados.append(itemCalculado)

        return itemCalculado

    def __inserirItemNaGrid(self, itemAtividade, itemEspecifico, itemCalculado):
        self.treeView.insert('', END, text=itemEspecifico.id, 
                                values=(itemEspecifico.nome, itemAtividade.unidade, itemAtividade.quantidade,
                                    aplicarMascaraE(itemEspecifico.emiEfeitoEstufa),
                                    aplicarMascaraE(itemCalculado.emiEfeitoEstufaGWP),
                                    aplicarMascaraE(itemEspecifico.emiDegradamOzonio),
                                    aplicarMascaraE(itemCalculado.emiDegradamOzonioODP),
                                    aplicarMascaraE(itemEspecifico.emiChuvaAcida),
                                    aplicarMascaraE(itemCalculado.emiChuvaAcidaAP),
                                    aplicarMascaraE(itemEspecifico.emiToxicosPatogenicos),
                                    aplicarMascaraE(itemCalculado.emiToxicosPatogenicosEP),
                                    aplicarMascaraE(itemEspecifico.emiContribEutroficacao),
                                    aplicarMascaraE(itemCalculado.emiContribEutroficacaoPOCP),
                                    aplicarMascaraE(itemEspecifico.qtdResiduosPerigosos), 
                                    aplicarMascaraE(itemCalculado.qtdResiduosPerigosos),
                                    aplicarMascaraE(itemEspecifico.qtdResiduosRadioativos), 
                                    aplicarMascaraE(itemCalculado.qtdResiduosRadioativos),
                                    aplicarMascaraE(itemEspecifico.qtdResiduosNaoPerigosos),
                                    aplicarMascaraE(itemCalculado.qtdResiduosNaoPerigosos),
                                    aplicarMascaraE(itemCalculado.qtdEnergiaNaoRenovavel),
                                    aplicarMascaraE(itemCalculado.qtdEnergiaRenovavel),
                                    aplicarMascaraE(itemCalculado.qtdAguaRedeAbastecimento),
                                    aplicarMascaraE(itemCalculado.qtdAguaReutilizada)))


    def DefinirAtividade(self, atividade):
        self.__atividadeDto = atividade
        self.__carregarAtividade()

    def DefinirDescricaoTotal(self, descricao):
        self.__descricaoTotal = descricao

    def ExibirTotais(self, listaItens, descricao):
        calculoSoma = CalculoIndicadoresSoma(listaItens) 
        soma = calculoSoma.Calcular(1)

        self.__inserirSomatorioFinal(soma, descricao)
    
    def definirHeightTreeView(self, height):
        self.treeView["height"] = height

    def definirDescricao(self, descricao):
        self.lbTitulo.config(text=descricao)
    
    
    def __inserirSomatorioFinal(self, itemCalculado, descricao):
        self.treeView.insert('', END, text="(+)", 
                                values=(descricao, "", "",
                                    "GWP", aplicarMascaraE(itemCalculado.emiEfeitoEstufaGWP),
                                    "ODP", aplicarMascaraE(itemCalculado.emiDegradamOzonioODP),
                                    "AP", aplicarMascaraE(itemCalculado.emiChuvaAcidaAP),
                                    "EP", aplicarMascaraE(itemCalculado.emiToxicosPatogenicosEP),
                                    "POCP", aplicarMascaraE(itemCalculado.emiContribEutroficacaoPOCP),
                                    "Residuos Perigosos", aplicarMascaraE(itemCalculado.qtdResiduosPerigosos),
                                    "Residuos Radioativos", aplicarMascaraE(itemCalculado.qtdResiduosRadioativos),
                                    "Residuos não Perigosos", aplicarMascaraE(itemCalculado.qtdResiduosNaoPerigosos),
                                    aplicarMascaraE(itemCalculado.qtdEnergiaNaoRenovavel),
                                    aplicarMascaraE(itemCalculado.qtdEnergiaRenovavel),
                                    aplicarMascaraE(itemCalculado.qtdAguaRedeAbastecimento),
                                    aplicarMascaraE(itemCalculado.qtdAguaReutilizada)))






        


        





