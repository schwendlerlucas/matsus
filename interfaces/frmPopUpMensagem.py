import tkinter as tk


class PopUpMensagem(tk.Toplevel):
    def __init__(self, master=None, callBack=None, mensagem=None):
        super().__init__(master)

        self.__callBack = callBack

        self.geometry("480x60+400+400")
        self.title("Aviso")

        if mensagem == None:
            mensagem = "Você tem certeza que deseja deletar este item?"
        
        tk.Label(self, text=mensagem).pack()

        tk.Button(self, text='Ok', command=self.destroy).pack(side=tk.BOTTOM , pady=5)

