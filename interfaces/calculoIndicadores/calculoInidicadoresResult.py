
class CalculoIndicadoresResult:

    emiEfeitoEstufaGWP = 0.0
    emiDegradamOzonioODP = 0.0
    emiChuvaAcidaAP = 0.0
    emiToxicosPatogenicosEP = 0.0
    emiContribEutroficacaoPOCP = 0.0


    #ambiental - resíduos
    qtdResiduosPerigosos = 0.0
    #intensidadeResiduosPerigosos = 0.0
    qtdResiduosRadioativos = 0.0
    #intensidadeResiduosRadioativos = 0.0
    qtdResiduosNaoPerigosos = 0.0

    #ambiental - consumo energia
    qtdEnergiaNaoRenovavel = 0.0
    qtdEnergiaRenovavel = 0.0
    #ambiental - consumo agua
    qtdAguaRedeAbastecimento = 0.0
    qtdAguaReutilizada = 0.0

    custo = 0.0

    #social
    grauSalubridade = 0.0
    grauSeguridade = 0.0

    
