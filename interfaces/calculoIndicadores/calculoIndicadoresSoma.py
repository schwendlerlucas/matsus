
from interfaces.calculoIndicadores.calculoInidicadoresResult import CalculoIndicadoresResult

class CalculoIndicadoresSoma:

    def __init__(self, itens):

        self.__itens = itens

    
    def Calcular(self, quantidade=1):

        result = CalculoIndicadoresResult()

        for item in self.__itens:


            result.emiEfeitoEstufaGWP += item.emiEfeitoEstufaGWP
            result.emiDegradamOzonioODP += item.emiDegradamOzonioODP
            result.emiChuvaAcidaAP += item.emiChuvaAcidaAP
            result.emiToxicosPatogenicosEP += item.emiToxicosPatogenicosEP
            result.emiContribEutroficacaoPOCP += item.emiContribEutroficacaoPOCP


            #ambiental - resíduos
            result.qtdResiduosPerigosos += item.qtdResiduosPerigosos
            #result.intensidadeResiduosPerigosos += item.intensidadeResiduosPerigosos
            result.qtdResiduosRadioativos += item.qtdResiduosRadioativos
            #result.intensidadeResiduosRadioativos += item.intensidadeResiduosRadioativos
            result.qtdResiduosNaoPerigosos += item.qtdResiduosNaoPerigosos

            #ambiental - consumo energia
            result.qtdEnergiaNaoRenovavel += item.qtdEnergiaNaoRenovavel
            result.qtdEnergiaRenovavel += item.qtdEnergiaRenovavel
            #ambiental - consumo agua
            result.qtdAguaRedeAbastecimento += item.qtdAguaRedeAbastecimento
            result.qtdAguaReutilizada += item.qtdAguaReutilizada

            result.custo += item.custo

            # social
            result.grauSalubridade += item.grauSalubridade
            result.grauSeguridade += item.grauSalubridade

        result = self.__calcularMediaItensSociais(result)
        return self.__multiplicarPelaQuantidade(result, quantidade)

    def __multiplicarPelaQuantidade(self, result, quantidade):

        result.emiEfeitoEstufaGWP *= float(quantidade)
        result.emiDegradamOzonioODP *= float(quantidade)
        result.emiChuvaAcidaAP *= float(quantidade)
        result.emiToxicosPatogenicosEP *= float(quantidade)
        result.emiContribEutroficacaoPOCP *= float(quantidade)


        #ambiental - resíduos
        result.qtdResiduosPerigosos *= float(quantidade)
        #result.intensidadeResiduosPerigosos += item.intensidadeResiduosPerigosos
        result.qtdResiduosRadioativos *= float(quantidade)
        #result.intensidadeResiduosRadioativos += item.intensidadeResiduosRadioativos
        result.qtdResiduosNaoPerigosos *= float(quantidade)

        #ambiental - consumo energia
        result.qtdEnergiaNaoRenovavel *= float(quantidade)
        result.qtdEnergiaRenovavel *= float(quantidade)
        #ambiental - consumo agua
        result.qtdAguaRedeAbastecimento *= float(quantidade)
        result.qtdAguaReutilizada *= float(quantidade)

        result.custo *= float(quantidade)

        return result

    def __calcularMediaItensSociais(self, result):
        if len(self.__itens) == 0:
            return(result)
        result.grauSalubridade /= len(self.__itens)
        result.grauSeguridade /= len(self.__itens)
        
        return result


