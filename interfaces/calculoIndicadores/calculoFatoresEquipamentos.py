from sqlalchemy.orm import Session
from models.equipamento import Equipamento
from models.fatoresEmissao import FatoresEmissao

class CalculoFatoresEquipamento():

    def __init__(self, session: Session):
        self.__session = session

    def Calcular(self, equipamento: Equipamento):
        fatorEnergia = self.__session.query(FatoresEmissao).filter(FatoresEmissao.id==1).first()
        fatorDiesel = self.__session.query(FatoresEmissao).filter(FatoresEmissao.id==2).first()

        equipamento.emiEfeitoEstufa = fatorEnergia.emiEfeitoEstufaGWP * float(equipamento.consumoMedioEnergia) + fatorDiesel.emiEfeitoEstufaGWP * float(equipamento.consumoMedioDiesel)
        equipamento.emiDegradamOzonio = fatorEnergia.emiDegradamOzonioODP * float(equipamento.consumoMedioEnergia) + fatorDiesel.emiDegradamOzonioODP * float(equipamento.consumoMedioDiesel)
        equipamento.emiChuvaAcida = fatorEnergia.emiChuvaAcidaAP * float(equipamento.consumoMedioEnergia) + fatorDiesel.emiChuvaAcidaAP * float(equipamento.consumoMedioDiesel)
        equipamento.emiToxicosPatogenicos = fatorEnergia.emiToxicosPatogenicosEP * float(equipamento.consumoMedioEnergia) + fatorDiesel.emiToxicosPatogenicosEP * float(equipamento.consumoMedioDiesel)
        equipamento.emiContribEutroficacao = fatorEnergia.emiContribEutroficacaoPOCP * float(equipamento.consumoMedioEnergia) + fatorDiesel.emiContribEutroficacaoPOCP * float(equipamento.consumoMedioDiesel)