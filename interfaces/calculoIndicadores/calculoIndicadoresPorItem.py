
from interfaces.calculoIndicadores.calculoInidicadoresResult import CalculoIndicadoresResult

class CalculoIndicadoresPorItem:

    def __tryToFloat(self, numeroStr, default):
        try:
            return float(numeroStr)
        except:
            return default

    def __init__(self, itemBase):

        self.__itemBase = itemBase

    
    def Calcular(self, quantidade):

        result = CalculoIndicadoresResult()

        result.emiEfeitoEstufaGWP =  self.__tryToFloat(self.__itemBase.emiEfeitoEstufa, 0) * quantidade
        result.emiDegradamOzonioODP = self.__tryToFloat(self.__itemBase.emiDegradamOzonio, 0) * quantidade
        result.emiChuvaAcidaAP = self.__tryToFloat(self.__itemBase.emiChuvaAcida, 0) * quantidade
        result.emiToxicosPatogenicosEP = self.__tryToFloat(self.__itemBase.emiToxicosPatogenicos, 0) * quantidade
        result.emiContribEutroficacaoPOCP = self.__tryToFloat(self.__itemBase.emiContribEutroficacao, 0) * quantidade


    #ambiental - resíduos
        result.qtdResiduosPerigosos = self.__tryToFloat(self.__itemBase.qtdResiduosPerigosos, 0) * quantidade
        #result.intensidadeResiduosPerigosos = self.__tryToFloat(self.__itemBase.intensidadeResiduosPerigosos, 0) * quantidade
        result.qtdResiduosRadioativos = self.__tryToFloat(self.__itemBase.qtdResiduosRadioativos, 0) * quantidade
        #result.intensidadeResiduosRadioativos = self.__tryToFloat(self.__itemBase.intensidadeResiduosRadioativos, 0) * quantidade
        result.qtdResiduosNaoPerigosos = self.__tryToFloat(self.__itemBase.qtdResiduosNaoPerigosos, 0) * quantidade

    #ambiental - consumo energia
        result.qtdEnergiaNaoRenovavel = self.__tryToFloat(self.__itemBase.qtdEnergiaNaoRenovavel, 0) * quantidade
        result.qtdEnergiaRenovavel = self.__tryToFloat(self.__itemBase.qtdEnergiaRenovavel, 0) * quantidade
    #ambiental - consumo agua
        result.qtdAguaRedeAbastecimento = self.__tryToFloat(self.__itemBase.qtdAguaRedeAbastecimento, 0) * quantidade
        result.qtdAguaReutilizada = self.__tryToFloat(self.__itemBase.qtdAguaReutilizada, 0) * quantidade

        return result

