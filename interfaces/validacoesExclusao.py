from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaterial import AtividadeMaterial
from models.atividadeMaoDeObra import AtividadeMaoDeObra
from models.custo import Custo
from models.projeto import Projeto
from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from models.projetoSistemaConstrutivoAtividade import ProjetoSistemaConstrutivoAtividade
from models.projetoSistemaConstrutivoAtividadeTransporte import ProjetoSistemaConstrutivoAtividadeTransporte


def validarExclusaoEquipamento(session, id):
    print(id)
    atividades = session.query(AtividadeEquipamento).filter(AtividadeEquipamento.equipamento_id==id).first()
    if atividades != None:
        return False

    custos = session.query(Custo).filter(Custo.equipamento_id==id).first()
    print(custos)
    if custos != None:
        return False
    
    projeto = session.query(ProjetoSistemaConstrutivoAtividadeTransporte).filter(
        ProjetoSistemaConstrutivoAtividadeTransporte.equipamento_id==id).first()
    if projeto != None:
        return False

    projeto = session.query(ProjetoSistemaConstrutivoAtividadeTransporte).filter(
        ProjetoSistemaConstrutivoAtividadeTransporte.meioTransporte_id==id).first()
    if projeto != None:
        return False

    return True

def validarExclusaoMaterial(session, id):
    print(id)
    atividades = session.query(AtividadeMaterial).filter(AtividadeMaterial.material_id==id).first()
    if atividades != None:
        return False

    custos = session.query(Custo).filter(Custo.material_id==id).first()
    print(custos)
    if custos != None:
        return False

    projeto = session.query(ProjetoSistemaConstrutivoAtividadeTransporte).filter(
        ProjetoSistemaConstrutivoAtividadeTransporte.material_id==id).first()
    if projeto != None:
        return False

    return True

def validarExclusaoMaoDeObra(session, id):
    print(id)
    atividades = session.query(AtividadeMaoDeObra).filter(AtividadeMaoDeObra.maoDeObra_id==id).first()
    if atividades != None:
        return False

    custos = session.query(Custo).filter(Custo.maoDeObra_id==id).first()
    print(custos)
    if custos != None:
        return False

    return True

def validarExclusaoAtividade(session, id):
    sistemaConstrutivo = session.query(SistemaConstrutivoAtividade).filter(SistemaConstrutivoAtividade.atividade_id==id).first()
    if sistemaConstrutivo != None:
        return False

    projeto = session.query(ProjetoSistemaConstrutivoAtividade).filter(
        ProjetoSistemaConstrutivoAtividade.atividade_id==id).first()
    if projeto != None:
        return False
    
    projeto = session.query(ProjetoSistemaConstrutivoAtividadeTransporte).filter(
        ProjetoSistemaConstrutivoAtividadeTransporte.atividade_id==id).first()
    if projeto != None:
        return False
    
    return True

def validarExclusaoSistemaConstrutivo(session, id):
    print(id)
    projeto = session.query(ProjetoSistemaConstrutivo).filter(
        ProjetoSistemaConstrutivo.sistemaConstrutivo_id==id).first()
    if projeto != None:
        return False
    
    projeto = session.query(ProjetoSistemaConstrutivoAtividade).filter(
        ProjetoSistemaConstrutivoAtividade.sistemaConstrutivo_id==id).first()
    if projeto != None:
        return False

    projeto = session.query(ProjetoSistemaConstrutivoAtividadeTransporte).filter(
        ProjetoSistemaConstrutivoAtividadeTransporte.sistemaConstrutivo_id==id).first()
    if projeto != None:
        return False


    return True
