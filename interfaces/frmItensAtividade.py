from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from interfaces.constantesViews import *
from interfaces.frmCardItensBase import FrameCardItensBase
from interfaces.validacoesCampos import *

class FrameItensAtividade(FrameCardItensBase):

    def __init__(self, parent, session, model, modelAtividade):
        super(FrameItensAtividade, self).__init__(parent, session=session, model=model, modelAtividade=modelAtividade)


    def CreateUI(self):
        super(FrameItensAtividade, self).CreateUI()

        self.treeView['columns'] = ('descicao', 'unidade', 'quantidade')
        self.treeView.heading("#0", text='Código ', anchor='w')
        self.treeView.column("#0", anchor='n', width=100)
        self.treeView.heading('descicao', text='Mao de obra', anchor='w')
        self.treeView.column('descicao', anchor='w', width=200)
        self.treeView.heading('unidade', text='Unidade')
        self.treeView.column('unidade', anchor='center', width=100)
        self.treeView.heading('quantidade', text='Quantidade')
        self.treeView.column('quantidade', anchor='ne', width=100)

        self.cbbItemClasse = ttk.Combobox(master=self.frameEdicao, state='readonly', width=34) 
        self.cbbItemClasse.pack(side=LEFT, padx=5)
        self.cbbItemClasse.bind('<<ComboboxSelected>>', self.onChangeItemClasse)

        self.edtUnidadeVar = StringVar()
        self.edtUnidade = Entry(master=self.frameEdicao, width=10, textvariable=self.edtUnidadeVar)
        self.edtUnidade.pack(side=LEFT, after=self.cbbItemClasse, padx=5)
        
        self.edtQuantidadeVar = StringVar()
        self.edtQuantidade = Entry(master=self.frameEdicao, width=10, textvariable=self.edtQuantidadeVar)
        self.edtQuantidade.pack(side=LEFT, after=self.edtUnidade, padx=5)

        self.btnIncluir = Button(master=self.frameEdicao, text="ok", command=self.__btnOkEdicaoClick, width=2)
        self.btnIncluir.pack(side=LEFT, padx=5, pady=5)

    def __btnOkEdicaoClick(self):
        super(FrameItensAtividade, self).btnOkEdicaoClick()


    def LoadTable(self):
        super(FrameItensAtividade, self).LoadTable()

        for item in self.listaItens:
            self.treeView.insert('', END, text=item.pegarObjetoFk().id, 
                                values=(item.pegarObjetoFk().nome, item.unidade, item.quantidade))


    def updateItem(self, item):
        super(FrameItensAtividade, self).updateItem(item)
        item.unidade = self.edtUnidade.get()
        item.quantidade = self.edtQuantidadeVar.get()       

    
    def updateComponents(self, item):
        super(FrameItensAtividade, self).updateComponents(item)

        self.edtUnidadeVar.set(item.unidade)
        self.edtQuantidadeVar.set(item.quantidade)

    def limparCamposEdicao(self):
        super(FrameItensAtividade, self).limparCamposEdicao()
        self.edtUnidadeVar.set("")
        self.edtQuantidadeVar.set("")

    def validarCampos(self):
        super(FrameItensAtividade, self).limparCamposEdicao()
        if self.cbbItemClasse.get() == "":
            return False
        
        if self.edtUnidade.get() == "":
            return False

        if not validarDouble(self.edtQuantidade.get()):
            return False
        
        return True    

    def definirIdPaiNoItem(self, item, id):
        super(FrameItensAtividade, self).definirIdPaiNoItem(item, id)

        item.atividade_id = id
        
    def consultar(self, id):
        super(FrameItensAtividade, self).consultar(id)

        self.listaItens = self.session.query(self.modelAtividade).\
            filter(self.modelAtividade.atividade_id == id).all()  
        
        self.LoadTable() 

    
    
    