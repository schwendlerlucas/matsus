from sqlalchemy import create_engine, Column, Integer, String, Float, Boolean
from database.mydatabase import BaseClassSqlAlchemy

class Projeto(BaseClassSqlAlchemy):
    __tablename__ = "projeto"

    id = Column('id', Integer, primary_key=True)
    nome = Column('nome', String)

    estado = Column('estado', String)

    classifRegiao = Column('classif_regiao', String)
    situacaoFinanceira = Column('situacao_inanceira', String)
    distanciaCentroUrbano = Column('distancia_centro_urbano', Float)
    nvlConhecimentoTecAlternativas = Column('conhec_tec_alternativas', String)
    nvlConhecimentoTecBaseSolo = Column('nvl_conhec_tec_base_solo', String)
    grauAceitacaoTecBaseSolo = Column('grau_aceitacao_tec_base_solo', String)
    predisposicaoSolo = Column('predisposicao_solo', String)
    culturaAutoConstrucao = Column('cultura_auto_construcao', Boolean)
    qtdSistemaConstrutivo = Column('qtd_sistema_construtivo', Float)


