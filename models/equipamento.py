from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Boolean, Float
from database.mydatabase import BaseClassSqlAlchemy

class Equipamento(BaseClassSqlAlchemy):
    __tablename__ = "equipamento"

    id = Column('id', Integer, primary_key=True)
    nome = Column('nome', String)
    fonteEnergia = Column('fonte_energia', String)
    peso = Column('peso', String)
    porte = Column('porte', String)
    consumoAgua = Column('consumo_agua', Float)
    consumoMedioDiesel = Column('consumo_medio_diesel', Float)
    consumoMedioEnergia = Column('consumo_medio_energia', Float)
    
    #social
    possuiGarantiaTecnica = Column('possui_garantia_tecnica', Boolean)
    grauFacilidadeEstocagem = Column('grau_facilidade_estocagem', String)    
    grauFacilidadeTransporte = Column('grau_facilidade_transporte', String)
    oferta = Column('oferta', String)      

    #ambiental - emissões
    emiEfeitoEstufa = Column('emi_efeito_estufa', String)
    emiDegradamOzonio = Column('emi_degradam_ozonio', String)
    emiChuvaAcida = Column('emi_chuva_acida', String)
    emiToxicosPatogenicos = Column('emi_toxicos_patologicos', String)
    emiContribEutroficacao = Column('emi_contrib_Eutroficacao', String)
    #ambiental - resíduos
    qtdResiduosPerigosos = Column('qtd_resi_perigoso', String)
    #intensidadeResiduosPerigosos = Column('intens_resi_perigoso', String)
    qtdResiduosRadioativos = Column('qtd_resi_radioativo', String)
    #intensidadeResiduosRadioativos = Column('intens_resi_radioativo', String)
    qtdResiduosNaoPerigosos = Column('qtd_residuo_nao_perigoso', String)
    #ambiental - consumo energia
    qtdEnergiaNaoRenovavel = Column('qtd_energia_nao_renovavel', String)
    qtdEnergiaRenovavel = Column('qtd_energia_renovavel', String)
    #ambiental - consumo agua
    qtdAguaRedeAbastecimento = Column('qtd_agua_rede', String)
    qtdAguaReutilizada = Column('qtd_agua_reutilizada', String)

    ehMeioTransporte = Column('eh_meio_transporte', Boolean)
