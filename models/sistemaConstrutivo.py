from sqlalchemy import create_engine, Column, Integer, String, Float
from database.mydatabase import BaseClassSqlAlchemy

class SistemaConstrutivo(BaseClassSqlAlchemy):
    __tablename__ = "sistema_construtivo"

    id = Column('id', Integer, primary_key=True)
    nome = Column('nome', String)

    econAquisicaoMatLocal = Column('econ_aquisicao_matLocal', String)
    econMaoObraLocal = Column('econ_mao_obra_local', Float)

    socGrauPopilarizacaoConce = Column('soc_grau_popilarizacao_conce', Float)
    socGrauFomentosInstituicoes = Column('soc_grau_fomentos_instituicoes', Float)
    socGrauMateriaisCult = Column('soc_grau_materiais_cult', Float)
    socGrauContribuicao = Column('soc_grau_contribuicao', Float)
    socGrauComplexidade = Column('soc_grau_complexidade', Float)
    socGrauPossibMutiroes = Column('soc_grau_possibMutiroes', Float)

    tecParcelaPodeReclidada = Column('tec_parcela_pode_reclidada', Float)
    tecParcelaPodeReaproveitada = Column('tec_parcela_pode_reaproveitada', Float)
    tecGrauEstanqueidade = Column('tec_grau_estanqueidade', Float)
    tecGrauTransmitanciaTermica = Column('tecGrau_transmitancia_termica', Float)
    tecGrauTransmitanciaSonora = Column('tecGrau_transmitancia_sonora', Float)
    tecComportamentoMecanico = Column('tec_comportamento_mecanico', Float)
    tecDurabilidade = Column('tec_durabilidade', Float)
    tecAcompanhamentoTecnico = Column('tec_acompanhamento_tecnico', Float)
    tecGrauPerecibilidade= Column('tec_grau_perecibilidade', Float)
    tecFacilidadeEstocagem = Column('tec_facilidade_estocagem', Float)
    tecFacilidadeTransporte = Column('tec_facilidade_transporte', Float)
    tecFacilidadeManutPeriodica = Column('tec_facilidade_manutPeriodica', Float)
    tecFacilidadeReparos = Column('tecFacilidadeReparos', Float)
    tecPadrinizacaoReplicacao = Column('tec_padrinizacao_replicacao', Float)
    tecVersatilidade = Column('tec_versatilidade', Float)