from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from database.mydatabase import BaseClassSqlAlchemy

class SistemaConstrutivoAtividade(BaseClassSqlAlchemy):
    __tablename__ = "sis_constr_atividade"

    id = Column('id', Integer, primary_key=True)

    atividade_id = Column( Integer, ForeignKey('atividade.id'))
    atividade = relationship("Atividade")

    sistemaConstrutivo_id = Column( Integer, ForeignKey('sistema_construtivo.id'))
    sistemaConstrutivo = relationship("SistemaConstrutivo")

    quantidade = Column('qtd_empregada_m2', Float)
    


    def definirFk(self, fkId):
        self.atividade_id = fkId

    def pegarObjetoFk(self):
        return self.atividade
    
    def definirObjetoFk(self, atividade):
        self.atividade = atividade