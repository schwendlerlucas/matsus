from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from database.mydatabase import BaseClassSqlAlchemy

class User(BaseClassSqlAlchemy): 
    __tablename__ = "users"

    id = Column('id', Integer, primary_key=True)
    tratamento = Column('de_tramento', String)
    firstName = Column('first_name', String)
    lastName = Column('last_name', String)
    emailUsuario = Column('email_usuario', String)

    pais = Column('de_pais', String)
    estado = Column('de_estado', String)
    cidade = Column('de_cidade', String)
    contatoTelefone = Column('de_contato_telefone', String)
    organizacaoVinculado = Column('organizacao_vinculado', String)
    senha = Column('senha', String) 