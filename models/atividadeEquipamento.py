from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from database.mydatabase import BaseClassSqlAlchemy

class AtividadeEquipamento(BaseClassSqlAlchemy):
    __tablename__ = "atividade_equipamento"

    id = Column('id', Integer, primary_key=True)

    atividade_id = Column( Integer, ForeignKey('atividade.id'))
    atividade = relationship("Atividade")

    equipamento_id = Column( Integer, ForeignKey('equipamento.id'))
    equipamento = relationship("Equipamento")

    unidade = Column('unidade', String)
    quantidade = Column('quantidade', Float)

    def definirFk(self, fkId):
        self.equipamento_id = fkId

    def pegarObjetoFk(self):
        return self.equipamento
    
    def definirObjetoFk(self, equipamento):
        self.equipamento = equipamento