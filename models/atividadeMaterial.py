from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from database.mydatabase import BaseClassSqlAlchemy

class AtividadeMaterial(BaseClassSqlAlchemy):
    __tablename__ = "atividade_material"

    id = Column('id', Integer, primary_key=True)

    atividade_id = Column( Integer, ForeignKey('atividade.id'))
    atividade = relationship("Atividade")

    material_id = Column( Integer, ForeignKey('material.id'))
    material = relationship("Material")

    unidade = Column('unidade', String)
    quantidade = Column('quantidade', Float)

    def definirFk(self, fkId):
        self.material_id = fkId

    def pegarObjetoFk(self):
        return self.material
    
    def definirObjetoFk(self, material):
        self.material = material