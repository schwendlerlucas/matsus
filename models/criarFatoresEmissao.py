from models.fatoresEmissao import FatoresEmissao
from sqlalchemy.orm import Session

class CriarFatoresEmissao():

    def __init__(self, session: Session):

        self.__session = session

    def CriarSeNaoExistir(self):

        fatores = self.__session.query(FatoresEmissao).all()
        if len(fatores) != 0:
            return
        
        fatorEnergia = FatoresEmissao()
        fatorEnergia.emiEfeitoEstufaGWP = 0.104
        fatorEnergia.emiDegradamOzonioODP = 0
        fatorEnergia.emiChuvaAcidaAP = 0
        fatorEnergia.emiToxicosPatogenicosEP = 0
        fatorEnergia.emiContribEutroficacaoPOCP = 0

        fatorDiesel = FatoresEmissao()
        fatorDiesel.emiEfeitoEstufaGWP = 2.671
        fatorDiesel.emiDegradamOzonioODP = (1.79856115*10.0 ** -4.0) * 0.0
        fatorDiesel.emiChuvaAcidaAP = (1.79856115 * 10.0 ** -4.0)*(0.449+5.31)
        fatorDiesel.emiToxicosPatogenicosEP = (1.79856115 * 10 ** -4)*0
        fatorDiesel.emiContribEutroficacaoPOCP = (1.79856115 * 10 ** -4)*0.92

        self.__session.add(fatorEnergia)
        self.__session.add(fatorDiesel)
        self.__session.commit()



