from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from database.mydatabase import BaseClassSqlAlchemy

class Pesos():
    GWP = 0.4325
    ODP = 0.1351
    AP = 0.1351
    EP = 0.1622
    POCP = 0.1351
    ResidPerigoso = 0.40
    ResidRadioativo = 0.25
    ResidNaoPerigoso = 0.35
    EnergiaNaoRenovavel = 0.50
    EnergiaRenovavel = 0.50
    AguaRedeAbastesimento = 0.50
    AguaReuso = 0.50

    Custo = 1

    AquisiMateriaPrimaLocal = 0.50
    MaoObraLocal = 0.50
        
    GrauPopularizacaoConceitos = 0.50
    GrauFomentoInstituicao = 0.50
    GrauMatCulturalmenteUsados = 1
    GrauSalubridade = 0.50
    GrauSeguridade = 0.50
    GrauContribNaEdificacao = 0.30
    GrauComplexidade = 0.30
    PossibilidadeMutiroes = 0.40

    ParcelaPodeSerReciclada = 0.50
    ParcelaPodeSerReaproveitada = 0.50
    GrauEstanqueidade = 0.1786
    GrauTransmitanciaTermica = 0.1786
    GrauTransmissaoOndas = 0.1786
    ComportMecanico = 0.2321
    Durabilidade = 0.2321
    AcompanhamentoProfissionais = 1
    GrauPericibilidade = 0.2963
    FacilidadeEstocagem = 0.3333
    FacilidadeTransporte = 0.3704
    FacilidadeManutencao = 0.2941
    FacilidadeReparo = 0.2941
    PadronizacaoReplicacao = 0.2059
    Versatilidade = 0.2059