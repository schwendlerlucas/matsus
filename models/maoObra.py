from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from database.mydatabase import BaseClassSqlAlchemy

class MaoObra(BaseClassSqlAlchemy):
    __tablename__ = "mao_obra"

    id = Column('id', Integer, primary_key=True)
    nome = Column('nome', String)
    nivelFormacao = Column('nivel_formacao', String)