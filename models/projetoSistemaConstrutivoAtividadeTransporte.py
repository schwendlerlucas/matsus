from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from database.mydatabase import BaseClassSqlAlchemy

class ProjetoSistemaConstrutivoAtividadeTransporte(BaseClassSqlAlchemy):
    __tablename__ = "projeto_sis_ativi_trans"

    id = Column('id', Integer, primary_key=True)

    atividade_id = Column( Integer, ForeignKey('atividade.id'))
    atividade = relationship("Atividade")
    
    sistemaConstrutivo_id = Column( Integer, ForeignKey('sistema_construtivo.id'))
    sistemaConstrutivo = relationship("SistemaConstrutivo")

    projeto_id = Column( Integer, ForeignKey('projeto.id'))
    projeto = relationship("Projeto")

    equipamento_id = Column( Integer, ForeignKey('equipamento.id'))
    equipamento = relationship("Equipamento", foreign_keys=equipamento_id)

    material_id = Column( Integer, ForeignKey('material.id'))
    material = relationship("Material")

    meioTransporte_id = Column(Integer, ForeignKey('equipamento.id'))
    meioTransporte = relationship("Equipamento", foreign_keys=meioTransporte_id)

    distancia1 = Column('distancia1', Float) 
    distancia2 = Column('distancia2', Float)

    valorFrete = Column('valor_frete', Float)


    
    