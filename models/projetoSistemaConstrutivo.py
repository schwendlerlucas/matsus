from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from database.mydatabase import BaseClassSqlAlchemy

class ProjetoSistemaConstrutivo(BaseClassSqlAlchemy):
    __tablename__ = "projeto_sis_constr"

    id = Column('id', Integer, primary_key=True)

    projeto_id = Column( Integer, ForeignKey('projeto.id'))
    projeto = relationship("Projeto")

    sistemaConstrutivo_id = Column( Integer, ForeignKey('sistema_construtivo.id'))
    sistemaConstrutivo = relationship("SistemaConstrutivo")
    

    def definirFk(self, fkId):
        self.sistemaConstrutivo_id = fkId

    def pegarObjetoFk(self):
        return self.sistemaConstrutivo
    
    def definirObjetoFk(self, sistemaConstrutivo):
        self.sistemaConstrutivo = sistemaConstrutivo