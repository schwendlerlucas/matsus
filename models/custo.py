from sqlalchemy import create_engine, Column, Integer, String, Float, Date, ForeignKey
from sqlalchemy.orm import relationship
from database.mydatabase import BaseClassSqlAlchemy

class Custo(BaseClassSqlAlchemy):
    __tablename__ = "custo"

    id = Column('id', Integer, primary_key=True)
    estado = Column('estado', String)

    equipamento_id = Column( Integer, ForeignKey('equipamento.id'))
    equipamento = relationship("Equipamento")

    maoDeObra_id = Column( Integer, ForeignKey('mao_obra.id'))
    maoDeObra = relationship("MaoObra")
   
    material_id = Column(Integer, ForeignKey('material.id'))
    material = relationship("Material")

    custoMedio = Column('custo_medio', Float)
    unidade = Column('unidade', String)
    dtCotacao  = Column('dt_cotacao', Date)


