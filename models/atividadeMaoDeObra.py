from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from database.mydatabase import BaseClassSqlAlchemy

class AtividadeMaoDeObra(BaseClassSqlAlchemy):
    __tablename__ = "atividade_mao_obra"

    id = Column('id', Integer, primary_key=True)

    atividade_id = Column( Integer, ForeignKey('atividade.id'))
    atividade = relationship("Atividade")

    maoDeObra_id = Column( Integer, ForeignKey('mao_obra.id'))
    maoDeObra = relationship("MaoObra")

    unidade = Column('unidade', String)
    quantidade = Column('quantidade', Float)

    def definirFk(self, fkId):
        self.maoDeObra_id = fkId

    def pegarObjetoFk(self):
        return self.maoDeObra
    
    def definirObjetoFk(self, maoDeObra):
        self.maoDeObra = maoDeObra