from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from database.mydatabase import BaseClassSqlAlchemy

class Atividade(BaseClassSqlAlchemy):
    __tablename__ = "atividade"

    id = Column('id', Integer, primary_key=True)
    nome = Column('nome', String)

    encargoSocial = Column('encargo_social', Float)
    grauSalubridade = Column('grau_salubridade', Float)
    grauSeguridade = Column('grau_seguridade', Float)
