from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float
from sqlalchemy.orm import relationship
from database.mydatabase import BaseClassSqlAlchemy

class ProjetoSistemaConstrutivoAtividade(BaseClassSqlAlchemy):
    __tablename__ = "projeto_sis_ativi_ativ"

    id = Column('id', Integer, primary_key=True)

    projeto_id = Column( Integer, ForeignKey('projeto.id'))
    projeto = relationship("Projeto")

    sistemaConstrutivo_id = Column( Integer, ForeignKey('sistema_construtivo.id'))
    sistemaConstrutivo = relationship("SistemaConstrutivo")

    atividade_id = Column( Integer, ForeignKey('atividade.id'))
    atividade = relationship("Atividade")

    grauSalubridade = Column('grau_salubridade', Float) 
    grauSeguridade = Column('grau_seguridade', Float)



    
    