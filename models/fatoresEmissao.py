from sqlalchemy import create_engine, Column, Integer, String, Float
import sys
from database.mydatabase import BaseClassSqlAlchemy

class FatoresEmissao(BaseClassSqlAlchemy):
    __tablename__ = "fatores_emissao"

    id = Column('id', Integer, primary_key=True)

    emiEfeitoEstufaGWP = Column('emi_efeito_estufa', Float)
    emiDegradamOzonioODP = Column('emi_degradam_ozonio', Float)
    emiChuvaAcidaAP = Column('emi_chuva_acida', Float)
    emiToxicosPatogenicosEP = Column('emi_toxicos_patologicos', Float)
    emiContribEutroficacaoPOCP = Column('emi_contrib_Eutroficacao', Float)