from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Boolean
from database.mydatabase import BaseClassSqlAlchemy

class Material(BaseClassSqlAlchemy):
    __tablename__ = "material"

    id = Column('id', Integer, primary_key=True)
    nome = Column('nome', String)
    tipo = Column('tipo', String)
    fonecedorFabricante = Column('fornecedor_fabricante', String)
    informacoes = Column('informacoes', String)
    possuiDeclaracao = Column('possui_declaracao', Boolean)
    fonteEPD = Column('epd_fonte', String)
    cdEPD = Column('cd_epd', String)
    paisEPD = Column('epd_pais', String)
    dtPublicacao = Column('epd_dtPublicacao', String)


    #ambiental - emissões
    emiEfeitoEstufa = Column('emi_efeito_estufa', String)
    emiDegradamOzonio = Column('emi_degradam_ozonio', String)
    emiChuvaAcida = Column('emi_chuva_acida', String)
    emiToxicosPatogenicos = Column('emi_toxicos_patologicos', String)
    emiContribEutroficacao = Column('emi_contrib_Eutroficacao', String)
    #ambiental - resíduos
    qtdResiduosPerigosos = Column('qtd_resi_perigoso', String)
    #intensidadeResiduosPerigosos = Column('intens_resi_perigoso', String)
    qtdResiduosRadioativos = Column('qtd_resi_radioativo', String)
    #intensidadeResiduosRadioativos = Column('intens_resi_radioativo', String)
    qtdResiduosNaoPerigosos = Column('qtd_residuo_nao_perigoso', String)
    #ambiental - consumo energia
    qtdEnergiaNaoRenovavel = Column('qtd_energia_nao_renovavel', String)
    qtdEnergiaRenovavel = Column('qtd_energia_renovavel', String)
    #ambiental - consumo agua
    qtdAguaRedeAbastecimento = Column('qtd_agua_rede', String)
    qtdAguaReutilizada = Column('qtd_agua_reutilizada', String)
