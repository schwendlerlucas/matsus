from sqlalchemy import create_engine, Column, Integer, String, Float, Boolean
from database.mydatabase import BaseClassSqlAlchemy

class FatoresDecisao(BaseClassSqlAlchemy):
    __tablename__ = "fatores_decisao"

    id = Column('id', Integer, primary_key=True)
    
    AI = Column('AI', Float)
    AII = Column('AII', Float)
    AIII = Column('AIII', Float)
    AIV = Column('AIV', Float)

    SI = Column('SI', Float)
    SII = Column('SII', Float)
    SIII = Column('SIII', Float)
    SIV = Column('SIV', Float)
            

    EI = Column('EI', Float)
    EII = Column('EII', Float)

    TI = Column('TI', Float)
    TII = Column('TII', Float)
    TIII = Column('TIII', Float)
    TIV = Column('TIV', Float)
    TV = Column('TV', Float)


