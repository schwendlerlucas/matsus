from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from database.mydatabase import BaseClassSqlAlchemy

class CreateTables():

    def create(self, db_engine):
        BaseClassSqlAlchemy.metadata.create_all(bind=db_engine)
