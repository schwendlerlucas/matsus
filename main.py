from tkinter import *

from sqlalchemy.orm import sessionmaker, relationship

from database.mydatabase import *
from database.createTables import CreateTables
from models.criarFatoresEmissao import *

from interfaces.frmLogin import FrameLogin
from interfaces.frmCadUsuario import FrameCadUsuario
from interfaces.frmMenu import FrameMenu
from interfaces.frmCadMaoObra import FrameCadMaoDeObra
from interfaces.frmCadMaterial import FrameCadMaterial
from interfaces.frmCadMaterialAmbiental import FrameCadMaterialAmbiental
from interfaces.frmCadSistemaConstrutivoIndicadoresEconomicos import FrameCadSistemaConstrutivoIndicadoresEconomicos
from interfaces.frmCadSistemaConstrutivoIndicadoresSociais import FrameCadSistemaConstrutivoIndicadoresSociais
from interfaces.frmCadSistemaConstrutivoIndicadoresTecnicos import FrameCadSistemaConstrutivoIndicadoresTecnicos
from interfaces.frmCadSistemaConstrutivo import FrameCadSistemaConstrutivo
from interfaces.frmCadEquipamento import FrameCadEquipamento
from interfaces.frmCadEquipamentoAmbiental import FrameCadEquipamentoAmbiental
from interfaces.frmPesquisar import FramePesquisa
from interfaces.frmCadCusto import FrameCadCusto
from interfaces.frmCadAtividade import FrameCadAtividade
from interfaces.frmCadSistemaConstrutivo import FrameCadSistemaConstrutivo
from interfaces.frmIndicadoresAmbientais import FrameIndicadoresAmbientais
from interfaces.frmIndicadoresEconomicos import FrameIndicadoresEconomicos
from interfaces.validacoesExclusao import *
from interfaces.exclusaoRegistrosFilhos import *
from interfaces.frmCadProjeto import FrameCadProjeto
from interfaces.frmCadProjetoSelecionarSistemas import FrameCadProjetoSelecionarSistemas
from interfaces.frmCadProjetoComparaSistemas import FrameCadProjetoComparaSistemas
from interfaces.frmSistemaConstrutivoProjetoResultado import FrameSistemaConstrutivoProjetoResultado
from interfaces.frmCategoriasPonderadas import FrameCategoriasPonderadas
from interfaces.frmParecerMelhorSistema import FrameParecerMelhorSistema
from interfaces.frmGraficosCategoriasPonderadas import FrameGraficosCategoriasPonderadas


from models.material import Material
from models.equipamento import Equipamento
from models.maoObra import MaoObra
from models.custo import Custo
from models.atividade import Atividade
from models.sistemaConstrutivo import SistemaConstrutivo
from models.criarFatoresEmissao import CriarFatoresEmissao
from models.projeto import Projeto

from DTOs.cadProjetoDto import CadProjetoDTO

from controllers.projetoController import ProjetoController
from controllers.projetoSistemaConstrutivoController import ProjetoSistemaConstrutivoController
from controllers.sistemaConstrutivoAtividadeController import SistemaConstrutivoAtividadeController
from controllers.insumoTransporteController import InsumoTransporteController

from interfaces.frmAnaliseDeTransporte import FrameAnaliseDeTransporte
from interfaces.frmCadFatoresDecisao import FrameFatoresDecisao

db = MyDatabase(SQLITE, dbname='./database/mydb.sqlite')

createAll = CreateTables()
createAll.create(db.db_engine)

Session = sessionmaker(bind=db.db_engine)
session = Session()


criarFatores = CriarFatoresEmissao(session)
criarFatores.CriarSeNaoExistir()

root = Tk()

def limparRoot():
    for width in root.winfo_children():
        width.destroy()


def openCadUsuario():
    frmCadUsuario = FrameCadUsuario(root, session=session)
    frmCadUsuario.definirVoltarTelaLogin(openTelaLogin)

def openMenu():
    frmMenu = FrameMenu(root)
    frmMenu.definirAbrirCadMaoObra(openCadMaoDeObra)
    frmMenu.definirAbrirCadMaterial(openCadMaterial)
    frmMenu.definirAbrirCadEquipamentos(openCadEquipamento)
    frmMenu.definirAbrirCadAtividade(openCadAtividade)
    frmMenu.definitAbrirCadCusto(openCadCusto)
    frmMenu.definitAbrirCadSistemaConstrutivo(openCadSistemaConstrutivoIndicadoresEconomicos)
    frmMenu.definirAbrirCadProjeto(openCadProjeto)
    frmMenu.definitAbrirIndicadoresAmbientais(openIndicadoresAmbientais)
    frmMenu.definitAbrirIndicadoresEconomicos(openIndicadoresEconomicos)
    frmMenu.definirAbrirIndicadoresSistema(openCadPesquisaProjetoResultados)
    frmMenu.definirAbrirCadFatoresDecisao(openCadFatoresDecisao)
    frmMenu.definirAbrirIndicadoresSistemaComPesos(openCadPesquisaProjetoResultadosComPesos)
    frmMenu.definirAbrirDefinirDesempenhoSustentavel(openCadPesquisaProjetoParecerMelhorSistema)
    frmMenu.definirAbrirIndicadoresDeTransporte(openCadPesquisaProjetoAnaliseTransporte)

#######################################################
def openTelaLogin():
    frmLogin = FrameLogin(root, session)

    frmLogin.definirOpenCadUsuario(openCadUsuario)
    frmLogin.definirAbrirMenu(openMenu)

def openCadMaterialConstrutivo():
    frmCadMaterialConstrutivo = FrameCadSistemaConstrutivo(root, session=session)
    frmCadMaterialConstrutivo.definirVoltarMenu(openMenu)

def openCadMaoDeObra(maoDeObra=None):
    frmCadMaoDeObra = FrameCadMaoDeObra(root, session=session, maoDeObra=maoDeObra)
    frmCadMaoDeObra.definirVoltarMenu(openMenu)
    frmCadMaoDeObra.definirPesquisar(openCadPesquisaMaoDeObra)

def openCadMaterial(material=None):
    frmCadMaterial = FrameCadMaterial(root, session=session, material=material)
    frmCadMaterial.definirVoltarMenu(openMenu)
    frmCadMaterial.definirTelaSeguinteCadMaterial(openTelaSeguinteCadMaterialAmbiental)
    frmCadMaterial.definirPesquisar(openCadPesquisaMaterial)

def openTelaSeguinteCadMaterialAmbiental(material=None):
     frmCadMaterialAmbiental = FrameCadMaterialAmbiental(root, session=session, material=material)
     frmCadMaterialAmbiental.definirVoltar(openCadMaterial)
     frmCadMaterialAmbiental.definirVoltarMenu(openMenu)
     frmCadMaterialAmbiental.definirNovoCadastro(openCadMaterial)

#####################################################33

def openCadEquipamento(equipamento=None):
    frmCadEquipamento = FrameCadEquipamento(root, session=session, equipamento=equipamento)
    frmCadEquipamento.definirVoltarMenu(openMenu)
    frmCadEquipamento.definirTelaSeguinteCadEquipamento(openTelaSeguinteCadEquipamentoAmbiental)
    frmCadEquipamento.definirPesquisar(openCadPesquisaEquipamento)

def openTelaSeguinteCadEquipamentoAmbiental(equipamento=None):
     frmCadEquipamentoAmbiental= FrameCadEquipamentoAmbiental(root, session=session, equipamento=equipamento)
     frmCadEquipamentoAmbiental.definirVoltar(openCadEquipamento)
     frmCadEquipamentoAmbiental.definirVoltarMenu(openMenu)
     frmCadEquipamentoAmbiental.definirNovoCadastro(openCadEquipamento)

def openCadPesquisaMaterial():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openCadMaterial)
     frmPesquisa.definirOnSelecionar(openCadMaterial)
     frmPesquisa.definirPesquisar(pesquisarMaterial)
     frmPesquisa.definirValidadorExclusao(validarExclusaoMaterial)

def pesquisarMaterial(materialNome):
     return session.query(Material).filter(Material.nome.like("%" + materialNome + "%")).all()


def openCadPesquisaEquipamento():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openCadEquipamento)
     frmPesquisa.definirOnSelecionar(openCadEquipamento)
     frmPesquisa.definirPesquisar(pesquisarEquipamento)
     frmPesquisa.definirValidadorExclusao(validarExclusaoEquipamento)

def pesquisarEquipamento(EquipamentoNome):
     return session.query(Equipamento).filter(Equipamento.nome.like("%" + EquipamentoNome + "%")).all()

def listarItemEquipamento(listView, item):
     listView.insert(END, item.nome)


def openCadPesquisaMaoDeObra():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openCadMaoDeObra)
     frmPesquisa.definirOnSelecionar(openCadMaoDeObra)
     frmPesquisa.definirPesquisar(pesquisarMaoDeObra)
     frmPesquisa.definirValidadorExclusao(validarExclusaoMaoDeObra)

def pesquisarMaoDeObra(maoDeObraFuncao):
     return session.query(MaoObra).filter(MaoObra.nome.like("%" + maoDeObraFuncao + "%")).all()

def listarItemMaoDeObra(listView, item):
     listView.insert(END, item.nome)

def openCadCusto(custo=None):
    frmCadCusto = FrameCadCusto(root, session=session, custo=custo)
    frmCadCusto.definirVoltarMenu(openMenu)
    frmCadCusto.definirPesquisar(openCadPesquisaCusto)


def openCadPesquisaCusto():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openCadCusto)
     frmPesquisa.definirOnSelecionar(openCadCusto)
     frmPesquisa.definirPesquisar(pesquisarCusto)
     frmPesquisa.definirListarItem(listarItemCusto)


def pesquisarCusto(custoPesquisado):
     texto = custoPesquisado
     result = session.query(Custo).join(Equipamento).filter(Equipamento.nome.like("%" + texto + "%")).all()
     result += session.query(Custo).join(MaoObra).filter(MaoObra.nome.like("%" + texto + "%")).all()
     result += session.query(Custo).join(Material).filter(Material.nome.like("%" + texto + "%")).all()
     return result

def listarItemCusto(listView, item):
     itemClasse = None
     if item.maoDeObra_id != None:
        itemClasse = session.query(MaoObra).filter(MaoObra.id == item.maoDeObra_id).first()
     elif item.equipamento_id != None:
        itemClasse = session.query(Equipamento).filter(Equipamento.id == item.equipamento_id).first()
     elif item.material_id != None:
        itemClasse = session.query(Material).filter(Material.id == item.material_id).first()


     listView.insert(END, item.estado + " " + itemClasse.nome)
###########################################3

def openCadAtividade(atividade=None):
    frmCadAtividade = FrameCadAtividade(root, session=session, atividade=atividade)
    frmCadAtividade.definirVoltarMenu(openMenu)
    frmCadAtividade.definirPesquisar(openCadPesquisaAtividade)
    frmCadAtividade.definirNovaAtividadeClick(openCadAtividade)

def openCadPesquisaAtividade():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openCadAtividade)
     frmPesquisa.definirOnSelecionar(openCadAtividade)
     frmPesquisa.definirPesquisar(pesquisarAtividade)
     frmPesquisa.definirValidadorExclusao(validarExclusaoAtividade)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosAtividade)

def pesquisarAtividade(AtividadePesquisada):
     return session.query(Atividade).filter(Atividade.nome.like("%" + AtividadePesquisada + "%")).all()
##################################

def openCadSistemaConstrutivoIndicadoresEconomicos(sistemaConstrutivo=None):
    frmCadSistemaConstrutivo = FrameCadSistemaConstrutivoIndicadoresEconomicos(root, session=session, sistemaConstrutivo=sistemaConstrutivo)
    frmCadSistemaConstrutivo.definirVoltarMenu(openMenu)
    frmCadSistemaConstrutivo.definirPesquisar(openCadPesquisaSistemaConstrutivo)
    frmCadSistemaConstrutivo.definirTelaSeguinteCadSistemaConstrutivo(openCadSistemaConstrutivoIndicadoresSociais)

def openCadSistemaConstrutivoAtividades(sistemaConstrutivo=None):
    frmCadSistemaConstrutivo = FrameCadSistemaConstrutivo(root, session=session, sistemaConstrutivo=sistemaConstrutivo)
    frmCadSistemaConstrutivo.definirVoltarMenu(openMenu)
    frmCadSistemaConstrutivo.definirVoltar(openCadSistemaConstrutivoIndicadoresTecnicos)

def openCadPesquisaSistemaConstrutivo():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openCadSistemaConstrutivoIndicadoresEconomicos)
     frmPesquisa.definirOnSelecionar(openCadSistemaConstrutivoIndicadoresEconomicos)
     frmPesquisa.definirPesquisar(pesquisarSistemaConstrutivo)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosSistemaConstrutivo)
     frmPesquisa.definirValidadorExclusao(validarExclusaoSistemaConstrutivo)

def pesquisarSistemaConstrutivo(sistemaConstrutivoPesquisado):
     return session.query(SistemaConstrutivo).\
          filter(SistemaConstrutivo.nome.like("%" + sistemaConstrutivoPesquisado + "%")).all()

def openCadSistemaConstrutivoIndicadoresSociais(sistemaConstrutivo=None):
    frmCadSistemaConstrutivo = FrameCadSistemaConstrutivoIndicadoresSociais(root, session=session, sistemaConstrutivo=sistemaConstrutivo)
    frmCadSistemaConstrutivo.definirVoltarMenu(openCadSistemaConstrutivoIndicadoresEconomicos)
    frmCadSistemaConstrutivo.definirTelaSeguinteCadSistemaConstrutivo(openCadSistemaConstrutivoIndicadoresTecnicos)

def openCadSistemaConstrutivoIndicadoresTecnicos(sistemaConstrutivo=None):
    frmCadSistemaConstrutivo = FrameCadSistemaConstrutivoIndicadoresTecnicos(root, session=session, sistemaConstrutivo=sistemaConstrutivo)
    frmCadSistemaConstrutivo.definirVoltarMenu(openCadSistemaConstrutivoIndicadoresSociais)
    frmCadSistemaConstrutivo.definirTelaSeguinteCadSistemaConstrutivo(openCadSistemaConstrutivoAtividades)

##################################33

def openIndicadoresAmbientais(atividade=None):
    frmIndicadoresAmbientais = FrameIndicadoresAmbientais(root, session=session, atividade=atividade)
    frmIndicadoresAmbientais.definirVoltarMenu(openMenu)
    frmIndicadoresAmbientais.definirPesquisar(openCadPesquisaAtividadeIndicadorAmbiental)

def openCadPesquisaAtividadeIndicadorAmbiental():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openIndicadoresAmbientais)
     frmPesquisa.definirOnSelecionar(openIndicadoresAmbientais)
     frmPesquisa.definirPesquisar(pesquisarAtividade)
     frmPesquisa.definirValidadorExclusao(validarExclusaoAtividade)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosAtividade)
##################################


def openIndicadoresEconomicos(atividade=None):
    frmIndicadoresEconomicos = FrameIndicadoresEconomicos(root, session=session, atividade=atividade)
    frmIndicadoresEconomicos.definirVoltarMenu(openMenu)
    frmIndicadoresEconomicos.definirPesquisar(openCadPesquisaAtividadeIndicadorEconomico)

def openCadPesquisaAtividadeIndicadorEconomico():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openIndicadoresEconomicos)
     frmPesquisa.definirOnSelecionar(openIndicadoresEconomicos)
     frmPesquisa.definirPesquisar(pesquisarAtividade)
     frmPesquisa.definirValidadorExclusao(validarExclusaoAtividade)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosAtividade)

##################################

def openCadProjeto(cadProjetoDTO:CadProjetoDTO=None):
     frmCadProjeto = FrameCadProjeto(root, session=session, cadProjetoDTO=cadProjetoDTO)
     frmCadProjeto.definirVoltarMenu(openMenu)
     frmCadProjeto.definirPesquisar(openCadPesquisaProjeto)
     frmCadProjeto.definirProximaTela(openCadProjetoSelecionarSistema)

def openCadProjetoSelecionarSistema(cadProjetoDTO:CadProjetoDTO=None):
     frmCadProjetoSelecionarSistema = FrameCadProjetoSelecionarSistemas(root, session=session, cadProjetoDTO=cadProjetoDTO)
     frmCadProjetoSelecionarSistema.definirVoltar(openCadProjeto)
     frmCadProjetoSelecionarSistema.definirProximaTela(openCadProjetoCamparaSistemas)

def openCadProjetoCamparaSistemas(cadProjetoDTO:CadProjetoDTO=None):
     frmCadProjetoComparaSistemas = FrameCadProjetoComparaSistemas(root, session=session, cadProjetoDTO=cadProjetoDTO)
     frmCadProjetoComparaSistemas.definirVoltar(openCadProjetoSelecionarSistema)
     frmCadProjetoComparaSistemas.setOpenMenu(openMenu)

def openCadPesquisaProjeto():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openCadProjeto)
     frmPesquisa.definirOnSelecionar(openCadProjeto)
     frmPesquisa.definirPesquisar(pesquisarProjeto)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosProjeto)
     frmPesquisa.DefinirPreparadorRetorno(preparadorRetornoPesquisaProjeto)

def pesquisarProjeto(projetoPesquisado):
     return session.query(Projeto).filter(Projeto.nome.like("%" + projetoPesquisado + "%")).all()

def preparadorRetornoPesquisaProjeto(projeto: Projeto):
     projetoController = ProjetoController(session)
     projetoSistemaController = ProjetoSistemaConstrutivoController(session)
     sistemaAtividadeController = SistemaConstrutivoAtividadeController(session)
     insumoTransporteController = InsumoTransporteController(session)

     cadProjetoDTO = projetoController.getCadProjetoDTO(projeto)
     cadProjetoDTO.projetoSistemaConstrutivoList = projetoSistemaController.getSistemasByProjetoId(projeto.id)
     cadProjetoDTO.sistemaAtividadeSalubridadeSeguridadeList = sistemaAtividadeController.getAtividadesByProjetoId(projeto.id)
     cadProjetoDTO.meioTransporteInsumoEquipamentoList = insumoTransporteController.\
          getEquipamentoTransporteByProjetoId(projeto.id, projeto.qtdSistemaConstrutivo)
     cadProjetoDTO.meioTransporteInsumoMaterialList = insumoTransporteController.\
          getMaterialTransporteByProjetoId(projeto.id, projeto.qtdSistemaConstrutivo)

     return cadProjetoDTO


 ##################################

def openProjetoResultado(cadProjetoDTO:CadProjetoDTO=None):
     frmProjetoResultado = FrameSistemaConstrutivoProjetoResultado(root, session=session, cadProjetoDTO=cadProjetoDTO)
     frmProjetoResultado.definirTelaMenu(openMenu)
     frmProjetoResultado.definirVoltar(openMenu)
     frmProjetoResultado.definirPesquisar(openCadPesquisaProjetoResultados)


def openCadPesquisaProjetoResultados():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openProjetoResultado)
     frmPesquisa.definirOnSelecionar(openProjetoResultado)
     frmPesquisa.definirPesquisar(pesquisarProjeto)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosProjeto)
     frmPesquisa.DefinirPreparadorRetorno(preparadorRetornoPesquisaProjeto)


##########################################

def openCadFatoresDecisao():
     frmCadFatoresDecisao = FrameFatoresDecisao(root, session=session)
     frmCadFatoresDecisao.definirVoltarMenu(openMenu)

 ##################################

def openCategoriasPonderadas(cadProjetoDTO:CadProjetoDTO=None):
     frmProjetoResultado = FrameCategoriasPonderadas(root, session=session, cadProjetoDTO=cadProjetoDTO)
     frmProjetoResultado.definirTelaMenu(openMenu)
     frmProjetoResultado.definirVoltar(openMenu)
     frmProjetoResultado.definirPesquisar(openCadPesquisaProjetoResultadosComPesos)
     frmProjetoResultado.definirOpenGraficos(openGraficosCategoriasPonderadas)


def openCadPesquisaProjetoResultadosComPesos():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openCategoriasPonderadas)
     frmPesquisa.definirOnSelecionar(openCategoriasPonderadas)
     frmPesquisa.definirPesquisar(pesquisarProjeto)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosProjeto)
     frmPesquisa.DefinirPreparadorRetorno(preparadorRetornoPesquisaProjeto)
     
def openGraficosCategoriasPonderadas(cadProjetoDTO:CadProjetoDTO=None):
     frmGraficosCategoriasPonderadas = FrameGraficosCategoriasPonderadas(root, session=session, cadProjetoDTO=cadProjetoDTO)
     frmGraficosCategoriasPonderadas.definirTelaMenu(openMenu)
     frmGraficosCategoriasPonderadas.definirVoltar(openCategoriasPonderadas)
     frmGraficosCategoriasPonderadas.definirPesquisar(pesquisarProjeto)


##########################################


def openParecerMelhorSistema(cadProjetoDTO:CadProjetoDTO=None):
     frmParecerMelhorSistema = FrameParecerMelhorSistema(root, session=session, cadProjetoDTO=cadProjetoDTO)
     frmParecerMelhorSistema.definirTelaMenu(openMenu)
     frmParecerMelhorSistema.definirVoltar(openMenu)
     frmParecerMelhorSistema.definirPesquisar(openCadPesquisaProjetoParecerMelhorSistema)


def openCadPesquisaProjetoParecerMelhorSistema():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openParecerMelhorSistema)
     frmPesquisa.definirOnSelecionar(openParecerMelhorSistema)
     frmPesquisa.definirPesquisar(pesquisarProjeto)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosProjeto)
     frmPesquisa.DefinirPreparadorRetorno(preparadorRetornoPesquisaProjeto)


##########################################

def openAnaliseDeTransporte(cadProjetoDTO:CadProjetoDTO=None):
     frmAnaliseDeTransporte = FrameAnaliseDeTransporte(root, session=session, cadProjetoDTO=cadProjetoDTO)
     frmAnaliseDeTransporte.definirTelaMenu(openMenu)
     frmAnaliseDeTransporte.definirPesquisar(openCadPesquisaProjetoAnaliseTransporte)


def openCadPesquisaProjetoAnaliseTransporte():
     frmPesquisa = FramePesquisa(root, session=session)
     frmPesquisa.definirCancelar(openAnaliseDeTransporte)
     frmPesquisa.definirOnSelecionar(openAnaliseDeTransporte)
     frmPesquisa.definirPesquisar(pesquisarProjeto)
     frmPesquisa.definirExlusaoFilhos(excluirFilhosProjeto)
     frmPesquisa.DefinirPreparadorRetorno(preparadorRetornoPesquisaProjeto)


##########################################

#openTelaLogin()
openMenu()


root.mainloop()
