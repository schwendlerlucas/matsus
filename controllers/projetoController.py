from controllers.controllerBase import ControllerBase
from models.projetoSistemaConstrutivoAtividadeTransporte import ProjetoSistemaConstrutivoAtividadeTransporte
from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo
from models.projetoSistemaConstrutivoAtividade import ProjetoSistemaConstrutivoAtividade

from DTOs.cadProjetoDto import CadProjetoDTO

class ProjetoController(ControllerBase):


    def getAtividadesSistemaProjeto(self, sistema_id, projeto_id):
        atividadeList = self.session.query(ProjetoSistemaConstrutivoAtividadeTransporte).\
            filter(ProjetoSistemaConstrutivoAtividadeTransporte.sistemaConstrutivo_id==sistema_id, ProjetoSistemaConstrutivoAtividadeTransporte.projeto_id==projeto_id).\
                order_by(ProjetoSistemaConstrutivoAtividadeTransporte.atividade.id).all()

        if len(atividadeList) > 0:
            return atividadeList
        
        return self.session.query(SistemaConstrutivoAtividade).\
            filter(SistemaConstrutivoAtividade.sistemaConstrutivo_id==sistema_id).all()

    def saveProjeto(self, cadProjetoDTO: CadProjetoDTO):
        if cadProjetoDTO.projeto.id != None:
            self.deleteChildRecords(cadProjetoDTO.projeto.id)
        else:
            self.session.add(cadProjetoDTO.projeto)
            self.session.commit()

        for sistema in cadProjetoDTO.projetoSistemaConstrutivoList:
            newSistema = ProjetoSistemaConstrutivo()
            newSistema.projeto_id = cadProjetoDTO.projeto.id
            newSistema.sistemaConstrutivo_id = sistema.sistemaConstrutivo_id
            self.session.add(newSistema)

        for sistema in cadProjetoDTO.sistemaAtividadeSalubridadeSeguridadeList:
            for atividade in sistema.atividadeList:
                newAtividade = ProjetoSistemaConstrutivoAtividade()
                newAtividade.projeto_id = cadProjetoDTO.projeto.id
                newAtividade.sistemaConstrutivo_id = sistema.sistema_id
                newAtividade.atividade_id = atividade.atividade_id
                newAtividade.grauSalubridade = atividade.grauSalubridade
                newAtividade.grauSeguridade = atividade.grauSeguridade

                self.session.add(newAtividade) 


        for sistema in cadProjetoDTO.meioTransporteInsumoEquipamentoList :
            for equipamento in sistema.insumosList:
                newEquipamento = ProjetoSistemaConstrutivoAtividadeTransporte()
                newEquipamento.projeto_id = cadProjetoDTO.projeto.id
                newEquipamento.sistemaConstrutivo_id = sistema.sistema_id
                newEquipamento.atividade_id = equipamento.atividade_id
                newEquipamento.equipamento_id = equipamento.insumo_id
                newEquipamento.meioTransporte_id = equipamento.meio_transporte_id
                newEquipamento.distancia1 = equipamento.distancia1
                newEquipamento.distancia2 = equipamento.distancia2
                newEquipamento.valorFrete = equipamento.valorFrete

                self.session.add(newEquipamento)    


        for sistema in cadProjetoDTO.meioTransporteInsumoMaterialList:
            for material in sistema.insumosList:
                newmaterial = ProjetoSistemaConstrutivoAtividadeTransporte()
                newmaterial.projeto_id = cadProjetoDTO.projeto.id
                newmaterial.sistemaConstrutivo_id = sistema.sistema_id
                newmaterial.atividade_id = material.atividade_id
                newmaterial.material_id = material.insumo_id
                newmaterial.meioTransporte_id = material.meio_transporte_id
                newmaterial.distancia1 = material.distancia1
                newmaterial.distancia2 = material.distancia2
                newmaterial.valorFrete = material.valorFrete

                self.session.add(newmaterial)  


    def deleteChildRecords(self, projetoId: int):
        rows = self.session.query(ProjetoSistemaConstrutivo).filter(ProjetoSistemaConstrutivo.projeto_id==projetoId).all()
        for row in rows:
            self.session.delete(row)

        rows = self.session.query(ProjetoSistemaConstrutivoAtividade).filter(ProjetoSistemaConstrutivoAtividade.projeto_id==projetoId).all()
        for row in rows:
            self.session.delete(row)

        rows = self.session.query(ProjetoSistemaConstrutivoAtividadeTransporte).\
            filter(ProjetoSistemaConstrutivoAtividadeTransporte.projeto_id==projetoId).all()
        for row in rows:
            self.session.delete(row)

    def getCadProjetoDTO(self, projeto):
        cadProjetoDTO = CadProjetoDTO()
        cadProjetoDTO.projeto = projeto


        return cadProjetoDTO

        
