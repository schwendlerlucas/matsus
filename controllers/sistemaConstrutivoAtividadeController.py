from sqlalchemy.orm import Session

from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade
from models.projetoSistemaConstrutivoAtividade import ProjetoSistemaConstrutivoAtividade
from controllers.controllerBase import ControllerBase

from DTOs.atividadesSeguridadeSalubridadeDTO import AtividadesSeguridadeSalubridadeDTO
from DTOs.atividadeSeguridadeSalubridadeSistemaDTO import AtividadeSeguridadeSalubridadeSistemaDTO

class SistemaConstrutivoAtividadeController(ControllerBase):

    
    def getAtividadesByIdSistemaContrutivo(self, idSistemaContrutivo: int):
        atividadeList = self.session.query(SistemaConstrutivoAtividade).\
            filter(SistemaConstrutivoAtividade.sistemaConstrutivo_id==idSistemaContrutivo).all()
        
        atividadeDTOList = AtividadeSeguridadeSalubridadeSistemaDTO()
        atividadeDTOList.sistema_id = idSistemaContrutivo
        for item in atividadeList:
            atividadeSalubridadeSeguridade = AtividadesSeguridadeSalubridadeDTO()
            atividadeSalubridadeSeguridade.atividade_id = item.atividade.id
            atividadeSalubridadeSeguridade.atividade_nome = item.atividade.nome
            atividadeSalubridadeSeguridade.grauSalubridade = item.atividade.grauSalubridade
            atividadeSalubridadeSeguridade.grauSeguridade = item.atividade.grauSeguridade
            atividadeSalubridadeSeguridade.quantidade = item.quantidade

            atividadeDTOList.atividadeList.append(atividadeSalubridadeSeguridade)

        
        return atividadeDTOList

    def getAtividadesByProjetoId(self, projeto_id: int):
        sistemaAtividadeList = self.session.query(ProjetoSistemaConstrutivoAtividade).filter(ProjetoSistemaConstrutivoAtividade.projeto_id==projeto_id).\
            order_by(ProjetoSistemaConstrutivoAtividade.sistemaConstrutivo_id).all()
    
        sistemaAtividadeSalubridadeSeguridadeList = []
        lastSistema = -9999
        for sistemaAtividade in sistemaAtividadeList:
            if lastSistema != sistemaAtividade.sistemaConstrutivo_id:
                newSistema = AtividadeSeguridadeSalubridadeSistemaDTO()
                lastSistema = sistemaAtividade.sistemaConstrutivo_id
                newSistema.sistema_id = lastSistema
                sistemaAtividadeSalubridadeSeguridadeList.append(newSistema)
            
            newAtividade = AtividadesSeguridadeSalubridadeDTO()
            newSistema.atividadeList.append(newAtividade)

            newAtividade.atividade_id = sistemaAtividade.atividade_id
            newAtividade.atividade_nome = sistemaAtividade.atividade.nome
            newAtividade.grauSalubridade = sistemaAtividade.grauSalubridade
            newAtividade.grauSeguridade = sistemaAtividade.grauSeguridade
            
            sistemaAtividade = self.session.query(SistemaConstrutivoAtividade).\
                filter(SistemaConstrutivoAtividade.sistemaConstrutivo_id==lastSistema, 
                       SistemaConstrutivoAtividade.atividade_id==sistemaAtividade.atividade_id).first()
            if sistemaAtividade != None:
                newAtividade.quantidade = sistemaAtividade.quantidade

        return sistemaAtividadeSalubridadeSeguridadeList
        


    def getAvgSalubridadeSeguridadeAndSumQuantidade(self, listaSistemaAtividade):
        salubridadeAvg = 0.0
        seguridadeAvg = 0.0
        quantidadeSum = 0.0

        for item in listaSistemaAtividade:
            salubridadeAvg += item.grauSalubridade
            seguridadeAvg += item.grauSeguridade
            quantidadeSum += item.quantidade


        if len(listaSistemaAtividade) > 0:
            salubridadeAvg /= len(listaSistemaAtividade)
            seguridadeAvg /= len(listaSistemaAtividade)
        
        return salubridadeAvg, seguridadeAvg, quantidadeSum


        