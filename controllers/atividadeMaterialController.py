from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade
from controllers.controllerBase import ControllerBase
from DTOs.meioTransporteInsumosDTO import MeioTransporteInsumosDTO
from models.atividadeMaterial import AtividadeMaterial
from interfaces.validacoesCampos import *

class AtividadeMaterialtoController(ControllerBase):

    def getMaterialAtividadeList(self, sistema_id: int, quantidadeSistemaContrutivo: float):
        atividadeList = self.session.query(SistemaConstrutivoAtividade).\
            filter(SistemaConstrutivoAtividade.sistemaConstrutivo_id==sistema_id).all()
        
        meioTransporteList = []

        for atividade in atividadeList:
            materialList = self.session.query(AtividadeMaterial).\
                filter(AtividadeMaterial.atividade_id==atividade.atividade_id).all()
            for material in materialList:
                newMeioTransporteInsumo = MeioTransporteInsumosDTO()
                newMeioTransporteInsumo.atividade_id = atividade.atividade.id
                newMeioTransporteInsumo.atividade_nome = atividade.atividade.nome
                newMeioTransporteInsumo.insumo_id = material.material.id
                newMeioTransporteInsumo.insumo_nome = material.material.nome
                newMeioTransporteInsumo.quantidade_unitaria = retornarZeroSeNoneOuVazio(material.quantidade)
                newMeioTransporteInsumo.quantidade_total = float(newMeioTransporteInsumo.quantidade_unitaria) * float(quantidadeSistemaContrutivo)
                newMeioTransporteInsumo.peso_unitario = 0 #retornarZeroSeNoneOuVazio(material.material.peso)
                newMeioTransporteInsumo.peso_total = float(newMeioTransporteInsumo.peso_unitario) * float(quantidadeSistemaContrutivo)
                newMeioTransporteInsumo.unidade = material.unidade
                meioTransporteList.append(newMeioTransporteInsumo)

        return meioTransporteList


          
    
