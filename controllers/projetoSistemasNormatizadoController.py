from sqlalchemy.orm import Session
from controllers.controllerBase import ControllerBase

from DTOs.resultadosSistemaConstrutivoDTO import ResultadosSistemaContrutivoDTO

from controllers.sistemaConstrutivoResultadoAmbientalController import SistemaConstrutivoResultadoAmbientalController
from controllers.sistemaConstrutivoResultadoController import SistemaConstrutivoResultadoController
from controllers.projetoSistemaConstrutivoController import ProjetoSistemaConstrutivoController

from models.pesos import Pesos

from interfaces.validacoesCampos import retornarZeroSeNoneOuVazio
from DTOs.categoriasNormatizadasDTO import CategoriasNormatizadasDTO


class ProjetoSistemasNormatizadoController(ControllerBase):

    def __init__(self, session, projeto_id: int, estado:str):
        super(ProjetoSistemasNormatizadoController, self).__init__(session=session)

        projetoSistemaController = ProjetoSistemaConstrutivoController(session)
        self.__estado = estado
        self.__projeto_id = projeto_id
        self.__sistemasList = projetoSistemaController.getSistemasByProjetoId(self.__projeto_id)
        self.__listaResultadosSistemas = []
        self.__listaSistemaNormatizado = []
        self.__sistemaSendoNormatizado = None

        self.__pesos = Pesos()


    def GetListaResultados(self):
        return self.__listaResultadosSistemas

    def GetListaResultadosNormatizados(self):
        return self.__listaSistemaNormatizado

    def Calc(self):
        self.__carregarListaResultados()
        self.__inicializaListaSistemasNormatizado()
        self.__calcAI()
        self.__calcAII()
        self.__calcAIII()
        self.__calcAIV()

        self.__calcSI()
        self.__calcSII()
        self.__calcSIII()
        self.__calcSIV()

        self.__calcEI()
        self.__calcEII()

        self.__calcTI()
        self.__calcTII()
        self.__calcTIII()
        self.__calcTIV()
        self.__calcTV()     

        
    def __calcAI(self): 
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.GWP, self.setAIValue, lambda i: self.__listaResultadosSistemas[i].GWPTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.ODP, self.setAIValue, lambda i: self.__listaResultadosSistemas[i].ODPTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.AP, self.setAIValue, lambda i: self.__listaResultadosSistemas[i].APTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.POCP, self.setAIValue, lambda i: self.__listaResultadosSistemas[i].POCPTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.EP, self.setAIValue, lambda i: self.__listaResultadosSistemas[i].EPTotal)

        listaAI = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].AI)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaAI)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].AI = listaNormatizada[i]

    def __calcAII(self): 
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.ResidPerigoso, self.setAIIValue, lambda i: self.__listaResultadosSistemas[i].ResidPerigosoTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.ResidRadioativo, self.setAIIValue, lambda i: self.__listaResultadosSistemas[i].ResidRadioativoTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.ResidNaoPerigoso, self.setAIIValue, lambda i: self.__listaResultadosSistemas[i].ResidNaoPerigosoTotal)
        
        listaAII = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].AII)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaAII)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].AII = listaNormatizada[i]


    def __calcAIII(self): 
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.EnergiaNaoRenovavel, self.setAIIIValue, lambda i: self.__listaResultadosSistemas[i].EnergiaNaoRenovavelTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.EnergiaRenovavel, self.setAIIIValue, lambda i: self.__listaResultadosSistemas[i].EnergiaRenovavelTotal)
        
        listaAIII = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].AIII)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaAIII)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].AIII = listaNormatizada[i]


    def __calcAIV(self): 
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.AguaRedeAbastesimento, self.setAIVValue, lambda i: self.__listaResultadosSistemas[i].AguaRedeAbastesimentoTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.AguaReuso, self.setAIVValue, lambda i: self.__listaResultadosSistemas[i].AguaReusoTotal)
        
        listaAIV = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].AIV)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaAIV)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].AIV = listaNormatizada[i]

    def __calcSI(self): 
        self.__setSumIndicadorNormatizado(self.__pesos.GrauPopularizacaoConceitos, self.setSIValue, lambda i: self.__listaResultadosSistemas[i].GrauPopularizacaoConceitosTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.GrauFomentoInstituicao, self.setSIValue, lambda i: self.__listaResultadosSistemas[i].GrauFomentoInstituicaoTotal)
        
        listaSI = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].SI)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaSI)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].SI = listaNormatizada[i]

    def __calcSII(self): 
        self.__setSumIndicadorNormatizado(self.__pesos.GrauMatCulturalmenteUsados, self.setSIIValue, lambda i: self.__listaResultadosSistemas[i].GrauMatCulturalmenteUsadosTotal)
        
        listaSII = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].SII)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaSII)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].SII = listaNormatizada[i]


    def __calcSIII(self): 
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.GrauSalubridade , self.setSIIIValue, lambda i: self.__listaResultadosSistemas[i].GrauSalubridadeTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.GrauSeguridade, self.setSIIIValue, lambda i: self.__listaResultadosSistemas[i].GrauSeguridadeTotal)
        
        listaSIII = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].SIII)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaSIII)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].SIII = listaNormatizada[i]


    def __calcSIV(self): 
        self.__setSumIndicadorNormatizado(self.__pesos.GrauContribNaEdificacao , self.setSIVValue, lambda i: self.__listaResultadosSistemas[i].GrauContribNaEdificacaoTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.GrauComplexidade, self.setSIVValue, lambda i: self.__listaResultadosSistemas[i].GrauComplexidadeTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.PossibilidadeMutiroes , self.setSIVValue, lambda i: self.__listaResultadosSistemas[i].PossibilidadeMutiroesTotal)
        
        listaSIV = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].SIV)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaSIV)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].SIV = listaNormatizada[i]


    def __calcEI(self): 
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.Custo, self.setEIValue, lambda i: self.__listaResultadosSistemas[i].CustoTotal)
        
        listaEI = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].EI)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaEI)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].EI = listaNormatizada[i]

    def __calcEII(self): 
        self.__setSumIndicadorNormatizado(self.__pesos.AquisiMateriaPrimaLocal , self.setEIIValue, lambda i: self.__listaResultadosSistemas[i].AquisiMateriaPrimaLocalTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.MaoObraLocal, self.setEIIValue, lambda i: self.__listaResultadosSistemas[i].MaoObraLocalTotal)
        
        listaEII = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].EII)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaEII)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].EII = listaNormatizada[i]

    def __calcTI(self): 
        self.__setSumIndicadorNormatizado(self.__pesos.ParcelaPodeSerReciclada , self.setTIValue, lambda i: self.__listaResultadosSistemas[i].ParcelaPodeSerRecicladaTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.ParcelaPodeSerReaproveitada, self.setTIValue, lambda i: self.__listaResultadosSistemas[i].ParcelaPodeSerReaproveitadaTotal)
        
        listaTI = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].TI)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaTI)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].TI = listaNormatizada[i]


    def __calcTII(self): 
        self.__setSumIndicadorNormatizado(self.__pesos.GrauEstanqueidade , self.setTIIValue, lambda i: self.__listaResultadosSistemas[i].GrauEstanqueidadeTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.GrauTransmitanciaTermica, self.setTIIValue, lambda i: self.__listaResultadosSistemas[i].GrauTransmitanciaTermicaTotal)
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.GrauTransmissaoOndas, self.setTIIValue, lambda i: self.__listaResultadosSistemas[i].GrauTransmissaoOndasTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.ComportMecanico, self.setTIIValue, lambda i: self.__listaResultadosSistemas[i].ComportMecanicoTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.Durabilidade, self.setTIIValue, lambda i: self.__listaResultadosSistemas[i].DurabilidadeTotal)

        listaTII = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].TII)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaTII)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].TII = listaNormatizada[i]

    def __calcTIII(self): 
        self.__setSumIndicadorNormatizado(self.__pesos.AcompanhamentoProfissionais, self.setTIIIValue, lambda i: self.__listaResultadosSistemas[i].AcompanhamentoProfissionaisTotal)

        listaTIII = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].TIII)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaTIII)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].TIII = listaNormatizada[i]

    def __calcTIV(self): 
        self.__setSumIndicadorHarmonizadoENormatizado(self.__pesos.GrauPericibilidade, self.setTIVValue, lambda i: self.__listaResultadosSistemas[i].GrauPericibilidadeTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.FacilidadeEstocagem, self.setTIVValue, lambda i: self.__listaResultadosSistemas[i].FacilidadeEstocagemTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.FacilidadeTransporte, self.setTIVValue, lambda i: self.__listaResultadosSistemas[i].FacilidadeTransporteTotal)

        listaTIV = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].TIV)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaTIV)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].TIV = listaNormatizada[i]

    def __calcTV(self): 
        self.__setSumIndicadorNormatizado(self.__pesos.FacilidadeManutencao, self.setTVValue, lambda i: self.__listaResultadosSistemas[i].FacilidadeManutencaoTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.FacilidadeReparo, self.setTVValue, lambda i: self.__listaResultadosSistemas[i].FacilidadeReparoTotal)
        self.__setSumIndicadorNormatizado(self.__pesos.PadronizacaoReplicacao, self.setTVValue, lambda i: self.__listaResultadosSistemas[i].PadronizacaoReplicacaoTotal)
        #self.__setSumIndicadorNormatizado(self.__pesos.PadronizacaoReplicacao, self.setTVValue, lambda i: self.__listaResultadosSistemas[i])

        listaTV = self.__getValuesList(self.__listaSistemaNormatizado, lambda i : self.__listaSistemaNormatizado[i].TV)
        
        listaNormatizada = self.__getIndicadorNormalizados(listaTV)
        for i in range(0, len(self.__listaSistemaNormatizado)):
            self.__listaSistemaNormatizado[i].TV = listaNormatizada[i]


    def __setSumIndicadorNormatizado(self, peso, setValue, getValue):
        lista = self.__getValuesList(self.__listaResultadosSistemas, getValue)
        listaValoresNormalizados = self.__getIndicadorNormalizados(lista)
        
        for i in range(0, len(listaValoresNormalizados)):
            setValue(i, listaValoresNormalizados[i] * (peso))

    def __setSumIndicadorHarmonizadoENormatizado(self, peso, setValue, getValue):
        lista = self.__getValuesList(self.__listaResultadosSistemas, getValue)
        listaValoresHarmonizados = self.__getIndicadorHarmonizadoENormalizado(lista)
        
        for i in range(0, len(listaValoresHarmonizados)):
            setValue(i, listaValoresHarmonizados[i] * (peso))
        

    def __preparePercentual(self, valor):
        try:
            return float(valor)/100
        except:
            return 1

    def __getIndicadorHarmonizadoENormalizado(self, valuesList):
        valuesHarmonizados = self.__getIndicadoresHarmonizados(valuesList)
        return self.__getIndicadorNormalizados(valuesHarmonizados)


    def __getIndicadorNormalizados(self, valuesList):
        soma = self.__sumList(valuesList)
        
        valuesNormalizados = []
        for item in valuesList:
            if item == 0:
                valuesNormalizados.append(0)
                continue    
            normalizado = item / soma
            valuesNormalizados.append(normalizado)

        return valuesNormalizados


    def __getIndicadoresHarmonizados(self, valuesList):
        soma = self.__sumList(valuesList)

        valuesHarmonizados = []
        for item in valuesList:
            if item == 0:
                valuesHarmonizados.append(0)
                continue    
            harmonizado = soma/ item
            valuesHarmonizados.append(harmonizado)

        return valuesHarmonizados

    def __sumList(self, values):
        result = 0.0

        for item in values:
            result += item

        return result

    def __inicializaListaSistemasNormatizado(self):
        self.__listaSistemaNormatizado = []
        for sistema in self.__listaResultadosSistemas:
            self.__listaSistemaNormatizado.append(CategoriasNormatizadasDTO())

    def __getValuesList(self, valuesList, getValue):
        result = []

        for i in  range(0, len(valuesList)):
            result.append(getValue(i))

        return result

    def __carregarListaResultados(self):
        self.__listaResultadosSistemas = []
        for sistema in self.__sistemasList:
            controllerAmbiental = SistemaConstrutivoResultadoAmbientalController(self.session)
            resultado = controllerAmbiental.Calc(self.__projeto_id, sistema.sistemaConstrutivo_id, self.__estado)
            resultado.sistema_nome = sistema.sistemaConstrutivo.nome

            controller = SistemaConstrutivoResultadoController(self.session)
            controller.Calc(self.__projeto_id, sistema.sistemaConstrutivo_id, self.__estado, resultado)

            self.__listaResultadosSistemas.append(resultado)


    def setAIValue(self, i, value):
        self.__listaSistemaNormatizado[i].AI += value

    def setAIIValue(self, i, value):
        self.__listaSistemaNormatizado[i].AII += value

    def setAIIIValue(self, i, value):
        self.__listaSistemaNormatizado[i].AIII += value

    def setAIVValue(self, i, value):
        self.__listaSistemaNormatizado[i].AIV += value

    def setSIValue(self, i, value):
        self.__listaSistemaNormatizado[i].SI += value

    def setSIIValue(self, i, value):
        self.__listaSistemaNormatizado[i].SII += value

    def setSIIIValue(self, i, value):
        self.__listaSistemaNormatizado[i].SIII += value

    def setSIVValue(self, i, value):
        self.__listaSistemaNormatizado[i].SIV += value

    def setEIValue(self, i, value):
        self.__listaSistemaNormatizado[i].EI += value

    def setEIIValue(self, i, value):
        self.__listaSistemaNormatizado[i].EII += value


    def setTIValue(self, i, value):
        self.__listaSistemaNormatizado[i].TI += value

    def setTIIValue(self, i, value):
        self.__listaSistemaNormatizado[i].TII += value

    def setTIIIValue(self, i, value):
        self.__listaSistemaNormatizado[i].TIII += value

    def setTIVValue(self, i, value):
        self.__listaSistemaNormatizado[i].TIV += value

    def setTVValue(self, i, value):
        self.__listaSistemaNormatizado[i].TV += value