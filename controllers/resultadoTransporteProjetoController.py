from controllers.controllerBase import ControllerBase
from models.projetoSistemaConstrutivoAtividadeTransporte import ProjetoSistemaConstrutivoAtividadeTransporte
from DTOs.meioTransporteInsumosResultadoDTO import MeioTransporteInsumosResultadoDTO
from interfaces.validacoesCampos import *
from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaterial import AtividadeMaterial


class ResultadoTransporteProjetoController(ControllerBase):

    def queryTransportationResults(self, projeto_id: int, sistema_construtivo_id:int, quantidadeProjeto: float):
        insumos = self.session.query(ProjetoSistemaConstrutivoAtividadeTransporte).\
            filter(ProjetoSistemaConstrutivoAtividadeTransporte.projeto_id==projeto_id, 
            ProjetoSistemaConstrutivoAtividadeTransporte.sistemaConstrutivo_id==sistema_construtivo_id).\
                order_by(ProjetoSistemaConstrutivoAtividadeTransporte.atividade_id).all()

        insumoList = []
        for insumo in insumos:
            insumoResultado = MeioTransporteInsumosResultadoDTO()
            insumoResultado.atividade_nome = insumo.atividade.nome
            
            quantiadeNaAtividade = 1
            peso = 1
            if insumo.equipamento != None:
                insumoResultado.insumo_nome = insumo.equipamento.nome
                peso = retornarZeroSeNoneOuVazio(insumo.equipamento.peso)
                quantiadeNaAtividade = self.__queryQuantidadeEquipamentoNaAtividade(insumo.atividade_id, insumo.equipamento_id)
            elif insumo.material != None:
                insumoResultado.insumo_nome = insumo.material.nome
                quantiadeNaAtividade = self.__queryQuantidadeMaterialNaAtividade (insumo.atividade_id, insumo.material_id)

            insumoResultado.distancia = insumo.distancia1 + insumo.distancia2

            if insumo.meioTransporte != None:
                insumoResultado.meio_transporte_nome = insumo.meioTransporte.nome

                distanciaXQuantidadeXPeso = insumoResultado.distancia * quantidadeProjeto * quantiadeNaAtividade * peso

                insumoResultado.custoTotal = insumo.valorFrete

                insumoResultado.GWP = insumo.meioTransporte.emiEfeitoEstufa
                insumoResultado.GWPTotal = retornarZeroSeNoneOuVazio(insumo.meioTransporte.emiEfeitoEstufa) * distanciaXQuantidadeXPeso
                insumoResultado.ODP = insumo.meioTransporte.emiDegradamOzonio
                insumoResultado.ODPTotal = retornarZeroSeNoneOuVazio(insumo.meioTransporte.emiDegradamOzonio) * distanciaXQuantidadeXPeso
                insumoResultado.AP = insumo.meioTransporte.emiChuvaAcida
                insumoResultado.APTotal = retornarZeroSeNoneOuVazio(insumo.meioTransporte.emiChuvaAcida) * distanciaXQuantidadeXPeso
                insumoResultado.EP = insumo.meioTransporte.emiToxicosPatogenicos
                insumoResultado.EPTotal = retornarZeroSeNoneOuVazio(insumo.meioTransporte.emiToxicosPatogenicos) * distanciaXQuantidadeXPeso
                insumoResultado.POCP = insumo.meioTransporte.emiContribEutroficacao
                insumoResultado.POCPTotal = retornarZeroSeNoneOuVazio(insumo.meioTransporte.emiContribEutroficacao) * distanciaXQuantidadeXPeso

            insumoList.append(insumoResultado)
        
        return insumoList

    def __queryQuantidadeEquipamentoNaAtividade(self, atividade_id:int, equipamento_id:int):
        equipamentoAtividade = self.session.query(AtividadeEquipamento).filter(AtividadeEquipamento.equipamento_id==equipamento_id,
            AtividadeEquipamento.atividade_id==atividade_id).first()

        if equipamentoAtividade == None:
            return 1
        
        return equipamentoAtividade.quantidade


    def __queryQuantidadeMaterialNaAtividade(self, atividade_id:int, material_id:int):
        materialAtividade = self.session.query(AtividadeMaterial).filter(AtividadeMaterial.material_id==material_id,
            AtividadeMaterial.atividade_id==atividade_id).first()

        if materialAtividade == None:
            return 1
        
        return materialAtividade.quantidade

