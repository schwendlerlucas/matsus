from sqlalchemy.orm import Session
from controllers.controllerBase import ControllerBase

from DTOs.resultadosSistemaConstrutivoDTO import ResultadosSistemaContrutivoDTO

from models.projeto import Projeto
from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade
from models.sistemaConstrutivo import SistemaConstrutivo
from models.projetoSistemaConstrutivoAtividade import  ProjetoSistemaConstrutivoAtividade
from models.projetoSistemaConstrutivoAtividadeTransporte import ProjetoSistemaConstrutivoAtividadeTransporte
from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaoDeObra import AtividadeMaoDeObra
from models.atividadeMaterial import AtividadeMaterial
from models.custo import Custo

from interfaces.validacoesCampos import retornarZeroSeNoneOuVazio

from controllers.resultadoTransporteProjetoController import ResultadoTransporteProjetoController

class SistemaConstrutivoResultadoController(ControllerBase):

    def Calc(self, projeto_id:int, sistemaConstrutivo_id:int, estado:str, resultado: ResultadosSistemaContrutivoDTO):
        self.__projeto_id = projeto_id
        self.__sistemaConstrutivo_id = sistemaConstrutivo_id
        self.__resultado = resultado
        
        self.__definirInformaçõesSistemaContrutivo()
        self.__definirSeguridadeSalubridade()

        return self.__resultado

    def __definirSeguridadeSalubridade(self):
        sistemaAtividades = self.session.query(ProjetoSistemaConstrutivoAtividade).filter(
            ProjetoSistemaConstrutivoAtividade.projeto_id == self.__projeto_id,
            ProjetoSistemaConstrutivoAtividade.sistemaConstrutivo_id == self.__sistemaConstrutivo_id).all()

        if len(sistemaAtividades) == 0:
            return

        salubridade = 0.0
        seguridade = 0.0
        for atividade in sistemaAtividades:
            salubridade += atividade.grauSalubridade
            seguridade += atividade.grauSalubridade

        
        salubridade /= len(sistemaAtividades)
        seguridade /= len(sistemaAtividades)

        self.__resultado.GrauSalubridadeTotal = salubridade
        self.__resultado.GrauSeguridadeTotal = seguridade

    def __definirInformaçõesSistemaContrutivo(self):
        sistema = self.session.query(SistemaConstrutivo).filter(SistemaConstrutivo.id == self.__sistemaConstrutivo_id).first()
        self.__resultado.sistema_nome = sistema.nome

        self.__resultado.AquisiMateriaPrimaLocalTotal = retornarZeroSeNoneOuVazio(sistema.econAquisicaoMatLocal)
        self.__resultado.MaoObraLocalTotal = retornarZeroSeNoneOuVazio(sistema.econMaoObraLocal)

        self.__resultado.GrauPopularizacaoConceitosTotal = retornarZeroSeNoneOuVazio(sistema.socGrauPopilarizacaoConce)
        self.__resultado.GrauFomentoInstituicaoTotal = retornarZeroSeNoneOuVazio(sistema.socGrauFomentosInstituicoes)
        self.__resultado.GrauMatCulturalmenteUsadosTotal = retornarZeroSeNoneOuVazio(sistema.socGrauMateriaisCult)
        self.__resultado.GrauContribNaEdificacaoTotal = retornarZeroSeNoneOuVazio(sistema.socGrauContribuicao)
        self.__resultado.GrauComplexidadeTotal = retornarZeroSeNoneOuVazio(sistema.socGrauComplexidade)
        self.__resultado.PossibilidadeMutiroesTotal = retornarZeroSeNoneOuVazio(sistema.socGrauPossibMutiroes)

        self.__resultado.ParcelaPodeSerRecicladaTotal = retornarZeroSeNoneOuVazio(sistema.tecParcelaPodeReclidada)
        self.__resultado.ParcelaPodeSerReaproveitadaTotal = retornarZeroSeNoneOuVazio(sistema.tecParcelaPodeReaproveitada)
        self.__resultado.GrauEstanqueidadeTotal = retornarZeroSeNoneOuVazio(sistema.tecGrauEstanqueidade)
        self.__resultado.GrauTransmitanciaTermicaTotal = retornarZeroSeNoneOuVazio(sistema.tecGrauTransmitanciaTermica)
        self.__resultado.GrauTransmissaoOndasTotal = retornarZeroSeNoneOuVazio(sistema.tecGrauTransmitanciaSonora)
        self.__resultado.ComportMecanicoTotal = retornarZeroSeNoneOuVazio(sistema.tecComportamentoMecanico)
        self.__resultado.DurabilidadeTotal = retornarZeroSeNoneOuVazio(sistema.tecDurabilidade)
        self.__resultado.AcompanhamentoProfissionaisTotal = retornarZeroSeNoneOuVazio(sistema.tecAcompanhamentoTecnico)
        self.__resultado.GrauPericibilidadeTotal = retornarZeroSeNoneOuVazio(sistema.tecGrauPerecibilidade)
        self.__resultado.FacilidadeEstocagemTotal = retornarZeroSeNoneOuVazio(sistema.tecFacilidadeEstocagem)
        self.__resultado.FacilidadeTransporteTotal = retornarZeroSeNoneOuVazio(sistema.tecFacilidadeTransporte)
        self.__resultado.FacilidadeManutencaoTotal = retornarZeroSeNoneOuVazio(sistema.tecFacilidadeManutPeriodica)
        self.__resultado.FacilidadeReparoTotal = retornarZeroSeNoneOuVazio(sistema.tecFacilidadeReparos)
        self.__resultado.PadronizacaoReplicacaoTotal = retornarZeroSeNoneOuVazio(sistema.tecPadrinizacaoReplicacao)
        self.__resultado.VersatilidadeTotal = retornarZeroSeNoneOuVazio(sistema.tecVersatilidade)
