from controllers.controllerBase import ControllerBase
from models.equipamento import Equipamento
class EquipamentoController(ControllerBase):


    def getMeiosDeTransporte(self):
        return self.session.query(Equipamento).filter(Equipamento.ehMeioTransporte==True).order_by(Equipamento.nome).all()

    def getEquipamentoId(self, nome, equipamentolist):
        for equipamento in equipamentolist:
            if equipamento.nome == nome:
                return equipamento.id

        print("Erro ao encontrar ID equipamento - EquipamentoController")
        return -1

    def getEquipamentoNames(self, equipamentoList):
        equipamentoNames = []
        for equipamento in equipamentoList:
            equipamentoNames.append(equipamento.nome)
        
        return equipamentoNames

