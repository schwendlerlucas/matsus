from sqlalchemy.orm import Session
from controllers.controllerBase import ControllerBase

from DTOs.resultadosSistemaConstrutivoDTO import ResultadosSistemaContrutivoDTO

from models.projeto import Projeto
from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade
from models.sistemaConstrutivo import SistemaConstrutivo
from models.projetoSistemaConstrutivoAtividadeTransporte import ProjetoSistemaConstrutivoAtividadeTransporte
from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaoDeObra import AtividadeMaoDeObra
from models.atividadeMaterial import AtividadeMaterial
from models.custo import Custo
from models.atividade import Atividade

from controllers.resultadoTransporteProjetoController import ResultadoTransporteProjetoController

class SistemaConstrutivoResultadoAmbientalController(ControllerBase):

    def Calc(self, projeto_id:int, sistemaConstrutivo_id:int, estado:str):
        projeto = self.session.query(Projeto).filter(Projeto.id == projeto_id).first()
        
        resultado = ResultadosSistemaContrutivoDTO()

        resultado.projeto_nome = projeto.nome
        resultado.quantidade = float(projeto.qtdSistemaConstrutivo)

        sistemaAtividades = self.session.query(SistemaConstrutivoAtividade).filter(
            SistemaConstrutivoAtividade.sistemaConstrutivo_id == sistemaConstrutivo_id).all()

        for atividadeSistema in sistemaAtividades:
            quantidadeDeAtividadeNoSistema = atividadeSistema.quantidade
            self.__definirResultadosEquipamentos(atividadeSistema.atividade_id, estado, resultado, quantidadeDeAtividadeNoSistema)
            self.__definirResultadosMaterial(atividadeSistema.atividade_id, estado, resultado, quantidadeDeAtividadeNoSistema)
            self.__definirResultadosMaodeObra(atividadeSistema.atividade, estado, resultado, quantidadeDeAtividadeNoSistema)
            

        quantidadeDeSistemaNoProjeto = resultado.quantidade
        self.__multiplicarValoresAmbientais(resultado, quantidadeDeSistemaNoProjeto)

        self.__adicionarValoresTransporte(projeto_id,sistemaConstrutivo_id, resultado)

        return resultado


    def __definirResultadosEquipamentos(self, atividade_id:int, estado:str, resultado:ResultadosSistemaContrutivoDTO, quantidadeNoSistema: float):
        atividadeEquipamentos = self.session.query(AtividadeEquipamento).filter(AtividadeEquipamento.atividade_id==atividade_id).all()
        
        for atividadeEquipamento in atividadeEquipamentos:
            self.__definirValoresAmbientais(resultado, atividadeEquipamento.equipamento, atividadeEquipamento.quantidade, quantidadeNoSistema)

            custo = self.session.query(Custo).filter(Custo.equipamento_id == atividadeEquipamento.equipamento_id, Custo.estado == estado).first()
            if custo == None: 
                return
            
            resultado.CustoTotal += (custo.custoMedio * atividadeEquipamento.quantidade * quantidadeNoSistema)


    def __definirResultadosMaterial(self, atividade_id:int, estado:str, resultado:ResultadosSistemaContrutivoDTO, quantidadeNoSistema: float):
        atividadeMateriais = self.session.query(AtividadeMaterial).filter(AtividadeMaterial.atividade_id==atividade_id).all()
        
        for atividadeMaterial in atividadeMateriais:

            self.__definirValoresAmbientais(resultado, atividadeMaterial.material, atividadeMaterial.quantidade, quantidadeNoSistema)

            custo = self.session.query(Custo).filter(Custo.material_id == atividadeMaterial.material_id, Custo.estado == estado).first()
            if custo == None: 
                return
        
            resultado.CustoTotal += (custo.custoMedio * atividadeMaterial.quantidade * quantidadeNoSistema)

    def __definirResultadosMaodeObra(self, atividade: Atividade, estado:str, resultado:ResultadosSistemaContrutivoDTO, quantidadeNoSistema: float):
        atividadeMaoDeObras = self.session.query(AtividadeMaoDeObra).filter(AtividadeMaoDeObra.atividade_id==atividade.id).all()
        
        for atividadeMaoDeObra in atividadeMaoDeObras:
            custo = self.session.query(Custo).filter(Custo.maoDeObra_id == atividadeMaoDeObra.maoDeObra_id, Custo.estado == estado).first()
            if custo == None: 
                return
        
            resultado.CustoTotal += ((custo.custoMedio * atividadeMaoDeObra.quantidade) * (atividade.encargoSocial/100) * quantidadeNoSistema) 


    def __definirValoresAmbientais(self, resultado, insumo, quantidadeNaAtividade:float, quantidadeNoSistema:float):
        resultado.GWPTotal += float(insumo.emiEfeitoEstufa) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.ODPTotal += float(insumo.emiDegradamOzonio) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.APTotal += float(insumo.emiChuvaAcida) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.EPTotal += float(insumo.emiToxicosPatogenicos) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.POCPTotal += float(insumo.emiContribEutroficacao) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.ResidPerigosoTotal += float(insumo.qtdResiduosPerigosos) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.ResidRadioativoTotal += float(insumo.qtdResiduosRadioativos) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.ResidNaoPerigosoTotal += float(insumo.qtdResiduosNaoPerigosos) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.EnergiaNaoRenovavelTotal += float(insumo.qtdEnergiaNaoRenovavel) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.EnergiaRenovavelTotal += float(insumo.qtdEnergiaRenovavel) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.AguaRedeAbastesimentoTotal += float(insumo.qtdAguaRedeAbastecimento) * quantidadeNaAtividade * quantidadeNoSistema
        resultado.AguaReusoTotal += float(insumo.qtdAguaReutilizada)  * quantidadeNaAtividade * quantidadeNoSistema


    def __multiplicarValoresAmbientais(self, resultado:ResultadosSistemaContrutivoDTO, quantidade: float):
        resultado.GWPTotal *= quantidade
        resultado.ODPTotal *= quantidade
        resultado.APTotal *= quantidade
        resultado.EPTotal *= quantidade
        resultado.POCPTotal *= quantidade
        resultado.ResidPerigosoTotal *= quantidade
        resultado.ResidRadioativoTotal *= quantidade
        resultado.ResidNaoPerigosoTotal *= quantidade
        resultado.EnergiaNaoRenovavelTotal *= quantidade
        resultado.EnergiaRenovavelTotal *= quantidade
        resultado.AguaRedeAbastesimentoTotal *= quantidade
        resultado.AguaReusoTotal *= quantidade 
        resultado.CustoTotal *= quantidade
        


    def __adicionarValoresTransporte(self, projeto_id:int, sistemaConstrutivo_id:int, resultado: ResultadosSistemaContrutivoDTO):
        resultadoTransporte = ResultadoTransporteProjetoController(self.session)
        indicadoresMeioTransporte = resultadoTransporte.queryTransportationResults(projeto_id, sistemaConstrutivo_id, resultado.quantidade)

        for meioTransporte in indicadoresMeioTransporte:
            resultado.GWPTotal += meioTransporte.GWPTotal
            resultado.ODPTotal += meioTransporte.ODPTotal
            resultado.APTotal += meioTransporte.APTotal
            resultado.EPTotal += meioTransporte.EPTotal
            resultado.POCPTotal += meioTransporte.POCPTotal
            resultado.CustoTotal += meioTransporte.custoTotal

