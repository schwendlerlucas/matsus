from controllers.controllerBase import ControllerBase

from DTOs.meioTransporteInsumosDTO import MeioTransporteInsumosDTO
from DTOs.sistemaMeioTransporteInsumosDTO import SistemaMeioTransporteInsumosDTO

from models.projetoSistemaConstrutivoAtividadeTransporte import ProjetoSistemaConstrutivoAtividadeTransporte
from models.atividadeEquipamento import AtividadeEquipamento
from models.atividadeMaterial import AtividadeMaterial

from interfaces.validacoesCampos import *


class InsumoTransporteController(ControllerBase):

    def getEquipamentoTransporteByProjetoId(self, projeto_id: int, quantidade: float):
        sistemaAtividadeList = self.session.query(ProjetoSistemaConstrutivoAtividadeTransporte).filter(ProjetoSistemaConstrutivoAtividadeTransporte.projeto_id==projeto_id).\
            order_by(ProjetoSistemaConstrutivoAtividadeTransporte.sistemaConstrutivo_id, 
                     ProjetoSistemaConstrutivoAtividadeTransporte.atividade_id).all()
    
        sistemaAtividadeSalubridadeSeguridadeList = []
        lastSistema = -9999
        for sistemaAtividade in sistemaAtividadeList:
            if sistemaAtividade.equipamento_id == None:
                continue
            
            if lastSistema != sistemaAtividade.sistemaConstrutivo_id:
                newSistema = SistemaMeioTransporteInsumosDTO()
                lastSistema = sistemaAtividade.sistemaConstrutivo_id
                newSistema.sistema_id = lastSistema
                sistemaAtividadeSalubridadeSeguridadeList.append(newSistema)
            
            newAtividade = MeioTransporteInsumosDTO()
            newSistema.insumosList.append(newAtividade)
            newAtividade.atividade_id = sistemaAtividade.atividade_id
            newAtividade.atividade_nome = sistemaAtividade.atividade.nome
            newAtividade.insumo_id = sistemaAtividade.equipamento_id
            newAtividade.insumo_nome = sistemaAtividade.equipamento.nome
            if sistemaAtividade.meioTransporte != None:
                newAtividade.meio_transporte_id = sistemaAtividade.meioTransporte_id
                newAtividade.meio_transporte_nome = sistemaAtividade.meioTransporte.nome
            newAtividade.distancia1 = sistemaAtividade.distancia1
            newAtividade.distancia2 = sistemaAtividade.distancia2
            newAtividade.valorFrete = sistemaAtividade.valorFrete

            atividadeEquipamento = self.session.query(AtividadeEquipamento).\
                filter(AtividadeEquipamento.atividade_id==sistemaAtividade.atividade_id, 
                       AtividadeEquipamento.equipamento_id==sistemaAtividade.equipamento_id).first()

            newAtividade.quantidade_unitaria = float(atividadeEquipamento.quantidade)
            newAtividade.quantidade_total = newAtividade.quantidade_unitaria * float(quantidade)
            newAtividade.peso_unitario = retornarZeroSeNoneOuVazio(atividadeEquipamento.equipamento.peso)
            newAtividade.peso_total = newAtividade.peso_unitario * float(quantidade)
            newAtividade.unidade = atividadeEquipamento.unidade

        return sistemaAtividadeSalubridadeSeguridadeList


    def getMaterialTransporteByProjetoId(self, projeto_id: int, quantidade: float):
        sistemaAtividadeList = self.session.query(ProjetoSistemaConstrutivoAtividadeTransporte).filter(ProjetoSistemaConstrutivoAtividadeTransporte.projeto_id==projeto_id).\
            order_by(ProjetoSistemaConstrutivoAtividadeTransporte.sistemaConstrutivo_id, 
                     ProjetoSistemaConstrutivoAtividadeTransporte.atividade_id).all()
    
        sistemaAtividadeSalubridadeSeguridadeList = []
        lastSistema = -9999
        for sistemaAtividade in sistemaAtividadeList:
            if sistemaAtividade.material_id == None:
                continue
            
            if lastSistema != sistemaAtividade.sistemaConstrutivo_id:
                newSistema = SistemaMeioTransporteInsumosDTO()
                lastSistema = sistemaAtividade.sistemaConstrutivo_id
                newSistema.sistema_id = lastSistema
                sistemaAtividadeSalubridadeSeguridadeList.append(newSistema)
            
            newAtividade = MeioTransporteInsumosDTO()
            newSistema.insumosList.append(newAtividade)
            newAtividade.atividade_id = sistemaAtividade.atividade_id
            newAtividade.atividade_nome = sistemaAtividade.atividade.nome
            newAtividade.insumo_id = sistemaAtividade.material_id
            newAtividade.insumo_nome = sistemaAtividade.material.nome
            if sistemaAtividade.meioTransporte != None:
                newAtividade.meio_transporte_id = sistemaAtividade.meioTransporte_id
                newAtividade.meio_transporte_nome = sistemaAtividade.meioTransporte.nome
            newAtividade.distancia1 = sistemaAtividade.distancia1
            newAtividade.distancia2 = sistemaAtividade.distancia2
            newAtividade.valorFrete = sistemaAtividade.valorFrete

            atividadeMaterial = self.session.query(AtividadeMaterial).\
                filter(AtividadeMaterial.atividade_id==sistemaAtividade.atividade_id, 
                       AtividadeMaterial.material_id==sistemaAtividade.material_id).first()

            newAtividade.quantidade_unitaria = float(atividadeMaterial.quantidade)
            newAtividade.quantidade_total = newAtividade.quantidade_unitaria * float(quantidade)
            newAtividade.peso_unitario = retornarZeroSeNoneOuVazio(0)
            newAtividade.peso_total = newAtividade.peso_unitario * float(quantidade)
            newAtividade.unidade = atividadeMaterial.unidade

        return sistemaAtividadeSalubridadeSeguridadeList
        


    def getAvgSalubridadeSeguridadeAndSumQuantidade(self, listaSistemaAtividade):
        salubridadeAvg = 0.0
        seguridadeAvg = 0.0
        quantidadeSum = 0.0

        for item in listaSistemaAtividade:
            salubridadeAvg += item.grauSalubridade
            seguridadeAvg += item.grauSeguridade
            quantidadeSum += item.quantidade


        if len(listaSistemaAtividade) > 0:
            salubridadeAvg /= len(listaSistemaAtividade)
            seguridadeAvg /= len(listaSistemaAtividade)
        
        return salubridadeAvg, seguridadeAvg, quantidadeSum


        