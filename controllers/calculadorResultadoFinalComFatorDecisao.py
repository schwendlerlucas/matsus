from controllers.controllerBase import ControllerBase
from DTOs.categoriasNormatizadasDTO import CategoriasNormatizadasDTO
from DTOs.resultadosSistemaConstrutivoDTO import ResultadosSistemaContrutivoDTO
from models.fatoresDecisao import FatoresDecisao

class CalculadorResultadoFinalComFatorDecisao(ControllerBase):


    def Calc(self, resultadosList, categoriasNormatizadas):

        self.__categoriasNormatizadasDTO = categoriasNormatizadas

        fatoresDecisao = self.session.query(FatoresDecisao).filter(FatoresDecisao.id == 1).first()
        if fatoresDecisao == None:
            print("Fatores de decisão não cadastrados.")
            return
            
        for i in range(0, len(resultadosList)):
            soma = 0.0
            soma += self.__categoriasNormatizadasDTO[i].AI * self.__dividePercentual(fatoresDecisao.AI)
            soma += self.__categoriasNormatizadasDTO[i].AII * self.__dividePercentual(fatoresDecisao.AII)
            soma += self.__categoriasNormatizadasDTO[i].AIII * self.__dividePercentual(fatoresDecisao.AIII)
            soma += self.__categoriasNormatizadasDTO[i].AIV * self.__dividePercentual(fatoresDecisao.AIV)
            
            soma += self.__categoriasNormatizadasDTO[i].SI * self.__dividePercentual(fatoresDecisao.SI)
            soma += self.__categoriasNormatizadasDTO[i].SII * self.__dividePercentual(fatoresDecisao.SII)
            soma += self.__categoriasNormatizadasDTO[i].SIII * self.__dividePercentual(fatoresDecisao.SIII)
            soma += self.__categoriasNormatizadasDTO[i].SIV * self.__dividePercentual(fatoresDecisao.SIV)

            soma += self.__categoriasNormatizadasDTO[i].EI * self.__dividePercentual(fatoresDecisao.EI)
            soma += self.__categoriasNormatizadasDTO[i].EII * self.__dividePercentual(fatoresDecisao.EII)

            soma += self.__categoriasNormatizadasDTO[i].TI * self.__dividePercentual(fatoresDecisao.TI)
            soma += self.__categoriasNormatizadasDTO[i].TII * self.__dividePercentual(fatoresDecisao.TII)
            soma += self.__categoriasNormatizadasDTO[i].TIII * self.__dividePercentual(fatoresDecisao.TIII)
            soma += self.__categoriasNormatizadasDTO[i].TIV * self.__dividePercentual(fatoresDecisao.TIV)
            soma += self.__categoriasNormatizadasDTO[i].TV * self.__dividePercentual(fatoresDecisao.TV)

            resultadosList[i].ResultadoFinal = soma

    def __dividePercentual(self, valor: float):
        return valor/100