from controllers.controllerBase import ControllerBase
from models.projetoSistemaConstrutivo import ProjetoSistemaConstrutivo

class ProjetoSistemaConstrutivoController(ControllerBase):


    def getSistemasByProjetoId(self, projeto_id):
        return self.session.query(ProjetoSistemaConstrutivo).\
            filter(ProjetoSistemaConstrutivo.projeto_id==projeto_id).all()
