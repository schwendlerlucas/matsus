from models.atividadeEquipamento import AtividadeEquipamento
from models.sistemaConstrutivoAtividade import SistemaConstrutivoAtividade
from controllers.controllerBase import ControllerBase
from DTOs.sistemaMeioTransporteInsumosDTO import SistemaMeioTransporteInsumosDTO
from DTOs.meioTransporteInsumosDTO import MeioTransporteInsumosDTO
from models.atividadeEquipamento import AtividadeEquipamento
from interfaces.validacoesCampos import *

class AtividadeEquipamentoController(ControllerBase):

    def getEquipamentoAtividadeList(self, sistema_id: int, quantidadeSistemaContrutivo: float):
        atividadeList = self.session.query(SistemaConstrutivoAtividade).\
            filter(SistemaConstrutivoAtividade.sistemaConstrutivo_id==sistema_id).all()
        
        meioTransporteList = []

        for atividade in atividadeList:
            equipamentoList = self.session.query(AtividadeEquipamento).\
                filter(AtividadeEquipamento.atividade_id==atividade.atividade_id).all()
            for equipamento in equipamentoList:
                newMeioTransporteInsumo = MeioTransporteInsumosDTO()
                newMeioTransporteInsumo.atividade_id = atividade.atividade.id
                newMeioTransporteInsumo.atividade_nome = atividade.atividade.nome
                newMeioTransporteInsumo.insumo_id = equipamento.equipamento.id
                newMeioTransporteInsumo.insumo_nome = equipamento.equipamento.nome
                newMeioTransporteInsumo.quantidade_unitaria = retornarZeroSeNoneOuVazio(equipamento.quantidade)
                newMeioTransporteInsumo.quantidade_total = float(newMeioTransporteInsumo.quantidade_unitaria) * float(quantidadeSistemaContrutivo)
                newMeioTransporteInsumo.peso_unitario = retornarZeroSeNoneOuVazio(equipamento.equipamento.peso)
                newMeioTransporteInsumo.peso_total = float(newMeioTransporteInsumo.peso_unitario) * float(quantidadeSistemaContrutivo)
                newMeioTransporteInsumo.unidade = equipamento.unidade
                meioTransporteList.append(newMeioTransporteInsumo)

        return meioTransporteList


          
    
